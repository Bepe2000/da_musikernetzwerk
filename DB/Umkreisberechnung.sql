select @lat:=latitude from cities where cityname like "steinach am brenner";
select @log:=longitude from cities where cityname like "steinach am brenner";

SELECT 
    cityid,
    PostalCode,
    Cityname,
    latitude,
    longitude,
    (6371 * ACOS(COS(RADIANS(@lat)) * COS(RADIANS(latitude)) * COS(RADIANS(longitude) - RADIANS(@log)) + SIN(RADIANS(@lat)) * SIN(RADIANS(latitude)))) AS distance
FROM
    Cities
HAVING distance <= 30
ORDER BY distance ASC;
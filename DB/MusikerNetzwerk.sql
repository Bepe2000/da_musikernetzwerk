#create database musikernetzwerk;
USE musikernetzwerk;

-- DROP TABLES
DROP TABLE IF EXISTS BandMember_SearchInstruments;
DROP TABLE IF EXISTS MusicianInstruments;
DROP TABLE IF EXISTS Instruments;
DROP TABLE IF EXISTS MusicianGenres;
DROP TABLE IF EXISTS MusicianBand_Requests;
DROP TABLE IF EXISTS BandMember_Requests;
DROP TABLE IF EXISTS BandMembers;
DROP TABLE IF EXISTS MusicianBand_SearchGenres;
DROP TABLE IF EXISTS MusicianBand_SearchProperties;
DROP TABLE IF EXISTS Musicians;
DROP TABLE IF EXISTS BandGenres;
DROP TABLE IF EXISTS Genres;
DROP TABLE IF EXISTS BandMember_SearchProperties;
DROP TABLE IF EXISTS Bands;
DROP TABLE IF EXISTS Cities;
DROP TABLE IF EXISTS Countries;

-- DROP DATABASE

DROP DATABASE IF EXISTS musikernetzwerk;

-- CREATE DATABASE

CREATE DATABASE musikernetzwerk;

-- USE DATABASE

USE musikernetzwerk;

-- CREATE TABLES

CREATE TABLE Countries (
   CountryID    INT 		 NOT NULL AUTO_INCREMENT,
   CountryName  VARCHAR(2) NOT NULL, #ISO 3166 ALPHA-2
   CONSTRAINT pk_Countries_CountryID PRIMARY KEY (CountryID)
)   ENGINE=InnoDB;

CREATE TABLE Cities (
   CityID     INT    	   NOT NULL AUTO_INCREMENT,
   CountryID  INT          NOT NULL,
   PostalCode INT    	   NOT NULL,
   CityName   VARCHAR(100) NOT NULL,
   Latitude   FLOAT        NULL,
   Longitude  FLOAT        NULL,
   CONSTRAINT pk_Cities_CityID PRIMARY KEY (CityID),
   CONSTRAINT fk_Cities_Countries_CountryID FOREIGN KEY (CountryID) REFERENCES Countries(CountryID)
   ON DELETE CASCADE ON UPDATE CASCADE
)   ENGINE=InnoDB;


CREATE TABLE Bands (
   BandID             		INT          NOT NULL AUTO_INCREMENT,
   Email              		VARCHAR(100) NOT NULL UNIQUE,
   PasswordHash       		CHAR(64)     NOT NULL,
   BandName           		VARCHAR(100) NOT NULL UNIQUE,
   ClosedGroup        		BOOL         NOT NULL,
   MemberCount 		  		INT			 NOT NULL,
   Skill				    INT			 NOT NULL,
   UserLanguage				CHAR(2),
   CityID             		INT,
   Description        		VARCHAR(500),
   CONSTRAINT pk_Bands_BandID PRIMARY KEY (BandID),
   CONSTRAINT fk_Bands_Cities_CityID FOREIGN KEY (CityID) REFERENCES Cities(CityID)
   ON DELETE SET NULL ON UPDATE SET NULL  
)   ENGINE=InnoDB;

CREATE TABLE BandMember_SearchProperties (
   BandMember_SearchID          INT          NOT NULL AUTO_INCREMENT,
   BandID       		        INT          NOT NULL,
   Skillmin						INT,
   Skillmax						INT,
   CityDistance_max				INT,
   CONSTRAINT pk_BandMember_SearchProperties PRIMARY KEY (BandMember_SearchID),
   CONSTRAINT fk_BandMember_SearchProperties_Bands_BandID  FOREIGN KEY (BandID) REFERENCES Bands(BandID)
   ON DELETE CASCADE ON UPDATE CASCADE
)   ENGINE=InnoDB;

CREATE TABLE Genres (
   GenreID   INT		  NOT NULL	 AUTO_INCREMENT,
   de        VARCHAR(100) NOT NULL,
   en 		 VARCHAR(100) NOT NULL,
   CONSTRAINT pk_Genres PRIMARY KEY (GenreID)
)   ENGINE=InnoDB;

CREATE TABLE BandGenres (
   BandID  INT NOT NULL,
   GenreID INT,
   CONSTRAINT fk_BandGenres_Bands_BandID  FOREIGN KEY (BandID) REFERENCES Bands(BandID)
   ON DELETE CASCADE ON UPDATE CASCADE,
   CONSTRAINT fk_BandGenres_Genres_GenreID FOREIGN KEY (GenreID) REFERENCES Genres(GenreID)
   ON DELETE CASCADE ON UPDATE CASCADE   
)   ENGINE=InnoDB;

CREATE TABLE Musicians (
   MusicianID         INT          NOT NULL AUTO_INCREMENT,
   Email              VARCHAR(100) NOT NULL UNIQUE,
   PasswordHash       CHAR(64)     NOT NULL,
   FirstName          VARCHAR(50)  NOT NULL,
   LastName           VARCHAR(50)  NOT NULL,
   BirthDate          DATE         NOT NULL,
   Skill			  INT		   NOT NULL,
   UserLanguage		  CHAR(2),
   CityID      	      INT,
   ArtistName         VARCHAR(100) UNIQUE,
   CONSTRAINT pk_Musicians PRIMARY KEY (MusicianID),
   CONSTRAINT fk_Musicians_Cities_CityID FOREIGN KEY (CityID) REFERENCES Cities(CityID)
   ON DELETE SET NULL ON UPDATE SET NULL
)   ENGINE=InnoDB;

CREATE TABLE MusicianBand_SearchProperties (
   MusicianBand_SearchID       INT          NOT NULL AUTO_INCREMENT,
   MusicianID     				INT          NOT NULL,
   Skillmin						INT,
   Skillmax						INT,
   MemberCount_min				INT,
   MemberCount_max				INT,
   CityDistance_max				INT,
   CONSTRAINT pk_MusicianBand_SearchProperties PRIMARY KEY (MusicianBand_SearchID),
   CONSTRAINT fk_MusicianBand_SearchProperties_Musicians_MusicianID FOREIGN KEY (MusicianID) REFERENCES Musicians(MusicianID)
   ON DELETE CASCADE ON UPDATE CASCADE
)   ENGINE=InnoDB;

CREATE TABLE MusicianBand_SearchGenres (
	MusicianBand_SearchID  INT	NOT NULL,
    GenreID				    INT	NOT NULL,
	CONSTRAINT fk_MusicianBand_SearchGenres_MusicianBand_SearchProperties FOREIGN KEY ( MusicianBand_SearchID ) REFERENCES MusicianBand_SearchProperties ( MusicianBand_SearchID )
    ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_MusicianBand_SearchGenres_Genres_GenreID FOREIGN KEY (GenreID) REFERENCES Genres(GenreID)
    ON DELETE CASCADE ON UPDATE CASCADE   
)ENGINE=InnoDB;

CREATE TABLE BandMembers (
   BandID     INT NOT NULL,
   MusicianID INT NOT NULL,
   CONSTRAINT fk_BandMembers_Bands_BandID  FOREIGN KEY (BandID) REFERENCES Bands(BandID)
   ON DELETE CASCADE ON UPDATE CASCADE,
   CONSTRAINT fk_BandMembers_Musicians_MusicianID FOREIGN KEY (MusicianID) REFERENCES Musicians(MusicianID)
   ON DELETE CASCADE ON UPDATE CASCADE
)   ENGINE=InnoDB;

CREATE TABLE BandMember_Requests (
   MemberRequest_ID INT NOT NULL AUTO_INCREMENT,
   BandID           INT NOT NULL,
   MusicianID       INT NOT NULL,
   CONSTRAINT pk_BandMember_Requests PRIMARY KEY (MemberRequest_ID),
   CONSTRAINT fk_BandMember_Requests_Bands_BandID  FOREIGN KEY (BandID) REFERENCES Bands(BandID)
   ON DELETE CASCADE ON UPDATE CASCADE,
   CONSTRAINT fk_BandMember_Requests_Musicians_MusicianID FOREIGN KEY (MusicianID) REFERENCES Musicians(MusicianID)
   ON DELETE CASCADE ON UPDATE CASCADE
)   ENGINE=InnoDB;

CREATE TABLE MusicianBand_Requests (
   BandRequest_ID INT NOT NULL AUTO_INCREMENT,
   MusicianID     INT NOT NULL,
   BandID         INT NOT NULL,
   CONSTRAINT pk_MusicianBand_Requests PRIMARY KEY (BandRequest_ID),
   CONSTRAINT fk_MusicianBand_Requests_Bands_BandID  FOREIGN KEY (BandID) REFERENCES Bands(BandID)
   ON DELETE CASCADE ON UPDATE CASCADE,
   CONSTRAINT fk_MusicianBand_Requests_Musicians_MusicianID FOREIGN KEY (MusicianID) REFERENCES Musicians(MusicianID)
   ON DELETE CASCADE ON UPDATE CASCADE
)   ENGINE=InnoDB;

CREATE TABLE MusicianGenres (
   MusicianID INT NOT NULL,
   GenreID    INT NOT NULL,
   CONSTRAINT fk_MusicianGenres_Musicians_MusicianID FOREIGN KEY (MusicianID) REFERENCES Musicians(MusicianID)
   ON DELETE CASCADE ON UPDATE CASCADE,
   CONSTRAINT fk_MusicianGenres_Genres_GenreID FOREIGN KEY (GenreID) REFERENCES Genres(GenreID)
   ON DELETE CASCADE ON UPDATE CASCADE         
)   ENGINE=InnoDB;

CREATE TABLE Instruments (
   InstrumentID    INT          NOT NULL AUTO_INCREMENT,
   de 			   VARCHAR(100) NOT NULL,
   en 			   VARCHAR(100) NOT NULL,
   CONSTRAINT pk_Instruments PRIMARY KEY (InstrumentID)
)   ENGINE=InnoDB;

CREATE TABLE MusicianInstruments (
   MusicianID   INT NOT NULL,
   InstrumentID INT NOT NULL,
   Skill		INT NOT NULL,
   CONSTRAINT fk_MusicianInstruments_Musicians_MusicianID FOREIGN KEY (MusicianID) REFERENCES Musicians(MusicianID)
   ON DELETE CASCADE ON UPDATE CASCADE,
   CONSTRAINT fk_MusicianInstruments_Instruments_InstrumentID FOREIGN KEY (InstrumentID) REFERENCES Instruments(InstrumentID)
   ON DELETE CASCADE ON UPDATE CASCADE         
)   ENGINE=InnoDB;

CREATE TABLE BandMember_SearchInstruments (
   BandMember_SearchID  INT NOT NULL,
   InstrumentID INT,
   CONSTRAINT fk_BandMember_SearchInstruments_SearchProperties_SearchID FOREIGN KEY (BandMember_SearchID ) REFERENCES BandMember_SearchProperties(BandMember_SearchID )
   ON DELETE CASCADE ON UPDATE CASCADE,
   CONSTRAINT fk_BandMember_SearchInstruments_Instruments_InstrumentID FOREIGN KEY (InstrumentID) REFERENCES Instruments(InstrumentID)
   ON DELETE CASCADE ON UPDATE CASCADE         
)   ENGINE=InnoDB;


SHOW TABLES;


INSERT INTO GENRES VALUES(NULL,"Alternative","Alternative");
INSERT INTO GENRES VALUES(NULL,"Blues","Blues");
INSERT INTO GENRES VALUES(NULL,"EDM","EDM");
INSERT INTO GENRES VALUES(NULL,"Folk","Folk");
INSERT INTO GENRES VALUES(NULL,"Funk","Funk");
INSERT INTO GENRES VALUES(NULL,"Jazz","Jazz");
INSERT INTO GENRES VALUES(NULL,"Klassik","Classic");
INSERT INTO GENRES VALUES(NULL,"Metal","Metal");
INSERT INTO GENRES VALUES(NULL,"Pop","Pop");
INSERT INTO GENRES VALUES(NULL,"Punk","Punk");
INSERT INTO GENRES VALUES(NULL,"Reggae","Reggae");
INSERT INTO GENRES VALUES(NULL,"Rock","Rock");
INSERT INTO GENRES VALUES(NULL,"Schlager","Schlager");
INSERT INTO GENRES VALUES(NULL,"Soul","Soul");
INSERT INTO GENRES VALUES(NULL,"Techno","Techno");
INSERT INTO GENRES VALUES(NULL,"VolkstümlicheMusik","Folkmusic");

INSERT INTO INSTRUMENTS VALUES (NULL,"Akkordeon","Accordion");
INSERT INTO INSTRUMENTS VALUES (NULL,"Balalaika","Balalaika");
INSERT INTO INSTRUMENTS VALUES (NULL,"Banjo","Banjo");
INSERT INTO INSTRUMENTS VALUES (NULL,"Bass-Gitarre","Bass Guitar");
INSERT INTO INSTRUMENTS VALUES (NULL,"Becken","Basins");
INSERT INTO INSTRUMENTS VALUES (NULL,"Blockflöte","Fipple Flute ");
INSERT INTO INSTRUMENTS VALUES (NULL,"Bongos","Bongos");
INSERT INTO INSTRUMENTS VALUES (NULL,"Bratsche","Viola");
INSERT INTO INSTRUMENTS VALUES (NULL,"Cajón","Cajón");
INSERT INTO INSTRUMENTS VALUES (NULL,"Cello","Cello");
INSERT INTO INSTRUMENTS VALUES (NULL,"Djembé","Djembé");
INSERT INTO INSTRUMENTS VALUES (NULL,"Dudelsack","Bagpipe");
INSERT INTO INSTRUMENTS VALUES (NULL,"E-Bass","E-Bass");
INSERT INTO INSTRUMENTS VALUES (NULL,"E-Gitarre","Electric guitar");
INSERT INTO INSTRUMENTS VALUES (NULL,"Fagott","Bassoon");
INSERT INTO INSTRUMENTS VALUES (NULL,"Geige","Violin");
INSERT INTO INSTRUMENTS VALUES (NULL,"Gitarre","Guitar");
INSERT INTO INSTRUMENTS VALUES (NULL,"Gong","Gong");
INSERT INTO INSTRUMENTS VALUES (NULL,"Harfe","Harp");
INSERT INTO INSTRUMENTS VALUES (NULL,"Horn","Horn");
INSERT INTO INSTRUMENTS VALUES (NULL,"Keyboard","Keyboard");
INSERT INTO INSTRUMENTS VALUES (NULL,"Klarinette","Clarinet");
INSERT INTO INSTRUMENTS VALUES (NULL,"Klavier","Piano");
INSERT INTO INSTRUMENTS VALUES (NULL,"Kontrabass","Double Bass");
INSERT INTO INSTRUMENTS VALUES (NULL,"Mundharmonika","Harmonica");
INSERT INTO INSTRUMENTS VALUES (NULL,"Oboe","Oboe");
INSERT INTO INSTRUMENTS VALUES (NULL,"Okarina","Ocarina");
INSERT INTO INSTRUMENTS VALUES (NULL,"Orgel","Organ");
INSERT INTO INSTRUMENTS VALUES (NULL,"Panflöte","Panflute");
INSERT INTO INSTRUMENTS VALUES (NULL,"Pauke","Timpani");
INSERT INTO INSTRUMENTS VALUES (NULL,"Posaune","Trombone");
INSERT INTO INSTRUMENTS VALUES (NULL,"Querflöte","Transverse Flute");
INSERT INTO INSTRUMENTS VALUES (NULL,"Saxophon","Saxophone");
INSERT INTO INSTRUMENTS VALUES (NULL,"Schellenring","Clamp Ring");
INSERT INTO INSTRUMENTS VALUES (NULL,"Spinett","Spinet");
INSERT INTO INSTRUMENTS VALUES (NULL,"Synthesizer","Synthesizer");
INSERT INTO INSTRUMENTS VALUES (NULL,"Tamburin","Tambourine");
INSERT INTO INSTRUMENTS VALUES (NULL,"Triangel","Triangle");
INSERT INTO INSTRUMENTS VALUES (NULL,"Trompete","Trumpet");
INSERT INTO INSTRUMENTS VALUES (NULL,"Tuba","Tuba");
INSERT INTO INSTRUMENTS VALUES (NULL,"Xylophon","Xylophone");
INSERT INTO INSTRUMENTS VALUES (NULL,"Zither","Zither");
# AT_DE_CH_Dataset einspielen dann
insert into musicians values (1,"ladner@weband.com",sha2("1234",256),"Jonas","Ladner","2000-11-27",8,"de",12344,"Ladner");
insert into musicianinstruments values (1,1,3);
insert into musicianinstruments values (1,3,5);

insert into musicians values (2,"egger@weband.com",sha2("1234",256),"Benjamin","Egger","2000-12-14",5,"de",12099,"Egger");
insert into musicianinstruments values (2,15,3);
insert into musicianinstruments values (2,12,5);

insert into musicians values (3,"gatt@weband.com",sha2("1234",256),"Marian","Gatt","2001-04-01",6,"de",11998,"Gatt");
insert into musicianinstruments values (3,2,3);
insert into musicianinstruments values (3,3,5);

insert into bands values(1,"ladner_band@weband.com",sha2("1234",256),"LadnerBand",true,5,8,"de",12344,"Beschreibung Ladner");
insert into bandgenres values (1,2);
insert into bandgenres values (1,10);

insert into bands values(2,"egger_band@weband.com",sha2("1234",256),"EggerBand",false,3,5,"de",12099,"Beschreibung Egger");
insert into bandgenres values (2,4);
insert into bandgenres values (2,1);

insert into bands values(3,"gatt_band@weband.com",sha2("1234",256),"GattBand",false,1,6,"de",11998,"Beschreibung Gatt");
insert into bandgenres values (3,7);
insert into bandgenres values (3,10);




INSERT INTO MusicianBand_Requests VALUES(null, 1, 1);
INSERT INTO MusicianBand_Requests VALUES(null, 2, 1);
INSERT INTO MusicianBand_Requests VALUES(null, 3, 1);
insert into BandMember_Requests VALUES(null, 1, 1);
insert into BandMember_Requests VALUES(null, 2, 1);
insert into BandMember_Requests VALUES(null, 3, 1);

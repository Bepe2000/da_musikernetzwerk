﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models
{
    public class BandMember_SearchInstrument
    {
        public int? BandMember_SearchId { get; set; }
        public int? InstrumentId { get; set; }

        public BandMember_SearchInstrument(): this(null, null) { }
        public BandMember_SearchInstrument(int? bandMember_SearchId, int? instrumentId)
        {
            this.BandMember_SearchId = bandMember_SearchId;
            this.InstrumentId = instrumentId;
        }
    }
}
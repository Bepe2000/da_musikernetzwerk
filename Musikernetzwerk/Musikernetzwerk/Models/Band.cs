﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models
{
    public class Band
    {
        public int BandID { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string Bandname { get; set; }
        public bool ClosedGroup { get; set; }
        public int? MemberCount { get; set; }
        public int? Skill { get; set; }
        public string Userlanguage { get; set; }
        public int? CityID { get; set; }   
        public string Description { get; set; }

        public Band() : this(-1, "", "", "", false, null, null, "", null, "") { }
        public Band(int bandID, string email, string passwordhash, string bandname, bool closedGroup, int? memberCount, int? skill, string userlanguage, int? cityID,  string description)
        {
            this.BandID = bandID;
            this.Email = email;
            this.PasswordHash = passwordhash;
            this.Bandname = bandname;
            this.ClosedGroup = closedGroup;
            this.MemberCount = memberCount;
            this.Skill = skill;
            this.Userlanguage = userlanguage;
            this.CityID = cityID;   
            this.Description = description;          
        }
    }
}
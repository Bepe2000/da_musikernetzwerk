﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models
{
    public class MusicianBand_SearchProperty
    {
        public int? MusicianBand_SearchID{ get; set; }
        public int? MusicianID { get; set; }
        public int? SkillMin { get; set; }
        public int? SkillMax { get; set; }
        public int? MemberCountMin { get; set; }
        public int? MemberCountMax { get; set; }
        public double? CityDistance_max { get; set; }

        public MusicianBand_SearchProperty() : this(null, null, null, null, null, null, null) { }
        public MusicianBand_SearchProperty(int? MusicianBand_SearchID, int? musicianID, int? skillMin, int? skillMax, int? memberCountMin, int? memberCountMax, double? cityDistance_max)
        {
            this.MusicianBand_SearchID= MusicianBand_SearchID;
            this.MusicianID = musicianID;
            this.SkillMin = skillMin;
            this.SkillMax = skillMax;
            this.MemberCountMin = memberCountMin;
            this.MemberCountMax = memberCountMax;
            this.CityDistance_max = cityDistance_max;
        }
    }
}
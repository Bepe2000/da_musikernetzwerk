﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models.ViewModels
{
    public class SearchAfterBandResultModel
    {

        public int? BandID { get; set; }
        public string Email { get; set; }
        public string Bandname { get; set; }
        public bool ClosedGroup { get; set; }
        public int? MemberCount { get; set; }
        public int? Skill { get; set; }
        public string Description { get; set; }
        public string CityName { get; set; }
        public int? PostalCode { get; set; }
        public decimal? CityDistance { get; set; }

        public SearchAfterBandResultModel() : this(0, "", "", false, null, null, null, "", 0, 0.0m) { }

        public SearchAfterBandResultModel(int? bandID, string email, string bandName, bool closedgroup, int? membercount, int? skill, string description, string cityName, int? postalCode, decimal? cityDistance)
        {
            this.BandID = bandID;
            this.Email = email;
            this.Bandname = bandName;
            this.ClosedGroup = closedgroup;
            this.MemberCount = membercount;
            this.Skill = skill;
            this.Description = description;
            this.CityName = cityName;
            this.PostalCode = postalCode;
            this.CityDistance = cityDistance;
        }
    }
}
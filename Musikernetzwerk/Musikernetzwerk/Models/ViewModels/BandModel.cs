﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Musikernetzwerk.Models.ViewModels
{
    public class BandModel
    {
        public Band Band { get; set; }
        public City City { get; set; }
        public List<Musician> Musicians { get; set; }
        public List<Genre> OldChosenGenres { get; set; }
        public int[] NewChosenGenreIDs { get; set; }
        public MultiSelectList GenreSelectList { get; set; }
        public List<Genre> NewChosenGenres { get; set; }
        public BandModel() : this(null, null, null, null, null, null) { }
        public BandModel(Band band, City city, List<Musician> musicians, List<Genre> oldChosenGenres, int[] newChosenGenreIDs, MultiSelectList newChosenGenres)
        {
            this.Band = band;
            this.City = city;
            this.Musicians = musicians;
            this.OldChosenGenres = oldChosenGenres;
            this.NewChosenGenreIDs = newChosenGenreIDs;
            this.GenreSelectList = GenreSelectList;
            this.NewChosenGenres = new List<Genre>();
        }
    }
}
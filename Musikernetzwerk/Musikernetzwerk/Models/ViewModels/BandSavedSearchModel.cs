﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models.ViewModels
{
    public class BandSavedSearchModel
    {
        public BandMember_SearchProperty SearchProperty { get; set; }
        public List<Instrument> Instruments { get; set; }

        public BandSavedSearchModel(BandMember_SearchProperty searchProperty, List<Instrument> instruments)
        {
            this.SearchProperty = searchProperty;
            this.Instruments = instruments;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models.ViewModels
{
    public class BandMemberRequestModel
    {       
        public Band Band { get; set; }
        public BandMember_Request Request { get; set; }

        public BandMemberRequestModel(Band band, BandMember_Request request)
        {
            this.Band = band;
            this.Request = request;
        }
    }
}
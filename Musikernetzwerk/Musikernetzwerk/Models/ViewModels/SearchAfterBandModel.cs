﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Musikernetzwerk.Models.ViewModels
{
    public class SearchAfterBandModel
    {
        public int? SkillMin { get; set; }
        public int? SkillMax { get; set; }
        public int CityDistance { get; set; }
        public int[]ChosenGenreIDs { get; set; }
        public MultiSelectList Genres { get; set; }
        public SearchAfterBandModel() : this(0,10, 15, new int[] { },  null) { }
        public SearchAfterBandModel(int skillmin, int skillmax, int citydistance, int[] chosengenreids,  MultiSelectList chosengenres)
        {         
            this.SkillMin = skillmin;
            this.SkillMax = skillmax;
            this.CityDistance = citydistance;
            this.ChosenGenreIDs = chosengenreids;
            this.Genres = chosengenres;
           
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models.ViewModels
{
    public class MusicianSavedSearchModel
    {
        public MusicianBand_SearchProperty SearchProperty { get; set; }
        public List<Genre> Genres { get; set; }

        public MusicianSavedSearchModel(MusicianBand_SearchProperty searchProperty, List<Genre> genres)
        {
            this.SearchProperty = searchProperty;
            this.Genres = genres;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;


namespace Musikernetzwerk.Models.ViewModels
{
    public class MusicianModel
    {
        public Musician Musician { get; set; }
        public City City { get; set; }
        public List<Band> Bands { get; set; }
        public List<Instrument> OldChosenInstruments { get; set; }
        public int[] NewChosenInstrumentIDs { get; set; }
        public MultiSelectList InstrumentSelectList { get; set; }
        public List<Instrument> NewChosenInstruments { get; set; }
        public MusicianModel(): this(null, null, null, null, null, null) { }
        public MusicianModel(Musician musician, City city, List<Band> bands, List<Instrument> oldChosenInstruments, int[] newChosenInstrumentIDs, MultiSelectList newChosenInstruments)
        {
            Musician = musician;
            City = city;
            Bands = bands;
            OldChosenInstruments = oldChosenInstruments;
            NewChosenInstrumentIDs = newChosenInstrumentIDs;
            InstrumentSelectList = InstrumentSelectList;
            NewChosenInstruments = new List<Instrument>();
        }
    }
}
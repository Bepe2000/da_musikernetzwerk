﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models.ViewModels
{
    public class SearchAfterMusicianResultModel
    {
        public int? MusicianID { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? Birthdate { get; set; }
        public string ArtistName { get; set; }
        public int? Skill { get; set; }
        public string CityName { get; set; }
        public int? PostalCode { get; set; }
        public decimal? CityDistance { get; set; }

        public SearchAfterMusicianResultModel() : this(0, "", "", "", null, "", 0, "", 0, 0.0m) { }

        public SearchAfterMusicianResultModel(int? musicianID, string email, string firstName, string lastName, DateTime? birthdate, string artistName, int? skill, string cityName, int? postalCode, decimal? cityDistance)
        {
            MusicianID = musicianID;
            Email = email;
            FirstName = firstName;
            LastName = lastName;
            Birthdate = birthdate;
            ArtistName = artistName;
            Skill = skill;
            CityName = cityName;
            PostalCode = postalCode;
            CityDistance = cityDistance;
        }
    }
}
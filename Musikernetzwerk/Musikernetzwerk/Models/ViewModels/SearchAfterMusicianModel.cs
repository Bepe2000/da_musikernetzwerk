﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Musikernetzwerk.Models.ViewModels
{
    public class SearchAfterMusicianModel
    {
        public int? SkillMin { get; set; }
        public int? SkillMax { get; set; }
        public int CityDistance { get; set; }
        public int[] ChosenInstrumentIDs { get; set; }
        public MultiSelectList Instruments { get; set; }
        public SearchAfterMusicianModel() : this(0, 10, 15, new int[] { }, null) { }
        public SearchAfterMusicianModel(int skillmin, int skillmax, int citydistance, int[] choseninstrumentids, MultiSelectList choseninstruments)
        {
            this.SkillMin = skillmin;
            this.SkillMax = skillmax;
            this.CityDistance = citydistance;
            this.ChosenInstrumentIDs = choseninstrumentids;
            this.Instruments = choseninstruments;
            
        }
    }
}
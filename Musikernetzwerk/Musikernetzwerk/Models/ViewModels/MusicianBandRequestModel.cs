﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models.ViewModels
{
    public class MusicianBandRequestModel
    {
        public Musician Musician { get; set; }
        public MusicianBand_Request Request { get; set; }

        
        public MusicianBandRequestModel(Musician musician, /*Band band,*/ MusicianBand_Request request)
        {
            this.Musician = musician;
            this.Request = request;
        }
    }
}
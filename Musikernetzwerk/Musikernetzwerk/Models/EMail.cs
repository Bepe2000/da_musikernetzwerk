﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models
{
    public class EMail
    {
        public string Address { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }

        public EMail() : this("", "", "") { }

        public EMail(string address, string subject, string message)
        {
            Address = address;
            Subject = subject;
            Message = message;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models
{
    public class BandMember_SearchProperty
    {
        public int? BandMember_SearchId { get; set; }
        public int? BandID { get; set; }
        public int? SkillMin { get; set; }
        public int? SkillMax { get; set; }
        public double? CityDistance_max { get; set; }
       
        public BandMember_SearchProperty(): this(null, null, null, null, null) { }
        public BandMember_SearchProperty(int? bandMember_SearchId, int? bandID, int? skillMin, int? skillMax, double? cityDistance_max)
        {
            this.BandMember_SearchId = bandMember_SearchId; ;
            this.BandID = bandID;
            this.SkillMin=skillMin;
            this.SkillMax = skillMax;
            this.CityDistance_max = cityDistance_max;
        }
    }
}
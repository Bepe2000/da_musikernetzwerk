﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;

namespace Musikernetzwerk.Models.DB
{
    public class BandGenreRepository : SQLConnection, IBandGenreRepository
    {
        public List<Genre> GetBandGenre(int bandId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<Genre> allGenre = new List<Genre>();
            try
            {
                MySqlCommand cmdSelectBandGenre = this._connection.CreateCommand();
                cmdSelectBandGenre.CommandText = "SELECT * from BandGenres JOIN genres USING (GenreId) where bandid = @bandId";
                cmdSelectBandGenre.Parameters.AddWithValue("bandId", bandId);

                using (MySqlDataReader reader = cmdSelectBandGenre.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        allGenre.Add(
                            new Genre(reader["genreid"] != DBNull.Value ? Convert.ToInt32(reader["genreid"]) : (int?)null,
                                 Convert.ToString(reader["de"]),
                                 Convert.ToString(reader["en"])
                            ));
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return allGenre.Count >= 1 ? allGenre : null;
        }

        public List<Band> GetBandsByGenre(int genreId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }
            List<Band> bandsByGenre = new List<Band>();
            try
            {
                MySqlCommand cmdSelectGenreByBand = this._connection.CreateCommand();
                cmdSelectGenreByBand.CommandText = "SELECT * from BandGenres JOIN bands USING (BandId) where genreid = @genreId";
                cmdSelectGenreByBand.Parameters.AddWithValue("genreId", genreId);

                using (MySqlDataReader reader = cmdSelectGenreByBand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        bandsByGenre.Add(
                            new Band
                            {
                                BandID = Convert.ToInt32(reader["bandid"]),
                                Bandname = Convert.ToString(reader["bandname"]),
                                Description = Convert.ToString(reader["description"])
                            }
                            );
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return bandsByGenre.Count >= 1 ? bandsByGenre : null;
        }

        public bool InsertBandGenre(int bandId, int genreId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdInsertBandGenre = this._connection.CreateCommand();
                cmdInsertBandGenre.CommandText = "INSERT INTO bandgenres VALUES(@bandId, @genreId)";
                cmdInsertBandGenre.Parameters.AddWithValue("bandId", bandId);
                cmdInsertBandGenre.Parameters.AddWithValue("genreId", genreId);

                return cmdInsertBandGenre.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public bool DeleteBandGenre(int bandId, int genreId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }

            try
            {
                MySqlCommand cmdDeleteBandGenre = this._connection.CreateCommand();
                cmdDeleteBandGenre.CommandText = "DELETE FROM bandgenres WHERE bandid = @bandid and genreid = @genreid";
                cmdDeleteBandGenre.Parameters.AddWithValue("bandId", bandId);
                cmdDeleteBandGenre.Parameters.AddWithValue("genreId", genreId);

                return cmdDeleteBandGenre.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

    }
}




﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Musikernetzwerk.Models.DB;
using Musikernetzwerk.Models.ViewModels;
using MySql.Data.MySqlClient;

namespace Musikernetzwerk.Models.DB
{
    public class BandRepository : SQLConnection, IBandRepository
    {
        ICityRepository cityRepository;
        IBandMembersRepository bandMembersRepository;
        IBandGenreRepository bandGenreRepository;
        IGenreRepository genreRepository;
        public Band GetBand(int bandid)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            try
            {
                MySqlCommand cmdSelectBands = this._connection.CreateCommand();
                cmdSelectBands.CommandText = "SELECT * FROM bands WHERE bandid = @bandid";
                cmdSelectBands.Parameters.AddWithValue("bandid", bandid);
                using (MySqlDataReader reader = cmdSelectBands.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        return new Band()
                        {
                            BandID = Convert.ToInt32(reader["bandid"]),
                            Email = Convert.ToString(reader["email"]),
                            PasswordHash = Convert.ToString(reader["passwordhash"]),
                            Bandname = Convert.ToString(reader["bandname"]),
                            ClosedGroup = Convert.ToBoolean(reader["closedgroup"]),
                            MemberCount = reader["membercount"] != DBNull.Value ? Convert.ToInt32(reader["membercount"]) : (int?)null,
                            Skill = reader["skill"] != DBNull.Value ? Convert.ToInt32(reader["skill"]) : (int?)null,
                            Userlanguage = Convert.ToString(reader["userlanguage"]),
                            CityID = reader["cityid"] != DBNull.Value ? Convert.ToInt32(reader["cityid"]) : (int?)null,
                            Description = Convert.ToString(reader["description"]),
                        };
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return null;
        }

        public List<Band> GetAllBands()
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<Band> allBands = new List<Band>();
            try
            {
                MySqlCommand cmdSelectAllBands = this._connection.CreateCommand();
                cmdSelectAllBands.CommandText = "SELECT * FROM bands";
                using (MySqlDataReader reader = cmdSelectAllBands.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        allBands.Add(
                            new Band
                            {
                                BandID = Convert.ToInt32(reader["bandid"]),
                                Email = Convert.ToString(reader["email"]),
                                PasswordHash = Convert.ToString(reader["passwordhash"]),
                                Bandname = Convert.ToString(reader["bandname"]),
                                ClosedGroup = Convert.ToBoolean(reader["closedgroup"]),
                                MemberCount = reader["membercount"] != DBNull.Value ? Convert.ToInt32(reader["membercount"]) : (int?)null,
                                Skill = reader["skill"] != DBNull.Value ? Convert.ToInt32(reader["skill"]) : (int?)null,
                                Userlanguage = Convert.ToString(reader["userlanguage"]),
                                CityID = reader["cityid"] != DBNull.Value ? Convert.ToInt32(reader["cityid"]) : (int?)null,
                                Description = Convert.ToString(reader["description"]),
                            });
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return allBands.Count >= 1 ? allBands : null;
        }

        public Band Authenticate(string bandnameOrEmail, string passwordhash)
        {
            try
            {
                MySqlCommand cmdAuthenticateBand = this._connection.CreateCommand();
                cmdAuthenticateBand.CommandText = "SELECT * FROM bands WHERE((bandname = @bandnameOrEMail) AND (passwordhash = sha2(@passwordhash, 256)) OR ((email = @bandnameOrEMail) AND (passwordhash = sha2(@passwordhash, 256))))";
                cmdAuthenticateBand.Parameters.AddWithValue("bandnameOrEmail", bandnameOrEmail);
                cmdAuthenticateBand.Parameters.AddWithValue("passwordhash", passwordhash);
                using (MySqlDataReader reader = cmdAuthenticateBand.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        return new Band()
                        {
                            BandID = Convert.ToInt32(reader["bandid"]),
                            Email = Convert.ToString(reader["email"]),
                            PasswordHash = Convert.ToString(reader["passwordhash"]),
                            Bandname = Convert.ToString(reader["bandname"]),
                            ClosedGroup = Convert.ToBoolean(reader["closedGroup"]),
                            MemberCount = reader["membercount"] != DBNull.Value ? Convert.ToInt32(reader["membercount"]) : (int?)null,
                            Skill = reader["skill"] != DBNull.Value ? Convert.ToInt32(reader["skill"]) : (int?)null,
                            Userlanguage = Convert.ToString(reader["userlanguage"]),
                            CityID = reader["cityid"] != DBNull.Value ? Convert.ToInt32(reader["cityid"]) : (int?)null,
                            Description = Convert.ToString(reader["description"]),
                        };
                    }
                    else
                    {
                        return null;
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool InsertBand(Band bandToInsert)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdInsertBand = this._connection.CreateCommand();
                cmdInsertBand.CommandText = "INSERT INTO bands VALUES(null, @email, sha2(@passwordhash, 256), @bandname, @closedgroup, @membercount,@skill,  @userlanguage, @cityid, @description)";
                cmdInsertBand.Parameters.AddWithValue("email", bandToInsert.Email);
                cmdInsertBand.Parameters.AddWithValue("passwordhash", bandToInsert.PasswordHash);
                cmdInsertBand.Parameters.AddWithValue("bandname", bandToInsert.Bandname);
                cmdInsertBand.Parameters.AddWithValue("closedgroup", bandToInsert.ClosedGroup);
                cmdInsertBand.Parameters.AddWithValue("membercount", bandToInsert.MemberCount);
                cmdInsertBand.Parameters.AddWithValue("skill", bandToInsert.Skill);
                cmdInsertBand.Parameters.AddWithValue("userlanguage", bandToInsert.Userlanguage);
                cmdInsertBand.Parameters.AddWithValue("cityid", bandToInsert.CityID);
                cmdInsertBand.Parameters.AddWithValue("description", bandToInsert.Description);

                return cmdInsertBand.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public bool ChangeBandData(Band newBandData)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdChangeBandData = this._connection.CreateCommand();
                cmdChangeBandData.CommandText = "UPDATE bands SET email=@email, passwordhash= sha2(@passwordhash, 256), bandname=@bandname, closedgroup=@closedgroup,  membercount = @membercount, skill = @skill,  userlanguage = @userlanguage, cityid=@cityid, description=@description WHERE bandid=@bandid";
                cmdChangeBandData.Parameters.AddWithValue("bandid", newBandData.BandID);
                cmdChangeBandData.Parameters.AddWithValue("email", newBandData.Email);
                cmdChangeBandData.Parameters.AddWithValue("passwordhash", newBandData.PasswordHash);
                cmdChangeBandData.Parameters.AddWithValue("bandname", newBandData.Bandname);
                cmdChangeBandData.Parameters.AddWithValue("closedgroup", newBandData.ClosedGroup);
                cmdChangeBandData.Parameters.AddWithValue("membercount", newBandData.MemberCount);
                cmdChangeBandData.Parameters.AddWithValue("skill", newBandData.Skill);
                cmdChangeBandData.Parameters.AddWithValue("userlanguage", newBandData.Userlanguage);
                cmdChangeBandData.Parameters.AddWithValue("cityid", newBandData.CityID);
                cmdChangeBandData.Parameters.AddWithValue("description", newBandData.Description);

                return cmdChangeBandData.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public bool ChangeBandDataWithGenres(BandModel newBandData)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {

                MySqlCommand cmdChangeBandData = this._connection.CreateCommand();
                cmdChangeBandData.CommandText = "UPDATE bands SET email=@email, passwordhash= sha2(@passwordhash, 256), bandname=@bandname, closedgroup=@closedgroup,  membercount = @membercount, skill = @skill,  userlanguage = @userlanguage, cityid=@cityid, description=@description WHERE bandid=@bandid";
                cmdChangeBandData.Parameters.AddWithValue("bandid", newBandData.Band.BandID);
                cmdChangeBandData.Parameters.AddWithValue("email", newBandData.Band.Email);
                cmdChangeBandData.Parameters.AddWithValue("passwordhash", newBandData.Band.PasswordHash);
                cmdChangeBandData.Parameters.AddWithValue("bandname", newBandData.Band.Bandname);
                cmdChangeBandData.Parameters.AddWithValue("closedgroup", newBandData.Band.ClosedGroup);
                cmdChangeBandData.Parameters.AddWithValue("membercount", newBandData.Band.MemberCount);
                cmdChangeBandData.Parameters.AddWithValue("skill", newBandData.Band.Skill);
                cmdChangeBandData.Parameters.AddWithValue("userlanguage", newBandData.Band.Userlanguage);
                cmdChangeBandData.Parameters.AddWithValue("cityid", newBandData.City.CityID);
                cmdChangeBandData.Parameters.AddWithValue("description", newBandData.Band.Description);

                cmdChangeBandData.ExecuteNonQuery();

                MySqlCommand cmdChangeDeleteBandGenres = this._connection.CreateCommand();
                cmdChangeDeleteBandGenres.CommandText = "DELETE FROM BandGenres WHERE bandid = @bandid";
                cmdChangeDeleteBandGenres.Parameters.AddWithValue("bandid", newBandData.Band.BandID);

                cmdChangeDeleteBandGenres.ExecuteNonQuery();

                foreach (Genre ge in newBandData.NewChosenGenres)
                {
                    MySqlCommand cmdChangeInsertBandGenres = this._connection.CreateCommand();
                    cmdChangeInsertBandGenres.CommandText = "INSERT INTO BandGenres VALUES(@bandid, @genreid);";
                    cmdChangeInsertBandGenres.Parameters.AddWithValue("bandid", newBandData.Band.BandID);
                    cmdChangeInsertBandGenres.Parameters.AddWithValue("genreid", ge.GenreID);

                    cmdChangeInsertBandGenres.ExecuteNonQuery();
                }
                return cmdChangeBandData.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public bool DeleteBand(int bandIdToDelete)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdDeleteBand = this._connection.CreateCommand();
                cmdDeleteBand.CommandText = "DELETE FROM bands WHERE bandid = @bandIdToDelete";
                cmdDeleteBand.Parameters.AddWithValue("bandIdToDelete", bandIdToDelete);

                return cmdDeleteBand.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public void UpdateMemberCount(int membercount, int bandId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return;
            }
            try
            {
                MySqlCommand cmdUpdateMemberCount = this._connection.CreateCommand();
                cmdUpdateMemberCount.CommandText = "UPDATE bands SET membercount = @membercount WHERE bandid = @bandid";
                cmdUpdateMemberCount.Parameters.AddWithValue("membercount", membercount);
                cmdUpdateMemberCount.Parameters.AddWithValue("bandid", bandId);

                cmdUpdateMemberCount.ExecuteNonQuery();
            }
            catch (MySqlException)
            {
                throw;
            }

        }
        public void IncreaseMemberCount(int bandId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return;
            }
            try
            {
                MySqlCommand cmdIncreaseMemberCount = this._connection.CreateCommand();
                cmdIncreaseMemberCount.CommandText = @"SELECT membercount from bands where bandid = @bandId;
                                                        UPDATE bands set membercount = membercount+ 1 where bandId = @bandId;";
                cmdIncreaseMemberCount.Parameters.AddWithValue("bandId", bandId);
                cmdIncreaseMemberCount.ExecuteNonQuery();
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public void DecreaseMemberCount(int bandId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return;
            }
            try
            {
                MySqlCommand cmdDecreaseMemberCount = this._connection.CreateCommand();
                cmdDecreaseMemberCount.CommandText = @"SELECT membercount from bands where bandid = @bandId;
                                                        UPDATE bands set membercount = membercount-1 where bandId = @bandId;";
                cmdDecreaseMemberCount.Parameters.AddWithValue("bandId", bandId);
                cmdDecreaseMemberCount.ExecuteNonQuery();
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public BandModel GetBandModel(Band band)
        {
            try
            {
                cityRepository = new CityRepository();
                cityRepository.Open();
                City city = cityRepository.GetCity(band.CityID);

                bandMembersRepository = new BandMembersRepository();
                bandMembersRepository.Open();
                List<Musician> musicians = bandMembersRepository.GetBandMember(band.BandID);

                bandGenreRepository = new BandGenreRepository();
                bandGenreRepository.Open();
                List<Genre> genres = bandGenreRepository.GetBandGenre(band.BandID);
                BandModel bandModel = new BandModel(band, city, musicians, genres, null, null);

                genreRepository = new GenreRepository();
                genreRepository.Open();
                List<Genre> g = genreRepository.GetAllGenres();
                bandModel.GenreSelectList = new System.Web.Mvc.MultiSelectList(g, "GenreID", band.Userlanguage);

                return bandModel;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public List<BandSavedSearchModel> GetAllBandSavedSearches(int bandid)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<BandSavedSearchModel> allBandSavedSearches = new List<BandSavedSearchModel>();
            try
            {
                MySqlCommand cmdSelectAllBandSavedSearches = this._connection.CreateCommand();
                cmdSelectAllBandSavedSearches.CommandText = "SELECT * FROM bandmember_searchproperties where bandid = @bandid";
                cmdSelectAllBandSavedSearches.Parameters.AddWithValue("bandid", bandid);
                List<BandMember_SearchProperty> SearchProperties = new List<BandMember_SearchProperty>();
                using (MySqlDataReader reader = cmdSelectAllBandSavedSearches.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        SearchProperties.Add(
                            new BandMember_SearchProperty(
                                Convert.ToInt32(reader["BandMember_SearchID"]),
                                Convert.ToInt32(reader["bandID"]),
                                reader["skillmin"] != DBNull.Value ? Convert.ToInt32(reader["skillmin"]) : (int?)null,
                                reader["skillmax"] != DBNull.Value ? Convert.ToInt32(reader["skillmax"]) : (int?)null,
                                reader["CityDistance_max"] != DBNull.Value ? Convert.ToInt32(reader["CityDistance_max"]) : (int?)null)
                        );
                    }
                }
                List<Instrument> InstrumentListe = new List<Instrument>();
                foreach (BandMember_SearchProperty sp in SearchProperties)
                {
                    InstrumentListe = new List<Instrument>();
                    MySqlCommand cmdSelectAllInstrumentsForSavedSearches = this._connection.CreateCommand();
                    cmdSelectAllInstrumentsForSavedSearches.CommandText = "SELECT * FROM BandMember_SearchInstruments JOIN Instruments using (InstrumentId) where BandMember_SearchID = @BandMember_SearchID";
                    cmdSelectAllInstrumentsForSavedSearches.Parameters.AddWithValue("BandMember_SearchID", sp.BandMember_SearchId);
                    using (MySqlDataReader reader2 = cmdSelectAllInstrumentsForSavedSearches.ExecuteReader())
                    {
                        while (reader2.Read())
                        {
                            InstrumentListe.Add(new Instrument(Convert.ToInt32(reader2["instrumentid"]), 0, Convert.ToString(reader2["de"]), Convert.ToString(reader2["en"])));
                        }
                        allBandSavedSearches.Add(new BandSavedSearchModel(sp, InstrumentListe));
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return allBandSavedSearches.Count >= 1 ? allBandSavedSearches : null;
        }

        public BandSavedSearchModel GetBandSavedSearch(int searchId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }
            try
            {
                BandMember_SearchProperty sp = null;
                MySqlCommand cmdSelectBandSavedSearch = this._connection.CreateCommand();
                cmdSelectBandSavedSearch.CommandText = "SELECT * FROM bandmember_searchproperties WHERE BandMember_searchId = @searchId";
                cmdSelectBandSavedSearch.Parameters.AddWithValue("searchId", searchId);
                using (MySqlDataReader reader = cmdSelectBandSavedSearch.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        sp = new BandMember_SearchProperty(
                            Convert.ToInt32(reader["BandMember_SearchID"]),
                            Convert.ToInt32(reader["BandID"]),
                            reader["skillmin"] != DBNull.Value ? Convert.ToInt32(reader["skillmin"]) : (int?)null,
                            reader["skillmax"] != DBNull.Value ? Convert.ToInt32(reader["skillmax"]) : (int?)null,
                            reader["CityDistance_max"] != DBNull.Value ? Convert.ToInt32(reader["CityDistance_max"]) : (int?)null
                            );
                    }
                }
                if (sp != null)
                {
                    List<Instrument> InstrumentListe = new List<Instrument>();

                    MySqlCommand cmdSelectAllInstrumentsForSavedSearch = this._connection.CreateCommand();
                    cmdSelectAllInstrumentsForSavedSearch.CommandText = "SELECT * FROM BandMember_SearchInstruments JOIN Instruments using (InstrumentId) where BandMember_SearchID = @BandMember_SearchID";
                    cmdSelectAllInstrumentsForSavedSearch.Parameters.AddWithValue("BandMember_SearchID", sp.BandMember_SearchId);
                    using (MySqlDataReader reader2 = cmdSelectAllInstrumentsForSavedSearch.ExecuteReader())
                    {
                        while (reader2.Read())
                        {
                            InstrumentListe.Add(new Instrument(Convert.ToInt32(reader2["genreid"]), Convert.ToInt32(reader2["skill"]), Convert.ToString(reader2["de"]), Convert.ToString(reader2["en"])));
                        }
                        return new BandSavedSearchModel(sp, InstrumentListe);

                    }
                }

            }
            catch (MySqlException)
            {
                throw;
            }
            return null;
        }

        public bool DeleteBandSavedSearch(int SavedSearchIdToDelete)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdDeleteBandSavedSearch = this._connection.CreateCommand();
                cmdDeleteBandSavedSearch.CommandText = "DELETE FROM bandMember_SearchProperties WHERE bandmember_searchid = @SavedSearchidtodelete";
                cmdDeleteBandSavedSearch.Parameters.AddWithValue("SavedSearchidtodelete", SavedSearchIdToDelete);

                MySqlCommand cmdDeleteBandSavedSearchInstrument = this._connection.CreateCommand();
                cmdDeleteBandSavedSearchInstrument.CommandText = "DELETE FROM BandMember_searchInstruments WHERE bandmember_searchid = @SavedSearchidtodelete";
                cmdDeleteBandSavedSearchInstrument.Parameters.AddWithValue("SavedSearchidtodelete", SavedSearchIdToDelete);

                bool ergebnis = cmdDeleteBandSavedSearch.ExecuteNonQuery() == 1;
                cmdDeleteBandSavedSearchInstrument.ExecuteNonQuery();
                return ergebnis;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public Band GetBandUsingBandname(string Bandname)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }
            try
            {
                MySqlCommand cmdSelectBands = this._connection.CreateCommand();
                cmdSelectBands.CommandText = "SELECT * FROM bands WHERE bandname = @bandname";
                cmdSelectBands.Parameters.AddWithValue("bandname", Bandname);
                using (MySqlDataReader reader = cmdSelectBands.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        return new Band()
                        {
                            BandID = Convert.ToInt32(reader["bandid"]),
                            Email = Convert.ToString(reader["email"]),
                            PasswordHash = Convert.ToString(reader["passwordhash"]),
                            Bandname = Convert.ToString(reader["bandname"]),
                            ClosedGroup = Convert.ToBoolean(reader["closedgroup"]),
                            MemberCount = reader["membercount"] != DBNull.Value ? Convert.ToInt32(reader["membercount"]) : (int?)null,
                            Skill = reader["skill"] != DBNull.Value ? Convert.ToInt32(reader["skill"]) : (int?)null,
                            Userlanguage = Convert.ToString(reader["userlanguage"]),
                            CityID = reader["cityid"] != DBNull.Value ? Convert.ToInt32(reader["cityid"]) : (int?)null,
                            Description = Convert.ToString(reader["description"]),
                        };
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return null;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musikernetzwerk.Models.DB
{
    interface ICountryRepository : ISQLConnection
    {
        Country GetCountry(int countryID);
        List<Country> GetAllCountries();
    }
}

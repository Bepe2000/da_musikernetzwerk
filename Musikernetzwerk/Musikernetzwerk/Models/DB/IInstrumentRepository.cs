﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musikernetzwerk.Models.DB
{
    interface IInstrumentRepository : ISQLConnection
    {
        Instrument GetInstrument(int instrumentId);
        List<Instrument> GetAllInstruments();
    }
}

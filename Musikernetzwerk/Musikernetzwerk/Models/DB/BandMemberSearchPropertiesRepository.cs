﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models.DB
{
    public class BandMemberSearchPropertiesRepository : SQLConnection, IBandMemberSearchPropertiesRepository
    {
        public bool InsertBandMemberSearchProperty(BandMember_SearchProperty bandmembersearchproperty)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdInsertBandMemberSearchProperty = this._connection.CreateCommand();
                cmdInsertBandMemberSearchProperty.CommandText = "INSERT INTO BandMember_SearchProperties VALUES(null, @BandID, @Skillmin, @Skillmax, @CityDistance_max)";
                cmdInsertBandMemberSearchProperty.Parameters.AddWithValue("BandID", bandmembersearchproperty.BandID);
                cmdInsertBandMemberSearchProperty.Parameters.AddWithValue("Skillmin", bandmembersearchproperty.SkillMin);
                cmdInsertBandMemberSearchProperty.Parameters.AddWithValue("Skillmax", bandmembersearchproperty.SkillMax);
                cmdInsertBandMemberSearchProperty.Parameters.AddWithValue("CityDistance_max", bandmembersearchproperty.CityDistance_max);

                return cmdInsertBandMemberSearchProperty.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public bool DeleteBandMemberSearchProperty(int bandmembersearchpropertyIdToDelete)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdDeleteBandMemberSearchProperty = this._connection.CreateCommand();
                cmdDeleteBandMemberSearchProperty.CommandText = "DELETE FROM BandMember_SearchProperties WHERE BandMember_SearchID = @bandmembersearchpropertyIdToDelete";
                cmdDeleteBandMemberSearchProperty.Parameters.AddWithValue("bandmembersearchpropertyIdToDelete", bandmembersearchpropertyIdToDelete);

                return cmdDeleteBandMemberSearchProperty.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public BandMember_SearchProperty GetBandMemberSearchProperty(int bandmembersearchpropertyId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }
            try
            {
                MySqlCommand cmdSelectBandMemberSearchProperty = this._connection.CreateCommand();
                cmdSelectBandMemberSearchProperty.CommandText = "SELECT * FROM BandMember_SearchProperties WHERE BandMember_SearchID = @bandmembersearchpropertyId";
                cmdSelectBandMemberSearchProperty.Parameters.AddWithValue("bandmembersearchpropertyId", bandmembersearchpropertyId);
                using (MySqlDataReader reader = cmdSelectBandMemberSearchProperty.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        return new BandMember_SearchProperty()
                        {
                            BandMember_SearchId = reader["bandmember_searchid"] != DBNull.Value ? Convert.ToInt32(reader["bandmember_searchid"]) : (int?)null,
                            BandID = reader["bandid"] != DBNull.Value ? Convert.ToInt32(reader["bandid"]) : (int?)null,
                            SkillMin = reader["skillmin"] != DBNull.Value ? Convert.ToInt32(reader["skillmin"]) : (int?)null,
                            SkillMax = reader["skillmax"] != DBNull.Value ? Convert.ToInt32(reader["skillmax"]) : (int?)null,
                            CityDistance_max = reader["citydistance_max"] != DBNull.Value ? Convert.ToDouble(reader["citydistance_max"]) : (double?)null
                        };
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return null;
        }

        public List<BandMember_SearchProperty> GetAllBandMemberSearchProperties()
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<BandMember_SearchProperty> allBandMemberSearchProperties = new List<BandMember_SearchProperty>();
            try
            {
                MySqlCommand cmdSelectAllBandMemberSearchProperties = this._connection.CreateCommand();
                cmdSelectAllBandMemberSearchProperties.CommandText = "SELECT * FROM BandMember_SearchProperties";
                using (MySqlDataReader reader = cmdSelectAllBandMemberSearchProperties.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        allBandMemberSearchProperties.Add(
                            new BandMember_SearchProperty
                            {
                                BandMember_SearchId = reader["bandmember_searchid"] != DBNull.Value ? Convert.ToInt32(reader["bandmember_searchid"]) : (int?)null,
                                BandID = reader["bandid"] != DBNull.Value ? Convert.ToInt32(reader["bandid"]) : (int?)null,
                                SkillMin = reader["skillmin"] != DBNull.Value ? Convert.ToInt32(reader["skillmin"]) : (int?)null,
                                SkillMax = reader["skillmax"] != DBNull.Value ? Convert.ToInt32(reader["skillmax"]) : (int?)null,
                                CityDistance_max = reader["citydistance_max"] != DBNull.Value ? Convert.ToDouble(reader["citydistance_max"]) : (double?)null
                            });
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return allBandMemberSearchProperties.Count >= 1 ? allBandMemberSearchProperties : null;
        }

        public List<BandMember_SearchProperty> GetAllBandMemberSearchPropertiesOfBand(int bandid)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<BandMember_SearchProperty> allBandMemberSearchProperties = new List<BandMember_SearchProperty>();
            try
            {
                MySqlCommand cmdSelectAllBandMemberSearchPropertiesOfBand = this._connection.CreateCommand();
                cmdSelectAllBandMemberSearchPropertiesOfBand.CommandText = "SELECT * FROM BandMember_SearchProperties where bandid = @bandid";
                cmdSelectAllBandMemberSearchPropertiesOfBand.Parameters.AddWithValue("bandid", bandid);
                using (MySqlDataReader reader = cmdSelectAllBandMemberSearchPropertiesOfBand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        allBandMemberSearchProperties.Add(
                            new BandMember_SearchProperty
                            {
                                BandMember_SearchId = reader["bandmember_searchid"] != DBNull.Value ? Convert.ToInt32(reader["bandmember_searchid"]) : (int?)null,
                                BandID = reader["bandid"] != DBNull.Value ? Convert.ToInt32(reader["bandid"]) : (int?)null,
                                SkillMin = reader["skillmin"] != DBNull.Value ? Convert.ToInt32(reader["skillmin"]) : (int?)null,
                                SkillMax = reader["skillmax"] != DBNull.Value ? Convert.ToInt32(reader["skillmax"]) : (int?)null,
                                CityDistance_max = reader["citydistance_max"] != DBNull.Value ? Convert.ToDouble(reader["citydistance_max"]) : (double?)null
                            });
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return allBandMemberSearchProperties.Count >= 1 ? allBandMemberSearchProperties : null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musikernetzwerk.Models.DB
{
    interface IMusicianInstrumentsRepository : ISQLConnection
    {
        bool InsertMusicianInstrument(int musicianId, int instrumentId, int skill);
        bool DeleteMusicianInstrument(int musicianId, int instrumentId);
        List<Instrument> GetMusicianInstruments(int musicianId);
        List<Musician> GetMusiciansByInstruments(int instrumentId);
    }
}

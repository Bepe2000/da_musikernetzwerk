﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musikernetzwerk.Models.DB
{
    interface IMusicianBandSearchGenresRepository : ISQLConnection
    {
        bool InsertBandSearchGenre(MusicianBand_SearchGenre bandsearchgenre);
        bool DeleteBandSearchGenre(int bandsearchgenreIdToDelete);
        MusicianBand_SearchGenre GetBandSearchGenre(int bandsearchgenreId);
        List<MusicianBand_SearchGenre> GetAllBandSearchGenres();
    }
}

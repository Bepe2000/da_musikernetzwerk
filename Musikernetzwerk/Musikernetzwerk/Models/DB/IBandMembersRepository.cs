﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musikernetzwerk.Models.DB
{
    interface IBandMembersRepository : ISQLConnection
    {
        bool InsertBandMember(int bandId, int musicianId);
        bool DeleteBandMember(int bandId, int musicianId);
        List<Musician> GetBandMember(int bandId);
        List<Band> GetBandsByMusicians(int musicianid);
    }
}

﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models.DB
{
    public class MusicianInstrumentsRepository : SQLConnection, IMusicianInstrumentsRepository
    {
        public bool InsertMusicianInstrument(int musicianId, int instrumentId, int skill)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdInsertMusicianInstrument = this._connection.CreateCommand();
                cmdInsertMusicianInstrument.CommandText = "INSERT INTO MusicianInstruments VALUES(@musicianId, @instrumentId, @skill)";
                cmdInsertMusicianInstrument.Parameters.AddWithValue("musicianId", musicianId);
                cmdInsertMusicianInstrument.Parameters.AddWithValue("instrumentId", instrumentId);
                cmdInsertMusicianInstrument.Parameters.AddWithValue("skill", skill);
                return cmdInsertMusicianInstrument.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public bool DeleteMusicianInstrument(int musicianId, int instrumentId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdDeleteMusicianInstrument = this._connection.CreateCommand();
                cmdDeleteMusicianInstrument.CommandText = "DELETE FROM MusicianInstruments WHERE musicianid = @musicianid and instrumentid = @instrumentid";
                cmdDeleteMusicianInstrument.Parameters.AddWithValue("musicianId", musicianId);
                cmdDeleteMusicianInstrument.Parameters.AddWithValue("instrumentid", instrumentId);

                return cmdDeleteMusicianInstrument.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public List<Instrument> GetMusicianInstruments(int musicianId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<Instrument> allInstruments = new List<Instrument>();
            try
            {
                MySqlCommand cmdSelectMusicianInstruments = this._connection.CreateCommand();
                cmdSelectMusicianInstruments.CommandText = "SELECT * from MusicianInstruments JOIN instruments USING (InstrumentId) where musicianid = @musicianId";
                cmdSelectMusicianInstruments.Parameters.AddWithValue("musicianId", musicianId);
                using (MySqlDataReader reader = cmdSelectMusicianInstruments.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        allInstruments.Add(
                            new Instrument(reader["instrumentid"] != DBNull.Value ? Convert.ToInt32(reader["instrumentid"]) : (int?)null,
                                 reader["skill"] != DBNull.Value ? Convert.ToInt32(reader["skill"]) : (int?)null,
                                 Convert.ToString(reader["de"]),
                                 Convert.ToString(reader["en"])
                            ));
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return allInstruments.Count >= 1 ? allInstruments : null;
        }

        public List<Musician> GetMusiciansByInstruments(int instrumentId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<Musician> musiciansByInstruments = new List<Musician>();
            try
            {
                MySqlCommand cmdSelectInstrumentByMusician = this._connection.CreateCommand();
                cmdSelectInstrumentByMusician.CommandText = "SELECT * from MusicianInstruments JOIN musicians USING (MusicianId) where instrumentid = @instrumentId";
                cmdSelectInstrumentByMusician.Parameters.AddWithValue("instrumentId", instrumentId);
                using (MySqlDataReader reader = cmdSelectInstrumentByMusician.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        musiciansByInstruments.Add(
                            new Musician
                            {
                                MusicianID = Convert.ToInt32(reader["musicianid"]),
                                ArtistName = Convert.ToString(reader["artistname"])
                            }
                            );
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return musiciansByInstruments.Count >= 1 ? musiciansByInstruments : null;
        }
    }
}
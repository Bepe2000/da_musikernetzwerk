﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Musikernetzwerk.Models.ViewModels;

namespace Musikernetzwerk.Models.DB
{
    interface IMusicianRepository : ISQLConnection
    {
        bool InsertMusician(Musician musicianToInsert);
        bool DeleteMusician(int musicianIdToDelete);
        Musician Authenticate(string artistnameOrEMail, string password);
        Musician GetMusician(int musicianId);
        List<Musician> GetAllMusicians();
        bool ChangeMusicianData(Musician newMusicianData);
        bool ChangeMusicianDataWithInstruments(MusicianModel newMusicianData);
        MusicianModel GetMusicianModel(Musician musician);
        List<MusicianSavedSearchModel> GetAllMusicianSavedSearches(int musicianid);
        MusicianSavedSearchModel GetMusicianSavedSearch(int searchId);
        bool DeleteMusicianSavedSearch(int SavedSearchIdToDelete);
        List<City> GetCities(string cityname, int postalcode);
        Musician GetMusicianIdByArtistname(string artistname);
    }
}

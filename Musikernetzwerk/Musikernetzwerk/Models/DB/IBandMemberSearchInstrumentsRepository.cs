﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musikernetzwerk.Models.DB
{
    interface IBandMemberSearchInstrumentsRepository : ISQLConnection
    {
        bool InsertBandMemberSearchInstrument(BandMember_SearchInstrument bandmembersearchinstrument);
        bool DeleteBandMemberSearchInstrument(int bandmembersearchinstrumentIdToDelete);
        BandMember_SearchInstrument GetBandMemberSearchInstrument(int bandmembersearchinstrumentId);
        List<BandMember_SearchInstrument> GetAllBandMemberSearchInstruments();
    }
}

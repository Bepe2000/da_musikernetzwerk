﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musikernetzwerk.Models.DB
{
    interface IMusicianGenreRepository : ISQLConnection
    {
        bool InsertMusicianGenre(int musicianId, int genreId);
        bool DeleteMusicianGenre(int musicianId, int genreId);
        List<Genre> GetMusicianGenre(int musicianId);
        List<Musician> GetMusiciansByGenre(int genreId);
    }
}

﻿using Musikernetzwerk.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musikernetzwerk.Models.DB
{
    interface IBandRepository : ISQLConnection
    {
        bool InsertBand(Band bandToInsert);
        bool DeleteBand(int bandIdToDelete);
        Band Authenticate(string bandnameOrEMail, string password);
        Band GetBand(int bandId);
        List<Band> GetAllBands();
        bool ChangeBandData(Band newBandData);
        bool ChangeBandDataWithGenres(BandModel newBandData);
        void UpdateMemberCount(int membercount, int bandId);
        void IncreaseMemberCount(int bandId);
        void DecreaseMemberCount(int bandId);
        BandModel GetBandModel(Band band);
        List<BandSavedSearchModel> GetAllBandSavedSearches(int bandid);
        BandSavedSearchModel GetBandSavedSearch(int searchId);
        bool DeleteBandSavedSearch(int SavedSearchIdToDelete);
        Band GetBandUsingBandname(string Bandname);
    }
}

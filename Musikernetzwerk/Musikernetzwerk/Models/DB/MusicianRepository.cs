﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using Musikernetzwerk.Models.DB;
using System.Data;
using Musikernetzwerk.Models.ViewModels;

namespace Musikernetzwerk.Models.DB
{
    public class MusicianRepository : SQLConnection, IMusicianRepository
    {
        ICityRepository cityRepository;
        IBandMembersRepository bandMembersRepository;
        IMusicianInstrumentsRepository musicianInstrumentsRepository;
        IInstrumentRepository instrumentRepository;
        public Musician GetMusician(int musicianid)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }
            try
            {
                MySqlCommand cmdSelectMusicians = this._connection.CreateCommand();
                cmdSelectMusicians.CommandText = "SELECT * FROM musicians WHERE musicianid = @musicianid";
                cmdSelectMusicians.Parameters.AddWithValue("musicianid", musicianid);
                using (MySqlDataReader reader = cmdSelectMusicians.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        return new Musician()
                        {
                            MusicianID = Convert.ToInt32(reader["musicianid"]),
                            Email = Convert.ToString(reader["email"]),
                            PasswordHash = Convert.ToString(reader["passwordhash"]),
                            FirstName = Convert.ToString(reader["firstname"]),
                            LastName = Convert.ToString(reader["lastname"]),
                            Birthdate = reader["birthdate"] != DBNull.Value ? Convert.ToDateTime(reader["birthdate"]) : (DateTime?)null,
                            Skill = reader["skill"] != DBNull.Value ? Convert.ToInt32(reader["skill"]) : (int?)null,
                            Userlanguage = Convert.ToString(reader["userlanguage"]),
                            CityID = reader["cityid"] != DBNull.Value ? Convert.ToInt32(reader["cityid"]) : (int?)null,
                            ArtistName = Convert.ToString(reader["artistname"]),
                        };
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return null;
        }

        public List<Musician> GetAllMusicians()
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<Musician> allMusicians = new List<Musician>();
            try
            {
                MySqlCommand cmdSelectAllMusicians = this._connection.CreateCommand();
                cmdSelectAllMusicians.CommandText = "SELECT * FROM musicians";

                using (MySqlDataReader reader = cmdSelectAllMusicians.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        allMusicians.Add(
                            new Musician
                            {
                                MusicianID = Convert.ToInt32(reader["musicianid"]),
                                Email = Convert.ToString(reader["email"]),
                                PasswordHash = Convert.ToString(reader["passwordhash"]),
                                FirstName = Convert.ToString(reader["firstname"]),
                                LastName = Convert.ToString(reader["lastname"]),
                                Birthdate = reader["birthdate"] != DBNull.Value ? Convert.ToDateTime(reader["birthdate"]) : (DateTime?)null,
                                Skill = reader["skill"] != DBNull.Value ? Convert.ToInt32(reader["skill"]) : (int?)null,
                                Userlanguage = Convert.ToString(reader["userlanguage"]),
                                CityID = reader["cityid"] != DBNull.Value ? Convert.ToInt32(reader["cityid"]) : (int?)null,
                                ArtistName = Convert.ToString(reader["artistname"]),

                            });
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return allMusicians.Count >= 1 ? allMusicians : null;
        }

        public Musician Authenticate(string artistnameOrEmail, string passwordhash)
        {
            try
            {
                MySqlCommand cmdAuthenticateMusician = this._connection.CreateCommand();
                cmdAuthenticateMusician.CommandText = "SELECT * FROM musicians WHERE ((artistname= @artistnameOrEmail) AND (passwordhash = sha2(@passwordhash, 256)) OR ((email = @artistnameOrEMail) AND (passwordhash = sha2(@passwordhash, 256))))";
                cmdAuthenticateMusician.Parameters.AddWithValue("artistnameOrEmail", artistnameOrEmail);
                cmdAuthenticateMusician.Parameters.AddWithValue("passwordhash", passwordhash);
                using (MySqlDataReader reader = cmdAuthenticateMusician.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        return new Musician()
                        {
                            MusicianID = Convert.ToInt32(reader["musicianid"]),
                            Email = Convert.ToString(reader["email"]),
                            PasswordHash = Convert.ToString(reader["passwordhash"]),
                            FirstName = Convert.ToString(reader["firstname"]),
                            LastName = Convert.ToString(reader["lastname"]),
                            Birthdate = reader["birthdate"] != DBNull.Value ? Convert.ToDateTime(reader["birthdate"]) : (DateTime?)null,
                            Skill = reader["skill"] != DBNull.Value ? Convert.ToInt32(reader["skill"]) : (int?)null,
                            Userlanguage = Convert.ToString(reader["userlanguage"]),
                            CityID = reader["cityid"] != DBNull.Value ? Convert.ToInt32(reader["cityid"]) : (int?)null,
                            ArtistName = Convert.ToString(reader["artistname"]),
                        };
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool InsertMusician(Musician musicianToInsert)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdInsertMusician = this._connection.CreateCommand();
                cmdInsertMusician.CommandText = "INSERT INTO musicians VALUES(null, @email, sha2(@passwordhash,256), @firstname, @lastname, @birthdate, @skill, @userlanguage,@cityid, @artistname )";
                cmdInsertMusician.Parameters.AddWithValue("email", musicianToInsert.Email);
                cmdInsertMusician.Parameters.AddWithValue("passwordhash", musicianToInsert.PasswordHash);
                cmdInsertMusician.Parameters.AddWithValue("firstname", musicianToInsert.FirstName);
                cmdInsertMusician.Parameters.AddWithValue("lastname", musicianToInsert.LastName);
                cmdInsertMusician.Parameters.AddWithValue("birthdate", musicianToInsert.Birthdate);
                cmdInsertMusician.Parameters.AddWithValue("skill", musicianToInsert.Skill);
                cmdInsertMusician.Parameters.AddWithValue("userlanguage", musicianToInsert.Userlanguage);
                cmdInsertMusician.Parameters.AddWithValue("cityid", musicianToInsert.CityID);
                cmdInsertMusician.Parameters.AddWithValue("artistname", musicianToInsert.ArtistName);

                return cmdInsertMusician.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public bool ChangeMusicianData(Musician newMusicianData)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdChangeMusicianData = this._connection.CreateCommand();
                cmdChangeMusicianData.CommandText = "UPDATE musicians SET email=@email, passwordhash = sha2(@passwordhash, 256), firstname=@firstname, lastname = @lastname, birthdate = @birthdate, skill=@skill, userlanguage = @userlanguage, cityid=@cityid, artistname = @artistname WHERE musicianid=@musicianid";
                cmdChangeMusicianData.Parameters.AddWithValue("musicianid", newMusicianData);
                cmdChangeMusicianData.Parameters.AddWithValue("email", newMusicianData.Email);
                cmdChangeMusicianData.Parameters.AddWithValue("passwordhash", newMusicianData.PasswordHash);
                cmdChangeMusicianData.Parameters.AddWithValue("firstname", newMusicianData.FirstName);
                cmdChangeMusicianData.Parameters.AddWithValue("lastname", newMusicianData.LastName);
                cmdChangeMusicianData.Parameters.AddWithValue("birthdate", newMusicianData.Birthdate);
                cmdChangeMusicianData.Parameters.AddWithValue("skill", newMusicianData.Skill);
                cmdChangeMusicianData.Parameters.AddWithValue("userlanguage", newMusicianData.Userlanguage);
                cmdChangeMusicianData.Parameters.AddWithValue("cityid", newMusicianData.CityID);
                cmdChangeMusicianData.Parameters.AddWithValue("artistname", newMusicianData.ArtistName);

                return cmdChangeMusicianData.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public bool ChangeMusicianDataWithInstruments(MusicianModel newMusicianData)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdChangeMusicianData = this._connection.CreateCommand();
                cmdChangeMusicianData.CommandText = "UPDATE musicians SET email=@email, passwordhash = sha2(@passwordhash, 256), firstname=@firstname, lastname = @lastname, birthdate = @birthdate, skill=@skill, userlanguage = @userlanguage, cityid=@cityid, artistname = @artistname WHERE musicianid=@musicianid";
                cmdChangeMusicianData.Parameters.AddWithValue("musicianid", newMusicianData.Musician.MusicianID);
                cmdChangeMusicianData.Parameters.AddWithValue("email", newMusicianData.Musician.Email);
                cmdChangeMusicianData.Parameters.AddWithValue("passwordhash", newMusicianData.Musician.PasswordHash);
                cmdChangeMusicianData.Parameters.AddWithValue("firstname", newMusicianData.Musician.FirstName);
                cmdChangeMusicianData.Parameters.AddWithValue("lastname", newMusicianData.Musician.LastName);
                cmdChangeMusicianData.Parameters.AddWithValue("birthdate", newMusicianData.Musician.Birthdate);
                cmdChangeMusicianData.Parameters.AddWithValue("skill", newMusicianData.Musician.Skill);
                cmdChangeMusicianData.Parameters.AddWithValue("userlanguage", newMusicianData.Musician.Userlanguage);
                cmdChangeMusicianData.Parameters.AddWithValue("cityid", newMusicianData.City.CityID);
                cmdChangeMusicianData.Parameters.AddWithValue("artistname", newMusicianData.Musician.ArtistName);

                cmdChangeMusicianData.ExecuteNonQuery();

                MySqlCommand cmdChangeDeleteMusicianInstruments = this._connection.CreateCommand();
                cmdChangeDeleteMusicianInstruments.CommandText = "DELETE FROM MusicianInstruments WHERE musicianid = @musicianid";
                cmdChangeDeleteMusicianInstruments.Parameters.AddWithValue("musicianid", newMusicianData.Musician.MusicianID);

                cmdChangeDeleteMusicianInstruments.ExecuteNonQuery();

                foreach (Instrument inst in newMusicianData.NewChosenInstruments)
                {
                    MySqlCommand cmdChangeInsertMusicianInstruments = this._connection.CreateCommand();
                    cmdChangeInsertMusicianInstruments.CommandText = "INSERT INTO MusicianInstruments VALUES(@musicianid, @instrumentid, @skill);";
                    cmdChangeInsertMusicianInstruments.Parameters.AddWithValue("musicianid", newMusicianData.Musician.MusicianID);
                    cmdChangeInsertMusicianInstruments.Parameters.AddWithValue("instrumentid", inst.InstrumentID);
                    cmdChangeInsertMusicianInstruments.Parameters.AddWithValue("skill", inst.Skill);

                    cmdChangeInsertMusicianInstruments.ExecuteNonQuery();

                }
                return cmdChangeMusicianData.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public bool DeleteMusician(int musicianIdToDelete)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdDeleteMusician = this._connection.CreateCommand();
                cmdDeleteMusician.CommandText = "DELETE FROM musicians WHERE musicianid = @musicianidtodelete";
                cmdDeleteMusician.Parameters.AddWithValue("musicianidtodelete", musicianIdToDelete);

                return cmdDeleteMusician.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public MusicianModel GetMusicianModel(Musician musician)
        {
            try
            {
                cityRepository = new CityRepository();
                cityRepository.Open();
                City city = cityRepository.GetCity(musician.CityID);

                bandMembersRepository = new BandMembersRepository();
                bandMembersRepository.Open();
                List<Band> bands = bandMembersRepository.GetBandsByMusicians(musician.MusicianID);

                musicianInstrumentsRepository = new MusicianInstrumentsRepository();
                musicianInstrumentsRepository.Open();
                List<Instrument> instruments = musicianInstrumentsRepository.GetMusicianInstruments(musician.MusicianID);
                MusicianModel musicianModel = new MusicianModel(musician, city, bands, instruments, null, null);

                instrumentRepository = new InstrumentRepository();
                instrumentRepository.Open();
                List<Instrument> i = instrumentRepository.GetAllInstruments();
                musicianModel.InstrumentSelectList = new System.Web.Mvc.MultiSelectList(i, "InstrumentID", musician.Userlanguage);

                musicianModel.NewChosenInstrumentIDs = new int[i.Count];
                for (int index = 0; index < instruments.Count; index++)
                {
                    musicianModel.NewChosenInstrumentIDs[index] = (int)instruments[index].Skill;
                }
                return musicianModel;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public List<MusicianSavedSearchModel> GetAllMusicianSavedSearches(int musicianid)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<MusicianSavedSearchModel> allMusicianSavedSearches = new List<MusicianSavedSearchModel>();
            try
            {
                MySqlCommand cmdSelectAllMusicianSavedSearches = this._connection.CreateCommand();
                cmdSelectAllMusicianSavedSearches.CommandTimeout = 200;
                cmdSelectAllMusicianSavedSearches.CommandText = "SELECT * FROM musicianband_searchproperties where musicianid = @musicianid";
                cmdSelectAllMusicianSavedSearches.Parameters.AddWithValue("musicianid", musicianid);

                List<MusicianBand_SearchProperty> SearchProperties = new List<MusicianBand_SearchProperty>();
                using (MySqlDataReader reader = cmdSelectAllMusicianSavedSearches.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        SearchProperties.Add(
                            new MusicianBand_SearchProperty(
                                Convert.ToInt32(reader["MusicianBand_SearchID"]),
                                Convert.ToInt32(reader["musicianID"]),
                                reader["skillmin"] != DBNull.Value ? Convert.ToInt32(reader["skillmin"]) : (int?)null,
                                reader["skillmax"] != DBNull.Value ? Convert.ToInt32(reader["skillmax"]) : (int?)null,
                                reader["membercount_min"] != DBNull.Value ? Convert.ToInt32(reader["membercount_min"]) : (int?)null,
                                reader["membercount_max"] != DBNull.Value ? Convert.ToInt32(reader["membercount_max"]) : (int?)null,
                                reader["CityDistance_max"] != DBNull.Value ? Convert.ToInt32(reader["CityDistance_max"]) : (int?)null)
                        );
                    }
                }
                List<Genre> GenreListe = new List<Genre>();
                foreach (MusicianBand_SearchProperty sp in SearchProperties)
                {
                    GenreListe = new List<Genre>();
                    MySqlCommand cmdSelectAllGenresForSavedSearches = this._connection.CreateCommand();
                    cmdSelectAllGenresForSavedSearches.CommandText = "SELECT * FROM MusicianBand_SearchGenres JOIN Genres using (GenreId) where MusicianBand_SearchID = @MusicianBand_SearchID";
                    cmdSelectAllGenresForSavedSearches.Parameters.AddWithValue("MusicianBand_SearchID", sp.MusicianBand_SearchID);
                    using (MySqlDataReader reader2 = cmdSelectAllGenresForSavedSearches.ExecuteReader())
                    {
                        while (reader2.Read())
                        {
                            GenreListe.Add(new Genre(Convert.ToInt32(reader2["genreid"]), Convert.ToString(reader2["de"]), Convert.ToString(reader2["en"])));
                        }
                        allMusicianSavedSearches.Add(new MusicianSavedSearchModel(sp, GenreListe));
                    }
                }

            }
            catch (MySqlException)
            {
                throw;
            }
            return allMusicianSavedSearches.Count >= 1 ? allMusicianSavedSearches : null;
        }

        public MusicianSavedSearchModel GetMusicianSavedSearch(int searchId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }
            try
            {
                MySqlCommand cmdSelectMusicianSavedSearch = this._connection.CreateCommand();
                cmdSelectMusicianSavedSearch.CommandText = "SELECT * FROM musicianband_searchproperties WHERE MusicianBand_searchId = @searchId";
                cmdSelectMusicianSavedSearch.Parameters.AddWithValue("searchId", searchId);
                MusicianBand_SearchProperty sp = new MusicianBand_SearchProperty();
                using (MySqlDataReader reader = cmdSelectMusicianSavedSearch.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        sp = new MusicianBand_SearchProperty(
                            Convert.ToInt32(reader["MusicianBand_SearchID"]),
                            Convert.ToInt32(reader["musicianID"]),
                            reader["skillmin"] != DBNull.Value ? Convert.ToInt32(reader["skillmin"]) : (int?)null,
                            reader["skillmax"] != DBNull.Value ? Convert.ToInt32(reader["skillmax"]) : (int?)null,
                            reader["membercount_min"] != DBNull.Value ? Convert.ToInt32(reader["membercount_min"]) : (int?)null,
                            reader["membercount_max"] != DBNull.Value ? Convert.ToInt32(reader["membercount_max"]) : (int?)null,
                            reader["CityDistance_max"] != DBNull.Value ? Convert.ToInt32(reader["CityDistance_max"]) : (int?)null
                            );
                    }
                }
                List<Genre> GenreListe = new List<Genre>();
                
                MySqlCommand cmdSelectAllGenresForSavedSearch = this._connection.CreateCommand();
                cmdSelectAllGenresForSavedSearch.CommandText = "SELECT * FROM MusicianBand_SearchGenres JOIN Genres using (GenreId) where MusicianBand_SearchID = @MusicianBand_SearchID";
                cmdSelectAllGenresForSavedSearch.Parameters.AddWithValue("MusicianBand_SearchID", sp.MusicianBand_SearchID);
                using (MySqlDataReader reader2 = cmdSelectAllGenresForSavedSearch.ExecuteReader())
                {
                    while (reader2.Read())
                    {
                        GenreListe.Add(new Genre(Convert.ToInt32(reader2["genreid"]), Convert.ToString(reader2["de"]), Convert.ToString(reader2["en"])));
                    }
                    return new MusicianSavedSearchModel(sp, GenreListe);                  
                }
            }
            catch (MySqlException)
            {
                throw;
            }           
        }

        public bool DeleteMusicianSavedSearch(int SavedSearchIdToDelete)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdDeleteMusicianSavedSearch = this._connection.CreateCommand();
                cmdDeleteMusicianSavedSearch.CommandText = "DELETE FROM musicianBand_SearchProperties WHERE musicianband_searchid = @SavedSearchidtodelete";
                cmdDeleteMusicianSavedSearch.Parameters.AddWithValue("SavedSearchidtodelete", SavedSearchIdToDelete);

                MySqlCommand cmdDeleteMusicianSavedSearchGenre = this._connection.CreateCommand();
                cmdDeleteMusicianSavedSearchGenre.CommandText = "DELETE FROM MusicianBand_searchgenres WHERE musicianband_searchid = @SavedSearchidtodelete";
                cmdDeleteMusicianSavedSearchGenre.Parameters.AddWithValue("SavedSearchidtodelete", SavedSearchIdToDelete);

                bool ergebnis = cmdDeleteMusicianSavedSearch.ExecuteNonQuery() == 1;
                cmdDeleteMusicianSavedSearchGenre.ExecuteNonQuery();
                return ergebnis;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public List<City> GetCities(string cityname, int postalcode)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<City> Cities = new List<City>();
            try
            {
                MySqlCommand cmdGetCities = this._connection.CreateCommand();
                cmdGetCities.CommandText = "SELECT * FROM cities where CityName LIKE @cityname and PostalCode LIKE @postalcode order by CityName limit 5000;";
                cmdGetCities.Parameters.AddWithValue("cityname", "%" + cityname + "%");
                cmdGetCities.Parameters.AddWithValue("postalcode", "%" + postalcode + "%");
                using (MySqlDataReader reader = cmdGetCities.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Cities.Add(
                            new City
                            {
                                CityID = reader["cityid"] != DBNull.Value ? Convert.ToInt32(reader["cityid"]) : (int?)null,
                                CountryID = reader["countryid"] != DBNull.Value ? Convert.ToInt32(reader["countryid"]) : (int?)null,
                                PostalCode = reader["postalcode"] != DBNull.Value ? Convert.ToInt32(reader["postalcode"]) : (int?)null,
                                CityName = Convert.ToString(reader["cityname"]),
                                Latitude = reader["latitude"] != DBNull.Value ? Convert.ToDouble(reader["latitude"]) : (double?)null,
                                Longitude = reader["longitude"] != DBNull.Value ? Convert.ToDouble(reader["longitude"]) : (double?)null,

                            });
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return Cities.Count >= 1 ? Cities : null;
        }

        public Musician GetMusicianIdByArtistname(string artistname)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }
            try
            {
                MySqlCommand cmdGetMusicianIdByArtistname = this._connection.CreateCommand();
                cmdGetMusicianIdByArtistname.CommandText = "SELECT * FROM Musicians where artistname = @artistname";
                cmdGetMusicianIdByArtistname.Parameters.AddWithValue("artistname", artistname);
                using (MySqlDataReader reader = cmdGetMusicianIdByArtistname.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        return new Musician()
                        {
                            MusicianID = Convert.ToInt32(reader["musicianid"]),
                            Email = Convert.ToString(reader["email"]),
                            PasswordHash = Convert.ToString(reader["passwordhash"]),
                            FirstName = Convert.ToString(reader["firstname"]),
                            LastName = Convert.ToString(reader["lastname"]),
                            Birthdate = reader["birthdate"] != DBNull.Value ? Convert.ToDateTime(reader["birthdate"]) : (DateTime?)null,
                            Skill = reader["skill"] != DBNull.Value ? Convert.ToInt32(reader["skill"]) : (int?)null,
                            Userlanguage = Convert.ToString(reader["userlanguage"]),
                            CityID = reader["cityid"] != DBNull.Value ? Convert.ToInt32(reader["cityid"]) : (int?)null,
                            ArtistName = Convert.ToString(reader["artistname"]),
                        };
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return null;
        }
    }
}

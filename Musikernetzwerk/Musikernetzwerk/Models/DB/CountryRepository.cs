﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models.DB
{
    public class CountryRepository : SQLConnection, ICountryRepository
    {
        public Country GetCountry(int countryID)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }
            try
            {
                MySqlCommand cmdSelectCountry = this._connection.CreateCommand();
                cmdSelectCountry.CommandText = "SELECT * FROM countries WHERE countryid = @countryid";
                cmdSelectCountry.Parameters.AddWithValue("countryid", countryID);
                using (MySqlDataReader reader = cmdSelectCountry.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        return new Country()
                        {
                            CountryID = reader["countryid"] != DBNull.Value ? Convert.ToInt32(reader["countryid"]) : (int?)null,
                            CountryName = Convert.ToString(reader["CountryName"]),

                        };
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return null;
        }

        public List<Country> GetAllCountries()
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<Country> allCountries = new List<Country>();
            try
            {
                MySqlCommand cmdSelectAllCountries = this._connection.CreateCommand();
                cmdSelectAllCountries.CommandText = "SELECT * FROM countries";
                using (MySqlDataReader reader = cmdSelectAllCountries.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        allCountries.Add(
                            new Country
                            {
                                CountryID = reader["countryid"] != DBNull.Value ? Convert.ToInt32(reader["countryid"]) : (int?)null,
                                CountryName = Convert.ToString(reader["CountryName"]),
                            });
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return allCountries.Count >= 1 ? allCountries : null;
        }
    }
}
﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models.DB
{
    public class CityRepository : SQLConnection, ICityRepository
    {
        public City GetCity(int? cityID)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open) || (cityID == null))
            {
                return null;
            }
            try
            {
                MySqlCommand cmdSelectCity = this._connection.CreateCommand();
                cmdSelectCity.CommandText = "SELECT * FROM cities WHERE cityid = @cityid";
                cmdSelectCity.Parameters.AddWithValue("cityid", cityID);
                using (MySqlDataReader reader = cmdSelectCity.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        return new City()
                        {
                            CityID = reader["cityid"] != DBNull.Value ? Convert.ToInt32(reader["cityid"]) : (int?)null,
                            CountryID = reader["CountryID"] != DBNull.Value ? Convert.ToInt32(reader["CountryID"]) : (int?)null,
                            PostalCode = reader["PostalCode"] != DBNull.Value ? Convert.ToInt32(reader["PostalCode"]) : (int?)null,
                            CityName = Convert.ToString(reader["CityName"]),
                            Latitude = reader.GetDouble(4),
                            Longitude = reader.GetDouble(5),
                        };
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return null;
        }

        public List<City> GetAllCities()
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<City> allCities = new List<City>();
            try
            {
                MySqlCommand cmdSelectAllCities = this._connection.CreateCommand();
                cmdSelectAllCities.CommandText = "SELECT * FROM cities";
                using (MySqlDataReader reader = cmdSelectAllCities.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        allCities.Add(
                            new City
                            {
                                CityID = reader["cityid"] != DBNull.Value ? Convert.ToInt32(reader["cityid"]) : (int?)null,
                                CountryID = reader["CountryID"] != DBNull.Value ? Convert.ToInt32(reader["CountryID"]) : (int?)null,
                                PostalCode = reader["PostalCode"] != DBNull.Value ? Convert.ToInt32(reader["PostalCode"]) : (int?)null,
                                CityName = Convert.ToString(reader["CityName"]),
                                Latitude = reader.GetDouble(4),
                                Longitude = reader.GetDouble(5),
                            });
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return allCities.Count >= 1 ? allCities : null;
        }
    }
}
﻿using Musikernetzwerk.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musikernetzwerk.Models.DB
{
    interface IMusicianBandRequestRepository : ISQLConnection
    {
        bool InsertMusicianBandRequest(int bandid, int musicianid);
        bool DeleteMusicianBandRequest(int bandrequestIdToDelete);
        MusicianBandRequestModel GetMusicianBandRequest(int bandrequestid);
        List<MusicianBandRequestModel> GetAllBandRequests(int bandid);
    }
}

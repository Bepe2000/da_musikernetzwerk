﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models.DB
{
    public class InstrumentRepository : SQLConnection, IInstrumentRepository
    {
        public Instrument GetInstrument(int instrumentId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }
            try
            {
                MySqlCommand cmdSelectInstrument = this._connection.CreateCommand();
                cmdSelectInstrument.CommandText = "SELECT * FROM instruments WHERE instrumentid = @instrumentid";
                cmdSelectInstrument.Parameters.AddWithValue("instrumentid", instrumentId);
                using (MySqlDataReader reader = cmdSelectInstrument.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        return new Instrument()
                        {
                            InstrumentID = reader["instrumentid"] != DBNull.Value ? Convert.ToInt32(reader["instrumentid"]) : (int?)null,
                            De = Convert.ToString(reader["De"]),
                            En = Convert.ToString(reader["En"]),                           
                        };
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return null;
        }

        public List<Instrument> GetAllInstruments()
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<Instrument> allInstruments = new List<Instrument>();
            try
            {
                MySqlCommand cmdSelectAllInstruments = this._connection.CreateCommand();
                cmdSelectAllInstruments.CommandText = "SELECT * FROM instruments";

                using (MySqlDataReader reader = cmdSelectAllInstruments.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        allInstruments.Add(
                            new Instrument
                            {
                                InstrumentID = reader["instrumentid"] != DBNull.Value ? Convert.ToInt32(reader["instrumentid"]) : (int?)null,
                                De = Convert.ToString(reader["De"]),
                                En = Convert.ToString(reader["En"]),
                            });
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return allInstruments.Count >= 1 ? allInstruments : null;
        }
    }
}
﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models.DB
{
    public class BandMemberSearchInstrumentsRepository : SQLConnection, IBandMemberSearchInstrumentsRepository
    {
        public bool InsertBandMemberSearchInstrument(BandMember_SearchInstrument bandmembersearchinstrument)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdInsertBandMemberSearchInstrument = this._connection.CreateCommand();
                cmdInsertBandMemberSearchInstrument.CommandText = "INSERT INTO BandMember_SearchInstruments VALUES(@BandMember_SearchID, @InstrumentID)";
                cmdInsertBandMemberSearchInstrument.Parameters.AddWithValue("BandMember_SearchID", bandmembersearchinstrument.BandMember_SearchId);
                cmdInsertBandMemberSearchInstrument.Parameters.AddWithValue("InstrumentID", bandmembersearchinstrument.InstrumentId);

                return cmdInsertBandMemberSearchInstrument.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public bool DeleteBandMemberSearchInstrument(int bandmembersearchinstrumentIdToDelete)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdDeleteBandMemberSearchInstrument = this._connection.CreateCommand();
                cmdDeleteBandMemberSearchInstrument.CommandText = "DELETE FROM BandMember_SearchInstruments WHERE BandMember_SearchID = @bandmembersearchinstrumentIdToDelete";
                cmdDeleteBandMemberSearchInstrument.Parameters.AddWithValue("bandmembersearchinstrumentIdToDelete", bandmembersearchinstrumentIdToDelete);

                return cmdDeleteBandMemberSearchInstrument.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public BandMember_SearchInstrument GetBandMemberSearchInstrument(int bandmembersearchinstrumentId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }
            try
            {
                MySqlCommand cmdSelectBandMemberSearchInstrument = this._connection.CreateCommand();
                cmdSelectBandMemberSearchInstrument.CommandText = "SELECT * FROM BandMember_SearchInstruments WHERE BandMember_SearchID = @bandmembersearchinstrumentId";
                cmdSelectBandMemberSearchInstrument.Parameters.AddWithValue("bandmembersearchinstrumentId", bandmembersearchinstrumentId);
                using (MySqlDataReader reader = cmdSelectBandMemberSearchInstrument.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        return new BandMember_SearchInstrument()
                        {
                            BandMember_SearchId = reader["bandmember_searchid"] != DBNull.Value ? Convert.ToInt32(reader["bandmember_searchid"]) : (int?)null,
                            InstrumentId = reader["instrumentid"] != DBNull.Value ? Convert.ToInt32(reader["instrumentid"]) : (int?)null,
                        };
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return null;
        }

        public List<BandMember_SearchInstrument> GetAllBandMemberSearchInstruments()
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<BandMember_SearchInstrument> allBandMemberSearchInstruments = new List<BandMember_SearchInstrument>();
            try
            {
                MySqlCommand cmdSelectAllBandMemberSearchInstruments = this._connection.CreateCommand();
                cmdSelectAllBandMemberSearchInstruments.CommandText = "SELECT * FROM BandMember_SearchInstruments";

                using (MySqlDataReader reader = cmdSelectAllBandMemberSearchInstruments.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        allBandMemberSearchInstruments.Add(
                            new BandMember_SearchInstrument
                            {
                                BandMember_SearchId = reader["bandmember_searchid"] != DBNull.Value ? Convert.ToInt32(reader["bandmember_searchid"]) : (int?)null,
                                InstrumentId = reader["instrumentid"] != DBNull.Value ? Convert.ToInt32(reader["instrumentid"]) : (int?)null,
                            });
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return allBandMemberSearchInstruments.Count >= 1 ? allBandMemberSearchInstruments : null;
        }
    }
}
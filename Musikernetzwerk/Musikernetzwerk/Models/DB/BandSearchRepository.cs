﻿using Musikernetzwerk.Models.ViewModels;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models.DB
{
    public class BandSearchRepository : SQLConnection, IBandSearchRepository
    {
        public List<SearchAfterBandResultModel> BandSearch(SearchAfterBandModel model, double lat, double log)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<SearchAfterBandResultModel> Searchresult = new List<SearchAfterBandResultModel>();
            try
            {
                MySqlCommand cmdGetSearchResult = this._connection.CreateCommand();
                cmdGetSearchResult.CommandText = "SELECT DISTINCT bandid, email, bandname, closedgroup, membercount , " +
                    " b.skill, description, cityname, postalcode, (6371 * ACOS(COS(RADIANS(@lat1)) * COS(RADIANS(latitude)) * COS(RADIANS(longitude) " +
                    " - RADIANS(@log)) + SIN(RADIANS(@lat2)) * SIN(RADIANS(latitude)))) AS citydistance FROM bands as b JOIN Cities USING (CityID) " +
                    " JOIN bandgenres as bg USING (bandid) ";

                cmdGetSearchResult.CommandText += " WHERE b.skill >= @skillmin AND b.skill <= @skillmax ";
                if ((model.ChosenGenreIDs != null) && (model.ChosenGenreIDs.Length >= 0))
                {
                    int i = 0;
                    foreach (int genreid in model.ChosenGenreIDs)
                    {
                        if (i == 0)
                        {
                            cmdGetSearchResult.CommandText += " AND genreid = @genreid" + i + " ";
                        }
                        else
                        {
                            cmdGetSearchResult.CommandText += " OR genreid = @genreid" + i + " ";
                        }
                        cmdGetSearchResult.Parameters.AddWithValue("genreid" + i, genreid);
                        i++;
                    }
                }
                if (model.CityDistance > 0)
                {
                    cmdGetSearchResult.CommandText += " HAVING citydistance <= @CityDistance";
                    cmdGetSearchResult.Parameters.AddWithValue("CityDistance", model.CityDistance);
                }

                cmdGetSearchResult.Parameters.AddWithValue("skillmin", model.SkillMin);
                cmdGetSearchResult.Parameters.AddWithValue("skillmax", model.SkillMax);

                cmdGetSearchResult.Parameters.AddWithValue("log", log);
                cmdGetSearchResult.Parameters.AddWithValue("lat1", lat);
                cmdGetSearchResult.Parameters.AddWithValue("lat2", lat);

                using (MySqlDataReader reader = cmdGetSearchResult.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Searchresult.Add(
                            new SearchAfterBandResultModel
                            {
                                BandID = Convert.ToInt32(reader["bandid"]),
                                Email = Convert.ToString(reader["email"]),
                                Bandname = Convert.ToString(reader["bandname"]),
                                ClosedGroup = Convert.ToBoolean(reader["closedgroup"]),
                                MemberCount = reader["membercount"] != DBNull.Value ? Convert.ToInt32(reader["membercount"]) : (int?)null,
                                Skill = reader["skill"] != DBNull.Value ? Convert.ToInt32(reader["skill"]) : (int?)null,
                                Description = Convert.ToString(reader["description"]),
                                CityName = Convert.ToString(reader["cityname"]),
                                PostalCode = Convert.ToInt32(reader["postalcode"]),
                                CityDistance = Convert.ToDecimal(reader["CityDistance"]),
                            });
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return Searchresult.Count >= 1 ? Searchresult : null;
        }

        public bool InsertBandSearchProperty(MusicianBand_SearchProperty bandsearchproperty)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdInsertBandSearchProperty = this._connection.CreateCommand();
                cmdInsertBandSearchProperty.CommandText = "INSERT INTO MusicianBand_SearchProperties VALUES(@MusicianBand_SearchID, @MusicianID, @Skillmin, @Skillmax, @MemberCountMin, @MemberCountMax, @CityDistance_max)";
                cmdInsertBandSearchProperty.Parameters.AddWithValue("MusicianBand_SearchID", bandsearchproperty.MusicianBand_SearchID);
                cmdInsertBandSearchProperty.Parameters.AddWithValue("MusicianID", bandsearchproperty.MusicianID);
                cmdInsertBandSearchProperty.Parameters.AddWithValue("Skillmin", bandsearchproperty.SkillMin);
                cmdInsertBandSearchProperty.Parameters.AddWithValue("Skillmax", bandsearchproperty.SkillMax);
                cmdInsertBandSearchProperty.Parameters.AddWithValue("MemberCountMin", bandsearchproperty.MemberCountMin);
                cmdInsertBandSearchProperty.Parameters.AddWithValue("MemberCountMax", bandsearchproperty.MemberCountMax);
                cmdInsertBandSearchProperty.Parameters.AddWithValue("CityDistance_max", bandsearchproperty.CityDistance_max);

                return cmdInsertBandSearchProperty.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public bool DeleteBandSearchProperty(int bandsearchpropertyIdToDelete)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdDeleteBandSearchProperty = this._connection.CreateCommand();
                cmdDeleteBandSearchProperty.CommandText = "DELETE FROM MusicianBand_SearchProperties WHERE MusicianBand_SearchID = @bandsearchpropertyIdToDelete";
                cmdDeleteBandSearchProperty.Parameters.AddWithValue("bandsearchpropertyIdToDelete", bandsearchpropertyIdToDelete);

                return cmdDeleteBandSearchProperty.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public MusicianBand_SearchProperty GetBandSearchProperty(int bandsearchpropertyId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }
            try
            {
                MySqlCommand cmdSelectBandSearchProperty = this._connection.CreateCommand();
                cmdSelectBandSearchProperty.CommandText = "SELECT * FROM MusicianBand_SearchProperties WHERE MusicianBand_SearchID = @bandsearchpropertyId";
                cmdSelectBandSearchProperty.Parameters.AddWithValue("bandsearchpropertyId", bandsearchpropertyId);
                using (MySqlDataReader reader = cmdSelectBandSearchProperty.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        return new MusicianBand_SearchProperty()
                        {
                            MusicianBand_SearchID= reader["musicianband_searchid"] != DBNull.Value ? Convert.ToInt32(reader["musicianband_searchid"]) : (int?)null,
                            MusicianID = reader["musicianid"] != DBNull.Value ? Convert.ToInt32(reader["musicianid"]) : (int?)null,
                            SkillMin = reader["skillmin"] != DBNull.Value ? Convert.ToInt32(reader["skillmin"]) : (int?)null,
                            SkillMax = reader["skillmax"] != DBNull.Value ? Convert.ToInt32(reader["skillmax"]) : (int?)null,
                            MemberCountMin = reader["membercountmin"] != DBNull.Value ? Convert.ToInt32(reader["membercountmin"]) : (int?)null,
                            MemberCountMax = reader["membercountmax"] != DBNull.Value ? Convert.ToInt32(reader["membercountmax"]) : (int?)null,
                            CityDistance_max = reader["citydistance_max"] != DBNull.Value ? Convert.ToDouble(reader["citydistance_max"]) : (double?)null
                        };
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return null;
        }

        public List<MusicianBand_SearchProperty> GetAllBandSearchProperties()
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<MusicianBand_SearchProperty> allBandSearchProperties = new List<MusicianBand_SearchProperty>();
            try
            {
                MySqlCommand cmdSelectAllBandSearchProperties = this._connection.CreateCommand();
                cmdSelectAllBandSearchProperties.CommandText = "SELECT * FROM MusicianBand_SearchProperties";

                using (MySqlDataReader reader = cmdSelectAllBandSearchProperties.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        allBandSearchProperties.Add(
                            new MusicianBand_SearchProperty
                            {
                                MusicianBand_SearchID= reader["musicianband_searchid"] != DBNull.Value ? Convert.ToInt32(reader["musicianband_searchid"]) : (int?)null,
                                MusicianID = reader["bandid"] != DBNull.Value ? Convert.ToInt32(reader["bandid"]) : (int?)null,
                                SkillMin = reader["skillmin"] != DBNull.Value ? Convert.ToInt32(reader["skillmin"]) : (int?)null,
                                SkillMax = reader["skillmax"] != DBNull.Value ? Convert.ToInt32(reader["skillmax"]) : (int?)null,
                                MemberCountMin = reader["membercountmin"] != DBNull.Value ? Convert.ToInt32(reader["membercountmin"]) : (int?)null,
                                MemberCountMax = reader["membercountmax"] != DBNull.Value ? Convert.ToInt32(reader["membercountmax"]) : (int?)null,
                                CityDistance_max = reader["citydistance_max"] != DBNull.Value ? Convert.ToDouble(reader["citydistance_max"]) : (double?)null
                            });
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return allBandSearchProperties.Count >= 1 ? allBandSearchProperties : null;
        }

        public SearchAfterBandModel GetSearchAfterModelWithSearchId(int? musicianband_SearchID)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }
            try
            {
                MySqlCommand cmdgetSearchAfterModelWithSearchId = this._connection.CreateCommand();
                cmdgetSearchAfterModelWithSearchId.CommandText = "SELECT * FROM MusicianBand_SearchProperties where MusicianBand_SearchID = @musicianband_SearchID";
                cmdgetSearchAfterModelWithSearchId.Parameters.AddWithValue("musicianband_SearchID", musicianband_SearchID);

                SearchAfterBandModel model = new SearchAfterBandModel();

                using (MySqlDataReader reader = cmdgetSearchAfterModelWithSearchId.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();

                        model = new SearchAfterBandModel
                        {
                            SkillMin = reader["skillmin"] != DBNull.Value ? Convert.ToInt32(reader["skillmin"]) : (int?)null,
                            SkillMax = reader["skillmax"] != DBNull.Value ? Convert.ToInt32(reader["skillmax"]) : (int?)null,
                            CityDistance = Convert.ToInt32(reader["citydistance_max"]),
                            Genres = null,
                            ChosenGenreIDs = null
                        };
                    }
                }
                List<int> ListOfGenreIDs = new List<int>() { };
                MySqlCommand cmdGetGenresOfBand = this._connection.CreateCommand();
                cmdGetGenresOfBand.CommandText = "SELECT * FROM MusicianBand_SearchGenres WHERE musicianband_SearchID = @musicianband_SearchID";
                cmdGetGenresOfBand.Parameters.AddWithValue("musicianband_SearchID", musicianband_SearchID);

                int? currentGenreValue;
                using (MySqlDataReader reader2 = cmdGetGenresOfBand.ExecuteReader())
                {
                    while (reader2.HasRows)
                    {
                        reader2.Read();
                        currentGenreValue = (int?)reader2["genreid"];
                        if (currentGenreValue != null)
                        {
                            if (ListOfGenreIDs.Count > 0)
                            {
                                if (ListOfGenreIDs.Last() == (int)currentGenreValue) { break; }
                            }
                            ListOfGenreIDs.Add((int)currentGenreValue);
                        }
                    }
                }
                int[] int_array = new int[ListOfGenreIDs.Count];
                for (int index = 0; index < ListOfGenreIDs.Count; index++)
                {
                    int_array[index] = ListOfGenreIDs[index];
                }
                model.ChosenGenreIDs = int_array;
                return model;
            }
            catch (MySqlException)
            {
                throw;
            }
        }
    }
}




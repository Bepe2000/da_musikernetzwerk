﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Musikernetzwerk.Models.ViewModels;

namespace Musikernetzwerk.Models.DB
{
    interface IBandSearchRepository : ISQLConnection
    {
        List<SearchAfterBandResultModel> BandSearch(SearchAfterBandModel model, double log, double lag);
        bool InsertBandSearchProperty(MusicianBand_SearchProperty bandsearchproperty);
        bool DeleteBandSearchProperty(int bandsearchpropertyIdToDelete);
        MusicianBand_SearchProperty GetBandSearchProperty(int bandsearchpropertyId);
        List<MusicianBand_SearchProperty> GetAllBandSearchProperties();
        SearchAfterBandModel GetSearchAfterModelWithSearchId(int? musicianband_SearchID);
    }
}

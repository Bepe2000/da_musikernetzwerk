﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models.DB
{
    public class MusicianBandSearchGenresRepository : SQLConnection, IMusicianBandSearchGenresRepository
    {
        public bool InsertBandSearchGenre(MusicianBand_SearchGenre bandsearchgenre)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdInsertBandSearchGenre = this._connection.CreateCommand();
                cmdInsertBandSearchGenre.CommandText = "INSERT INTO MusicianBand_SearchGenres VALUES(@MusicianBand_SearchID, @GenreID)";
                cmdInsertBandSearchGenre.Parameters.AddWithValue("MusicianBand_SearchID", bandsearchgenre.MusicianBand_SearchID);
                cmdInsertBandSearchGenre.Parameters.AddWithValue("GenreID", bandsearchgenre.GenreId);

                return cmdInsertBandSearchGenre.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public bool DeleteBandSearchGenre(int bandsearchgenreIdToDelete)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdDeleteBandSearchGenre = this._connection.CreateCommand();
                cmdDeleteBandSearchGenre.CommandText = "DELETE FROM MusicianBand_SearchGenres WHERE MusicianBand_SearchID= @bandsearchgenreIdToDelete";
                cmdDeleteBandSearchGenre.Parameters.AddWithValue("bandsearchgenreIdToDelete", bandsearchgenreIdToDelete);

                return cmdDeleteBandSearchGenre.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public MusicianBand_SearchGenre GetBandSearchGenre(int bandsearchgenreId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }
            try
            {
                MySqlCommand cmdSelectBandSearchGenre = this._connection.CreateCommand();
                cmdSelectBandSearchGenre.CommandText = "SELECT * FROM MusicianBand_SearchGenres WHERE MusicianBand_SearchID= @bandsearchgenreId";
                cmdSelectBandSearchGenre.Parameters.AddWithValue("bandsearchgenreId", bandsearchgenreId);
                using (MySqlDataReader reader = cmdSelectBandSearchGenre.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        return new MusicianBand_SearchGenre()
                        {
                            MusicianBand_SearchID= reader["MusicianBand_SearchID"] != DBNull.Value ? Convert.ToInt32(reader["MusicianBand_SearchID"]) : (int?)null,
                            GenreId = reader["genreid"] != DBNull.Value ? Convert.ToInt32(reader["genreid"]) : (int?)null,
                        };
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return null;
        }

        public List<MusicianBand_SearchGenre> GetAllBandSearchGenres()
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<MusicianBand_SearchGenre> allBandSearchGenres = new List<MusicianBand_SearchGenre>();
            try
            {
                MySqlCommand cmdSelectAllBandSearchGenres = this._connection.CreateCommand();
                cmdSelectAllBandSearchGenres.CommandText = "SELECT * FROM MusicianBand_SearchGenres";
                using (MySqlDataReader reader = cmdSelectAllBandSearchGenres.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        allBandSearchGenres.Add(
                            new MusicianBand_SearchGenre
                            {
                                MusicianBand_SearchID= reader["MusicianBand_SearchID"] != DBNull.Value ? Convert.ToInt32(reader["MusicianBand_SearchID"]) : (int?)null,
                                GenreId = reader["genreid"] != DBNull.Value ? Convert.ToInt32(reader["genreid"]) : (int?)null,
                            });
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return allBandSearchGenres.Count >= 1 ? allBandSearchGenres : null;
        }
    }
}

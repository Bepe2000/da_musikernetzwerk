﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musikernetzwerk.Models.DB
{
    interface IBandGenreRepository : ISQLConnection
    {
        bool InsertBandGenre(int bandId, int genreId);
        bool DeleteBandGenre(int bandId, int genreId);
        List<Genre> GetBandGenre(int bandId);
        List<Band> GetBandsByGenre(int genreId);        
    }
}

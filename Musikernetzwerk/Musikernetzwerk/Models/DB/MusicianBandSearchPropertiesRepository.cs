﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models.DB
{
    public class MusicianBandSearchPropertiesRepository : SQLConnection, IMusicianBandSearchPropertiesRepository
    {
        public bool InsertMusicianBandSearchProperty(MusicianBand_SearchProperty musicianbandsearchproperty)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdInsertMusicianBandSearchProperty = this._connection.CreateCommand();
                cmdInsertMusicianBandSearchProperty.CommandText = "INSERT INTO MusicianBand_SearchProperties VALUES(@MusicianBand_SearchID, @MusicianID, @Skillmin, @Skillmax, @MemberCount_min, @MemberCount_max, @CityDistance_max)";
                cmdInsertMusicianBandSearchProperty.Parameters.AddWithValue("MusicianBand_SearchID", musicianbandsearchproperty.MusicianBand_SearchID);
                cmdInsertMusicianBandSearchProperty.Parameters.AddWithValue("MusicianID", musicianbandsearchproperty.MusicianID);
                cmdInsertMusicianBandSearchProperty.Parameters.AddWithValue("Skillmin", musicianbandsearchproperty.SkillMin);
                cmdInsertMusicianBandSearchProperty.Parameters.AddWithValue("Skillmax", musicianbandsearchproperty.SkillMax);
                cmdInsertMusicianBandSearchProperty.Parameters.AddWithValue("MemberCount_min", musicianbandsearchproperty.MemberCountMin);
                cmdInsertMusicianBandSearchProperty.Parameters.AddWithValue("MemberCount_max", musicianbandsearchproperty.MemberCountMax);
                cmdInsertMusicianBandSearchProperty.Parameters.AddWithValue("CityDistance_max", musicianbandsearchproperty.CityDistance_max);

                return cmdInsertMusicianBandSearchProperty.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public bool DeleteMusicianBandSearchProperty(int musicianbandsearchpropertyIdToDelete)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdDeleteMusicianBandSearchProperty = this._connection.CreateCommand();
                cmdDeleteMusicianBandSearchProperty.CommandText = "DELETE FROM MusicianBand_SearchProperties WHERE MusicianBand_SearchID= @musicianbandsearchpropertyIdToDelete";
                cmdDeleteMusicianBandSearchProperty.Parameters.AddWithValue("musicianbandsearchpropertyIdToDelete", musicianbandsearchpropertyIdToDelete);

                return cmdDeleteMusicianBandSearchProperty.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public MusicianBand_SearchProperty GetMusicianBandSearchProperty(int musicianbandsearchpropertyId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }
            try
            {
                MySqlCommand cmdSelectMusicianBandSearchProperty = this._connection.CreateCommand();
                cmdSelectMusicianBandSearchProperty.CommandText = "SELECT * FROM MusicianBand_SearchProperties WHERE MusicianBand_SearchID= @musicianbandsearchpropertyId";
                cmdSelectMusicianBandSearchProperty.Parameters.AddWithValue("musicianbandsearchpropertyId", musicianbandsearchpropertyId);
                using (MySqlDataReader reader = cmdSelectMusicianBandSearchProperty.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        return new MusicianBand_SearchProperty()
                        {
                            MusicianBand_SearchID= reader["MusicianBand_SearchID"] != DBNull.Value ? Convert.ToInt32(reader["MusicianBand_SearchID"]) : (int?)null,
                            MusicianID = reader["bandid"] != DBNull.Value ? Convert.ToInt32(reader["bandid"]) : (int?)null,
                            SkillMin = reader["skillmin"] != DBNull.Value ? Convert.ToInt32(reader["skillmin"]) : (int?)null,
                            SkillMax = reader["skillmax"] != DBNull.Value ? Convert.ToInt32(reader["skillmax"]) : (int?)null,
                            MemberCountMin = reader["membercount_min"] != DBNull.Value ? Convert.ToInt32(reader["membercount_min"]) : (int?)null,
                            MemberCountMax = reader["membercount_max"] != DBNull.Value ? Convert.ToInt32(reader["membercount_max"]) : (int?)null,
                            CityDistance_max = reader["citydistance_max"] != DBNull.Value ? Convert.ToDouble(reader["citydistance_max"]) : (double?)null
                        };
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return null;
        }

        public List<MusicianBand_SearchProperty> GetAllMusicianBandSearchProperties()
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<MusicianBand_SearchProperty> allMusicianBandSearchProperties = new List<MusicianBand_SearchProperty>();
            try
            {
                MySqlCommand cmdSelectAllMusicianBandSearchProperties = this._connection.CreateCommand();
                cmdSelectAllMusicianBandSearchProperties.CommandText = "SELECT * FROM BandMember_SearchProperties";
                using (MySqlDataReader reader = cmdSelectAllMusicianBandSearchProperties.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        allMusicianBandSearchProperties.Add(
                            new MusicianBand_SearchProperty
                            {
                                MusicianBand_SearchID= reader["MusicianBand_SearchID"] != DBNull.Value ? Convert.ToInt32(reader["MusicianBand_SearchID"]) : (int?)null,
                                MusicianID = reader["bandid"] != DBNull.Value ? Convert.ToInt32(reader["bandid"]) : (int?)null,
                                SkillMin = reader["skillmin"] != DBNull.Value ? Convert.ToInt32(reader["skillmin"]) : (int?)null,
                                SkillMax = reader["skillmax"] != DBNull.Value ? Convert.ToInt32(reader["skillmax"]) : (int?)null,
                                MemberCountMin = reader["membercount_min"] != DBNull.Value ? Convert.ToInt32(reader["membercount_min"]) : (int?)null,
                                MemberCountMax = reader["membercount_max"] != DBNull.Value ? Convert.ToInt32(reader["membercount_max"]) : (int?)null,
                                CityDistance_max = reader["citydistance_max"] != DBNull.Value ? Convert.ToDouble(reader["citydistance_max"]) : (double?)null
                            });
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return allMusicianBandSearchProperties.Count >= 1 ? allMusicianBandSearchProperties : null;
        }

        public List<MusicianBand_SearchProperty> GetAllMusicianBandSearchPropertiesOfMusician(int musicianid)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<MusicianBand_SearchProperty> allBandSearchProperties = new List<MusicianBand_SearchProperty>();
            try
            {
                MySqlCommand cmdSelectAllBandSearchPropertiesOfMusician = this._connection.CreateCommand();
                cmdSelectAllBandSearchPropertiesOfMusician.CommandText = "SELECT * FROM MusicianBand_SearchProperties where musicianid = @musicianid";
                cmdSelectAllBandSearchPropertiesOfMusician.Parameters.AddWithValue("musicianid", musicianid);
                using (MySqlDataReader reader = cmdSelectAllBandSearchPropertiesOfMusician.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        allBandSearchProperties.Add(
                            new MusicianBand_SearchProperty
                            {
                                MusicianBand_SearchID= reader["MusicianBand_SearchID"] != DBNull.Value ? Convert.ToInt32(reader["MusicianBand_SearchID"]) : (int?)null,
                                MusicianID = reader["musicianid"] != DBNull.Value ? Convert.ToInt32(reader["musicianid"]) : (int?)null,
                                SkillMin = reader["skillmin"] != DBNull.Value ? Convert.ToInt32(reader["skillmin"]) : (int?)null,
                                SkillMax = reader["skillmax"] != DBNull.Value ? Convert.ToInt32(reader["skillmax"]) : (int?)null,
                                MemberCountMin = reader["membercount_min"] != DBNull.Value ? Convert.ToInt32(reader["membercount_min"]) : (int?)null,
                                MemberCountMax = reader["membercount_max"] != DBNull.Value ? Convert.ToInt32(reader["membercount_max"]) : (int?)null,
                                CityDistance_max = reader["citydistance_max"] != DBNull.Value ? Convert.ToDouble(reader["citydistance_max"]) : (double?)null
                            });
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return allBandSearchProperties.Count >= 1 ? allBandSearchProperties : null;
        }
    }
}
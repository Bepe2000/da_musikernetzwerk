﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musikernetzwerk.Models.DB
{
    interface IGenreRepository : ISQLConnection
    {
        Genre GetGenre(int genreID);
        List<Genre> GetAllGenres();
    }
}

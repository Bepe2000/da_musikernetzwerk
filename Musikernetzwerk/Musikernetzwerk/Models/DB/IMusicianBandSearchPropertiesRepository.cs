﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musikernetzwerk.Models.DB
{
    interface IMusicianBandSearchPropertiesRepository : ISQLConnection
    {
        bool InsertMusicianBandSearchProperty(MusicianBand_SearchProperty musicianbandsearchproperty);
        bool DeleteMusicianBandSearchProperty(int musicianbandsearchpropertyIdToDelete);
        MusicianBand_SearchProperty GetMusicianBandSearchProperty(int musicianbandsearchpropertyId);
        List<MusicianBand_SearchProperty> GetAllMusicianBandSearchProperties();
        List<MusicianBand_SearchProperty> GetAllMusicianBandSearchPropertiesOfMusician(int musicianid);
    }
}

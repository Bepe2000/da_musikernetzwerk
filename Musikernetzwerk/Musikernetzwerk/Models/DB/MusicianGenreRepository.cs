﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models.DB
{
    public class MusicianGenreRepository : SQLConnection, IMusicianGenreRepository
    {
        public List<Genre> GetMusicianGenre(int musicianId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<Genre> allGenre = new List<Genre>();
            try
            {
                MySqlCommand cmdSelectMusicianGenre = this._connection.CreateCommand();
                cmdSelectMusicianGenre.CommandText = "SELECT * from MusicianGenres JOIN genres USING (GenreId) where musicianid = @musicianId";
                cmdSelectMusicianGenre.Parameters.AddWithValue("musicianId", musicianId);
                using (MySqlDataReader reader = cmdSelectMusicianGenre.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        allGenre.Add(
                            new Genre(reader["genreid"] != DBNull.Value ? Convert.ToInt32(reader["genreid"]) : (int?)null,
                                 Convert.ToString(reader["de"]),
                                 Convert.ToString(reader["en"])
                            ));
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return allGenre.Count >= 1 ? allGenre : null;
        }

        public List<Musician> GetMusiciansByGenre(int genreId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<Musician> musiciansByGenre = new List<Musician>();
            try
            {
                MySqlCommand cmdSelectGenreByMusician = this._connection.CreateCommand();
                cmdSelectGenreByMusician.CommandText = "SELECT * from MusicianGenres JOIN musicians USING (MusicianId) where genreid = @genreId";
                cmdSelectGenreByMusician.Parameters.AddWithValue("genreId", genreId);

                using (MySqlDataReader reader = cmdSelectGenreByMusician.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        musiciansByGenre.Add(
                            new Musician
                            {
                                MusicianID = Convert.ToInt32(reader["musicianid"]),
                                ArtistName = Convert.ToString(reader["artistname"])                       
                            }
                            );
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return musiciansByGenre.Count >= 1 ? musiciansByGenre : null;
        }

        public bool InsertMusicianGenre(int musicianId, int genreId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdInsertMusicianGenre = this._connection.CreateCommand();
                cmdInsertMusicianGenre.CommandText = "INSERT INTO musiciangenres VALUES(@musicianId, @genreId)";
                cmdInsertMusicianGenre.Parameters.AddWithValue("musicianId", musicianId);
                cmdInsertMusicianGenre.Parameters.AddWithValue("genreId", genreId);

                return cmdInsertMusicianGenre.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public bool DeleteMusicianGenre(int musicianId, int genreId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdDeleteMusicianGenre = this._connection.CreateCommand();
                cmdDeleteMusicianGenre.CommandText = "DELETE FROM musiciangenres WHERE musicianid = @musicianid and genreid = @genreid";
                cmdDeleteMusicianGenre.Parameters.AddWithValue("musicianId", musicianId);

                return cmdDeleteMusicianGenre.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }
    }
}
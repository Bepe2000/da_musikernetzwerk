﻿using Musikernetzwerk.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models.DB
{
    interface IBandMemberRequestRepository : ISQLConnection
    {
        bool InsertBandMemberRequest(int musicianid, int bandid);
        bool DeleteBandMemberRequest(int bandmemberRequestIdToDelete);
        BandMemberRequestModel GetBandMemberRequest(int bandmemberrequestId);
        List<BandMemberRequestModel> GetAllMusicianRequests(int musicianId);
    }
}
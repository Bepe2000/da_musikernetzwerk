﻿using Musikernetzwerk.Models.ViewModels;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models.DB
{
    public class MusicianBandRequestRepository : SQLConnection, IMusicianBandRequestRepository
    {
        public bool InsertMusicianBandRequest(int musicianid, int bandid)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdInsertMusicianBandRequest = this._connection.CreateCommand();
                cmdInsertMusicianBandRequest.CommandText = "INSERT INTO MusicianBand_Requests VALUES(null, @MusicianID, @BandID)";
                cmdInsertMusicianBandRequest.Parameters.AddWithValue("MusicianID", musicianid);
                cmdInsertMusicianBandRequest.Parameters.AddWithValue("BandID", bandid);

                return cmdInsertMusicianBandRequest.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public bool DeleteMusicianBandRequest(int bandrequest_id)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdDeleteMusicianBandRequest = this._connection.CreateCommand();
                cmdDeleteMusicianBandRequest.CommandText = "DELETE FROM MusicianBand_Requests WHERE BandRequest_ID = @bandrequest_id";
                cmdDeleteMusicianBandRequest.Parameters.AddWithValue("bandrequest_id", bandrequest_id);

                return cmdDeleteMusicianBandRequest.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public MusicianBandRequestModel GetMusicianBandRequest(int bandrequestId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }
            try
            {
                MusicianBand_Request r = new MusicianBand_Request();
                MySqlCommand cmdSelectMusicianBandRequest = this._connection.CreateCommand();
                cmdSelectMusicianBandRequest.CommandText = "SELECT * FROM MusicianBand_Requests WHERE BandRequest_ID = @bandrequestid";
                cmdSelectMusicianBandRequest.Parameters.AddWithValue("bandrequestid", bandrequestId);
                using (MySqlDataReader reader = cmdSelectMusicianBandRequest.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        r = new MusicianBand_Request()
                        {
                            BandRequestID = reader["bandrequest_id"] != DBNull.Value ? Convert.ToInt32(reader["bandrequest_id"]) : (int?)null,
                            MusicianID = reader["musicianid"] != DBNull.Value ? Convert.ToInt32(reader["musicianid"]) : (int?)null,
                            BandID = reader["bandid"] != DBNull.Value ? Convert.ToInt32(reader["bandid"]) : (int?)null,
                        };
                    }
                    else
                    {
                        return null;
                    }
                }
                Musician m = new Musician();
                MySqlCommand cmdSelectAllMusicianInfosForRequest = this._connection.CreateCommand();
                cmdSelectAllMusicianInfosForRequest.CommandText = "SELECT * FROM Musicians WHERE MusicianID = @musicianid";
                cmdSelectAllMusicianInfosForRequest.Parameters.AddWithValue("musicianid", r.MusicianID);
                using (MySqlDataReader reader2 = cmdSelectAllMusicianInfosForRequest.ExecuteReader())
                {
                    if (reader2.HasRows)
                    {
                        reader2.Read();
                        m = new Musician
                        {
                            MusicianID = Convert.ToInt32(reader2["musicianid"]),
                            Email = Convert.ToString(reader2["email"]),
                            PasswordHash = Convert.ToString(reader2["passwordhash"]),
                            FirstName = Convert.ToString(reader2["firstname"]),
                            LastName = Convert.ToString(reader2["lastname"]),
                            Birthdate = reader2["birthdate"] != DBNull.Value ? Convert.ToDateTime(reader2["birthdate"]) : (DateTime?)null,
                            Skill = reader2["skill"] != DBNull.Value ? Convert.ToInt32(reader2["skill"]) : (int?)null,
                            Userlanguage = Convert.ToString(reader2["userlanguage"]),
                            CityID = reader2["cityid"] != DBNull.Value ? Convert.ToInt32(reader2["cityid"]) : (int?)null,
                            ArtistName = Convert.ToString(reader2["artistname"]),
                        };
                    }
                    else
                    {
                        return null;
                    }
                    return new MusicianBandRequestModel(m, r);
                }
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public List<MusicianBandRequestModel> GetAllBandRequests(int bandid)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<MusicianBandRequestModel> allMusicianBandRequests = new List<MusicianBandRequestModel>();
            try
            {
                MySqlCommand cmdSelectAllMusicianBandRequests = this._connection.CreateCommand();
                cmdSelectAllMusicianBandRequests.CommandText = "SELECT * FROM MusicianBand_Requests where bandid = @bandid";
                cmdSelectAllMusicianBandRequests.Parameters.AddWithValue("bandid", bandid);
                List<MusicianBand_Request> allRequestsOnly = new List<MusicianBand_Request>();
                using (MySqlDataReader reader = cmdSelectAllMusicianBandRequests.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        allRequestsOnly.Add(
                           new MusicianBand_Request
                           {
                               BandRequestID = reader["bandrequest_id"] != DBNull.Value ? Convert.ToInt32(reader["bandrequest_id"]) : (int?)null,
                               BandID = reader["bandid"] != DBNull.Value ? Convert.ToInt32(reader["bandid"]) : (int?)null,
                               MusicianID = reader["musicianid"] != DBNull.Value ? Convert.ToInt32(reader["musicianid"]) : (int?)null
                           });
                    }
                }
                Musician m = new Musician();
                foreach (MusicianBand_Request r in allRequestsOnly)
                {
                    MySqlCommand cmdSelectAllMusicianInfosForAllRequests = this._connection.CreateCommand();
                    cmdSelectAllMusicianInfosForAllRequests.CommandText = "SELECT * FROM Musicians where musicianid = @musicianid";
                    cmdSelectAllMusicianInfosForAllRequests.Parameters.AddWithValue("musicianid", r.MusicianID);
                    using (MySqlDataReader reader2 = cmdSelectAllMusicianInfosForAllRequests.ExecuteReader())
                    {
                        if (reader2.HasRows)
                        {
                            reader2.Read();
                            {
                                m = new Musician(
                                   Convert.ToInt32(reader2["musicianid"]),
                                   Convert.ToString(reader2["email"]),
                                   Convert.ToString(reader2["passwordhash"]),
                                   Convert.ToString(reader2["firstname"]),
                                   Convert.ToString(reader2["lastname"]),
                                   reader2["birthdate"] != DBNull.Value ? Convert.ToDateTime(reader2["birthdate"]) : (DateTime?)null,
                                   reader2["skill"] != DBNull.Value ? Convert.ToInt32(reader2["skill"]) : (int?)null,
                                   Convert.ToString(reader2["userlanguage"]),
                                   reader2["cityid"] != DBNull.Value ? Convert.ToInt32(reader2["cityid"]) : (int?)null,
                                   Convert.ToString(reader2["artistname"]));
                            }
                            allMusicianBandRequests.Add(new MusicianBandRequestModel(m, r));
                        }
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return allMusicianBandRequests.Count >= 1 ? allMusicianBandRequests : null;
        }
    }
}




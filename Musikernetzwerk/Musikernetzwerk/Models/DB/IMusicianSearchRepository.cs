﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Musikernetzwerk.Models.ViewModels;
namespace Musikernetzwerk.Models.DB
{
    interface IMusicianSearchRepository : ISQLConnection
    {
        List<SearchAfterMusicianResultModel> MusicianSearch(SearchAfterMusicianModel model, double log, double lag);
        bool InsertBandMemberSearchProperty(BandMember_SearchProperty bandmembersearchproperty);
        bool DeleteBandMemberSearchProperty(int bandmembersearchpropertyIdToDelete);
        BandMember_SearchProperty GetBandMemberSearchProperty(int bandmembersearchpropertyId);
        List<BandMember_SearchProperty> GetAllBandMemberSearchProperties();
        SearchAfterMusicianModel GetSearchAfterModelWithSearchId(int? searchid);
    }
}
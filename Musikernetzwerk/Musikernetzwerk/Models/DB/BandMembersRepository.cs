﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models.DB
{
    public class BandMembersRepository : SQLConnection, IBandMembersRepository
    {
        public List<Musician> GetBandMember(int bandId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<Musician> allMusicians = new List<Musician>();
            try
            {
                MySqlCommand cmdSelectBandMember = this._connection.CreateCommand();
                cmdSelectBandMember.CommandText = "SELECT * from BandMembers JOIN musicians USING (MusicianId) where bandid = @bandId";
                cmdSelectBandMember.Parameters.AddWithValue("bandId", bandId);

                using (MySqlDataReader reader = cmdSelectBandMember.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        allMusicians.Add(
                            new Musician(Convert.ToInt32(reader["musicianid"]),
                                 Convert.ToString(reader["email"]),
                                 Convert.ToString(reader["passwordhash"]),
                                 Convert.ToString(reader["firstname"]),
                                 Convert.ToString(reader["lastname"]),
                                 reader["birthdate"] != DBNull.Value ? Convert.ToDateTime(reader["birthdate"]) : (DateTime?)null,
                                 reader["skill"] != DBNull.Value ? Convert.ToInt32(reader["skill"]) : (int?)null,
                                 Convert.ToString(reader["userlanguage"]),
                                 reader["cityid"] != DBNull.Value ? Convert.ToInt32(reader["cityid"]) : (int?)null,
                                 Convert.ToString(reader["artistname"])
                           ));
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return allMusicians.Count >= 1 ? allMusicians : null;
        }

        public List<Band> GetBandsByMusicians(int musicianId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }
            List<Band> bandsByMusicians = new List<Band>();
            try
            {
                MySqlCommand cmdSelectBandByMusician = this._connection.CreateCommand();
                cmdSelectBandByMusician.CommandText = "SELECT * from BandMembers JOIN bands USING (BandId) where musicianid = @musicianId";
                cmdSelectBandByMusician.Parameters.AddWithValue("musicianId", musicianId);
                using (MySqlDataReader reader = cmdSelectBandByMusician.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        bandsByMusicians.Add(
                            new Band
                            {
                                BandID = Convert.ToInt32(reader["bandid"]),
                                Bandname = Convert.ToString(reader["bandname"]),
                                Description = Convert.ToString(reader["description"])
                            }
                            );
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return bandsByMusicians.Count >= 1 ? bandsByMusicians : null;
        }

        public bool InsertBandMember(int bandId, int musicianId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdInsertBandMember = this._connection.CreateCommand();
                cmdInsertBandMember.CommandText = "INSERT INTO bandmembers VALUES(@bandId, @musicianId)";
                cmdInsertBandMember.Parameters.AddWithValue("bandId", bandId);
                cmdInsertBandMember.Parameters.AddWithValue("musicianId", musicianId);

                return cmdInsertBandMember.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public bool DeleteBandMember(int bandId, int musicianId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdDeleteBandMember = this._connection.CreateCommand();
                cmdDeleteBandMember.CommandText = "DELETE FROM bandmembers WHERE bandid = @bandid and musicianid = @musicianid";
                cmdDeleteBandMember.Parameters.AddWithValue("bandId", bandId);
                cmdDeleteBandMember.Parameters.AddWithValue("musicianId", musicianId);

                return cmdDeleteBandMember.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }
    }
}
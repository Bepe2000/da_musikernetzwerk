﻿using Musikernetzwerk.Models.ViewModels;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models.DB
{
    public class MusicianSearchRepository : SQLConnection, IMusicianSearchRepository
    {
        public List<SearchAfterMusicianResultModel> MusicianSearch(SearchAfterMusicianModel model, double lat, double log)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<SearchAfterMusicianResultModel> Searchresult = new List<SearchAfterMusicianResultModel>();
            try
            {
                MySqlCommand cmdGetSearchResult = this._connection.CreateCommand();
                cmdGetSearchResult.CommandText = " SELECT DISTINCT musicianid, email, firstname, lastname, birthdate, m.skill, " +
                    " artistname, cityname, postalcode, (6371 * ACOS(COS(RADIANS(@lat1)) * COS(RADIANS(latitude)) * COS(RADIANS(longitude) " +
                    " - RADIANS(@log)) + SIN(RADIANS(@lat2)) * SIN(RADIANS(latitude)))) AS citydistance FROM musicians as m JOIN Cities USING (CityID) " +
                    " JOIN musicianinstruments as mi USING (musicianid) ";

                if ((model.ChosenInstrumentIDs == null) || (model.ChosenInstrumentIDs.Length <= 0))
                    cmdGetSearchResult.CommandText += " WHERE m.skill >= @skillmin AND m.skill <= @skillmax ";
                else
                {
                    cmdGetSearchResult.CommandText += " WHERE mi.skill >= @skillmin AND mi.skill <= @skillmax ";
                    int i = 0;
                    foreach (int instrumentid in model.ChosenInstrumentIDs)
                    {
                        if (i == 0)
                        {
                            cmdGetSearchResult.CommandText += " AND instrumentid = @instrumentid" + i + " ";
                        }
                        else
                        {
                            cmdGetSearchResult.CommandText += " OR instrumentid = @instrumentid" + i + " ";
                        }
                        cmdGetSearchResult.Parameters.AddWithValue("instrumentid" + i, instrumentid);
                        i++;
                    }
                }
                if (model.CityDistance > 0)
                {
                    cmdGetSearchResult.CommandText += " HAVING citydistance <= @CityDistance";
                    cmdGetSearchResult.Parameters.AddWithValue("CityDistance", model.CityDistance);
                }

                cmdGetSearchResult.Parameters.AddWithValue("skillmin", model.SkillMin);
                cmdGetSearchResult.Parameters.AddWithValue("skillmax", model.SkillMax);


                cmdGetSearchResult.Parameters.AddWithValue("log", log);
                cmdGetSearchResult.Parameters.AddWithValue("lat1", lat);
                cmdGetSearchResult.Parameters.AddWithValue("lat2", lat);

                using (MySqlDataReader reader = cmdGetSearchResult.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Searchresult.Add(
                            new SearchAfterMusicianResultModel
                            {
                                MusicianID = Convert.ToInt32(reader["musicianid"]),
                                Email = Convert.ToString(reader["email"]),
                                FirstName = Convert.ToString(reader["firstname"]),
                                LastName = Convert.ToString(reader["lastname"]),
                                Birthdate = reader["birthdate"] != DBNull.Value ? Convert.ToDateTime(reader["birthdate"]) : (DateTime?)null,
                                ArtistName = Convert.ToString(reader["artistname"]),
                                Skill = reader["skill"] != DBNull.Value ? Convert.ToInt32(reader["skill"]) : (int?)null,
                                CityName = Convert.ToString(reader["cityname"]),
                                PostalCode = Convert.ToInt32(reader["postalcode"]),
                                CityDistance = Convert.ToDecimal(reader["CityDistance"]),
                            });
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return Searchresult.Count >= 1 ? Searchresult : null;
        }


        public bool InsertBandMemberSearchProperty(BandMember_SearchProperty bandmembersearchproperty)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdInsertBandMemberSearchProperty = this._connection.CreateCommand();
                cmdInsertBandMemberSearchProperty.CommandText = "INSERT INTO BandMember_SearchProperties VALUES(@BandMember_SearchID, @BandID, @Skillmin, @Skillmax, @CityDistance_max)";
                cmdInsertBandMemberSearchProperty.Parameters.AddWithValue("BandMember_SearchID", bandmembersearchproperty.BandMember_SearchId);
                cmdInsertBandMemberSearchProperty.Parameters.AddWithValue("BandID", bandmembersearchproperty.BandID);
                cmdInsertBandMemberSearchProperty.Parameters.AddWithValue("Skillmin", bandmembersearchproperty.SkillMin);
                cmdInsertBandMemberSearchProperty.Parameters.AddWithValue("Skillmax", bandmembersearchproperty.SkillMax);
                cmdInsertBandMemberSearchProperty.Parameters.AddWithValue("CityDistance_max", bandmembersearchproperty.CityDistance_max);

                return cmdInsertBandMemberSearchProperty.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public bool DeleteBandMemberSearchProperty(int bandmembersearchpropertyIdToDelete)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }

            try
            {
                MySqlCommand cmdDeleteBandMemberSearchProperty = this._connection.CreateCommand();
                cmdDeleteBandMemberSearchProperty.CommandText = "DELETE FROM BandMember_SearchProperties WHERE BandMember_SearchID = @bandmembersearchpropertyIdToDelete";
                cmdDeleteBandMemberSearchProperty.Parameters.AddWithValue("bandmembersearchpropertyIdToDelete", bandmembersearchpropertyIdToDelete);

                return cmdDeleteBandMemberSearchProperty.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public BandMember_SearchProperty GetBandMemberSearchProperty(int bandmembersearchpropertyId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            try
            {
                MySqlCommand cmdSelectBandMemberSearchProperty = this._connection.CreateCommand();
                cmdSelectBandMemberSearchProperty.CommandText = "SELECT * FROM BandMember_SearchProperties WHERE BandMember_SearchID = @bandmembersearchpropertyId";
                cmdSelectBandMemberSearchProperty.Parameters.AddWithValue("bandmembersearchpropertyId", bandmembersearchpropertyId);
                using (MySqlDataReader reader = cmdSelectBandMemberSearchProperty.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        return new BandMember_SearchProperty()
                        {
                            BandMember_SearchId = reader["bandmember_searchid"] != DBNull.Value ? Convert.ToInt32(reader["bandmember_searchid"]) : (int?)null,
                            BandID = reader["bandid"] != DBNull.Value ? Convert.ToInt32(reader["bandid"]) : (int?)null,
                            SkillMin = reader["skillmin"] != DBNull.Value ? Convert.ToInt32(reader["skillmin"]) : (int?)null,
                            SkillMax = reader["skillmax"] != DBNull.Value ? Convert.ToInt32(reader["skillmax"]) : (int?)null,
                            CityDistance_max = reader["citydistance_max"] != DBNull.Value ? Convert.ToDouble(reader["citydistance_max"]) : (double?)null
                        };
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return null;
        }

        public List<BandMember_SearchProperty> GetAllBandMemberSearchProperties()
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<BandMember_SearchProperty> allBandMemberSearchProperties = new List<BandMember_SearchProperty>();
            try
            {
                MySqlCommand cmdSelectAllBandMemberSearchProperties = this._connection.CreateCommand();
                cmdSelectAllBandMemberSearchProperties.CommandText = "SELECT * FROM BandMember_SearchProperties";

                using (MySqlDataReader reader = cmdSelectAllBandMemberSearchProperties.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        allBandMemberSearchProperties.Add(
                            new BandMember_SearchProperty
                            {
                                BandMember_SearchId = reader["bandmember_searchid"] != DBNull.Value ? Convert.ToInt32(reader["bandmember_searchid"]) : (int?)null,
                                BandID = reader["bandid"] != DBNull.Value ? Convert.ToInt32(reader["bandid"]) : (int?)null,
                                SkillMin = reader["skillmin"] != DBNull.Value ? Convert.ToInt32(reader["skillmin"]) : (int?)null,
                                SkillMax = reader["skillmax"] != DBNull.Value ? Convert.ToInt32(reader["skillmax"]) : (int?)null,
                                CityDistance_max = reader["citydistance_max"] != DBNull.Value ? Convert.ToDouble(reader["citydistance_max"]) : (double?)null
                            });
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return allBandMemberSearchProperties.Count >= 1 ? allBandMemberSearchProperties : null;
        }
        public SearchAfterMusicianModel GetSearchAfterModelWithSearchId(int? bandMember_SearchID)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }
            try
            {
                MySqlCommand cmdgetSearchAfterModelWithSearchId = this._connection.CreateCommand();
                cmdgetSearchAfterModelWithSearchId.CommandText = "SELECT * FROM BandMember_SearchProperties where bandMember_SearchID = @bandMember_SearchID";
                cmdgetSearchAfterModelWithSearchId.Parameters.AddWithValue("bandMember_SearchID", bandMember_SearchID);

                SearchAfterMusicianModel model = new SearchAfterMusicianModel();

                using (MySqlDataReader reader = cmdgetSearchAfterModelWithSearchId.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();

                        model = new SearchAfterMusicianModel
                        {
                            SkillMin = reader["skillmin"] != DBNull.Value ? Convert.ToInt32(reader["skillmin"]) : (int?)null,
                            SkillMax = reader["skillmax"] != DBNull.Value ? Convert.ToInt32(reader["skillmax"]) : (int?)null,
                            CityDistance = Convert.ToInt32(reader["citydistance_max"]),
                            Instruments = null,
                            ChosenInstrumentIDs = null
                        };
                    }

                }
                List<int> ListOfInstrumentIDs = new List<int>() { };
                MySqlCommand cmdGetInstrumentsOfMusician = this._connection.CreateCommand();
                cmdGetInstrumentsOfMusician.CommandText = "SELECT * FROM BandMember_SearchInstruments WHERE bandMember_SearchID = @bandMember_SearchID";
                cmdGetInstrumentsOfMusician.Parameters.AddWithValue("bandMember_SearchID", bandMember_SearchID);


                int? currentInstrumentValue;
                using (MySqlDataReader reader2 = cmdGetInstrumentsOfMusician.ExecuteReader())
                {
                    while (reader2.HasRows)
                    {
                        reader2.Read();
                        currentInstrumentValue = (int?)reader2["instrumentid"];

                        if (currentInstrumentValue != null)
                        {
                            if (ListOfInstrumentIDs.Count > 0)
                            {
                                if (ListOfInstrumentIDs.Last() == (int)currentInstrumentValue) { break; }
                            }
                            ListOfInstrumentIDs.Add((int)currentInstrumentValue);
                        }
                    }
                }
                int[] int_array = new int[ListOfInstrumentIDs.Count];
                for (int index = 0; index < ListOfInstrumentIDs.Count; index++)
                {
                    int_array[index] = ListOfInstrumentIDs[index];
                }
                model.ChosenInstrumentIDs = int_array;
                return model;

            }
            catch (MySqlException)
            {
                throw;
            }

        }
    }


}

﻿using System.Collections.Generic;

namespace Musikernetzwerk.Models.DB
{
    interface ICityRepository : ISQLConnection
    {
        City GetCity(int? CityID);
        List<City> GetAllCities();
    }
}

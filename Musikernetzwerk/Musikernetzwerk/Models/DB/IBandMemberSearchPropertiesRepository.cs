﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musikernetzwerk.Models.DB
{
    interface IBandMemberSearchPropertiesRepository :ISQLConnection
    {
        bool InsertBandMemberSearchProperty(BandMember_SearchProperty bandmembersearchproperty);
        bool DeleteBandMemberSearchProperty(int bandmembersearchpropertyIdToDelete);
        BandMember_SearchProperty GetBandMemberSearchProperty(int bandmembersearchpropertyId);
        List<BandMember_SearchProperty> GetAllBandMemberSearchProperties();
        List<BandMember_SearchProperty> GetAllBandMemberSearchPropertiesOfBand(int bandid);

    }
}

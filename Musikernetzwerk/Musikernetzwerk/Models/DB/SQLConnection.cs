﻿using MySql.Data.MySqlClient;
using System.Data;

namespace Musikernetzwerk.Models.DB
{
    public class SQLConnection : ISQLConnection
    {
        protected readonly string _connectionString = "Server=localhost;Database=musikernetzwerk;Uid=root;";
        protected MySqlConnection _connection;

        public void Open()
        {
            try
            {
                if (this._connection == null)
                {
                    this._connection = new MySqlConnection(_connectionString);
                }
                if (this._connection.State != ConnectionState.Open)
                {
                    this._connection.Open();
                }
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public void Close()
        {
            try
            {
                if ((this._connection != null) && (_connection.State != ConnectionState.Open))
                {
                    this._connection.Close();
                    this._connection = null;
                }
            }
            catch (MySqlException)
            {
                throw;
            }
        }
    }
}
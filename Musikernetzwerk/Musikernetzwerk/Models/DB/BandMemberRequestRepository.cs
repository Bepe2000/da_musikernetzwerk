﻿using Musikernetzwerk.Models.ViewModels;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models.DB
{
    public class BandMemberRequestRepository : SQLConnection, IBandMemberRequestRepository
    {
        public bool InsertBandMemberRequest(int musicianid, int bandid)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }
            try
            {
                MySqlCommand cmdInsertBandMemberRequest = this._connection.CreateCommand();
                cmdInsertBandMemberRequest.CommandText = "INSERT INTO BandMember_Requests VALUES(null, @BandID, @MusicianID)";
                cmdInsertBandMemberRequest.Parameters.AddWithValue("BandID", bandid);
                cmdInsertBandMemberRequest.Parameters.AddWithValue("MusicianID", musicianid);

                return cmdInsertBandMemberRequest.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public bool DeleteBandMemberRequest(int bandmemberRequestIdToDelete)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return false;
            }

            try
            {
                MySqlCommand cmdDeleteBandMemberRequest = this._connection.CreateCommand();
                cmdDeleteBandMemberRequest.CommandText = "DELETE FROM BandMember_Requests WHERE MemberRequest_ID = @bandmemberRequestIdToDelete";
                cmdDeleteBandMemberRequest.Parameters.AddWithValue("bandmemberRequestIdToDelete", bandmemberRequestIdToDelete);

                return cmdDeleteBandMemberRequest.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public BandMemberRequestModel GetBandMemberRequest(int memberrequestId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            try
            {
                BandMember_Request r = new BandMember_Request();
                MySqlCommand cmdSelectBandMemberRequest = this._connection.CreateCommand();
                cmdSelectBandMemberRequest.CommandText = "SELECT * FROM BandMember_Requests WHERE MemberRequest_ID = @memberrequestid";
                cmdSelectBandMemberRequest.Parameters.AddWithValue("memberrequestid", memberrequestId);
                using (MySqlDataReader reader = cmdSelectBandMemberRequest.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        r = new BandMember_Request()
                        {
                            MemberRequestID = reader["memberrequest_id"] != DBNull.Value ? Convert.ToInt32(reader["memberrequest_id"]) : (int?)null,
                            BandID = reader["bandid"] != DBNull.Value ? Convert.ToInt32(reader["bandid"]) : (int?)null,
                            MusicianID = reader["musicianid"] != DBNull.Value ? Convert.ToInt32(reader["musicianid"]) : (int?)null,
                        };

                    }
                    else
                    {
                        return null;
                    }
                }

                Band b = new Band();
                MySqlCommand cmdSelectAllBandInfosForRequest = this._connection.CreateCommand();
                cmdSelectAllBandInfosForRequest.CommandText = "SELECT * FROM Bands where BandID = @bandid";
                cmdSelectAllBandInfosForRequest.Parameters.AddWithValue("bandid", r.BandID);
                using (MySqlDataReader reader2 = cmdSelectAllBandInfosForRequest.ExecuteReader())
                {
                    if (reader2.HasRows)
                    {
                        reader2.Read();
                        b = new Band
                        {
                            BandID = Convert.ToInt32(reader2["bandid"]),
                            Email = Convert.ToString(reader2["email"]),
                            PasswordHash = Convert.ToString(reader2["passwordhash"]),
                            Bandname = Convert.ToString(reader2["bandname"]),
                            ClosedGroup = Convert.ToBoolean(reader2["closedgroup"]),
                            MemberCount = reader2["membercount"] != DBNull.Value ? Convert.ToInt32(reader2["membercount"]) : (int?)null,
                            Skill = reader2["skill"] != DBNull.Value ? Convert.ToInt32(reader2["skill"]) : (int?)null,
                            Userlanguage = Convert.ToString(reader2["userlanguage"]),
                            CityID = reader2["cityid"] != DBNull.Value ? Convert.ToInt32(reader2["cityid"]) : (int?)null,
                            Description = Convert.ToString(reader2["description"]),
                        };
                    }
                    else
                    {
                        return null;
                    }
                    return new BandMemberRequestModel(b, r);
                }
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public List<BandMemberRequestModel> GetAllMusicianRequests(int musicianId)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<BandMemberRequestModel> allBandMemberRequests = new List<BandMemberRequestModel>();
            try
            {
                MySqlCommand cmdSelectAllBandMemberRequests = this._connection.CreateCommand();
                cmdSelectAllBandMemberRequests.CommandText = "SELECT * FROM BandMember_Requests where musicianid = @musicianId";
                cmdSelectAllBandMemberRequests.Parameters.AddWithValue("musicianid", musicianId);

                List<BandMember_Request> allRequestsOnly = new List<BandMember_Request>();

                using (MySqlDataReader reader = cmdSelectAllBandMemberRequests.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        allRequestsOnly.Add(
                            new BandMember_Request
                            {
                                MemberRequestID = reader["memberrequest_id"] != DBNull.Value ? Convert.ToInt32(reader["memberrequest_id"]) : (int?)null,
                                BandID = reader["bandid"] != DBNull.Value ? Convert.ToInt32(reader["bandid"]) : (int?)null,
                                MusicianID = reader["musicianid"] != DBNull.Value ? Convert.ToInt32(reader["musicianid"]) : (int?)null
                            });
                    }
                }
                Band b = new Band();

                foreach (BandMember_Request r in allRequestsOnly)
                {
                    MySqlCommand cmdSelectAllBandInfosForAllRequests = this._connection.CreateCommand();
                    cmdSelectAllBandInfosForAllRequests.CommandText = "SELECT * FROM Bands where bandid = @bandid";
                    cmdSelectAllBandInfosForAllRequests.Parameters.AddWithValue("bandid", r.BandID);
                    using (MySqlDataReader reader2 = cmdSelectAllBandInfosForAllRequests.ExecuteReader())
                    {

                        if (reader2.HasRows)
                        {
                            reader2.Read();
                            {
                                b = new Band(
                                    Convert.ToInt32(reader2["bandid"]),
                                    Convert.ToString(reader2["email"]),
                                    Convert.ToString(reader2["passwordhash"]),
                                    Convert.ToString(reader2["bandname"]),
                                    Convert.ToBoolean(reader2["closedgroup"]),
                                    reader2["membercount"] != DBNull.Value ? Convert.ToInt32(reader2["membercount"]) : (int?)null,
                                    reader2["skill"] != DBNull.Value ? Convert.ToInt32(reader2["skill"]) : (int?)null,
                                    Convert.ToString(reader2["userlanguage"]),
                                    reader2["cityid"] != DBNull.Value ? Convert.ToInt32(reader2["cityid"]) : (int?)null,
                                    Convert.ToString(reader2["description"]));
                            };

                            allBandMemberRequests.Add(new BandMemberRequestModel(b, r));
                        }
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return allBandMemberRequests.Count >= 1 ? allBandMemberRequests : null;
        }
    }
}

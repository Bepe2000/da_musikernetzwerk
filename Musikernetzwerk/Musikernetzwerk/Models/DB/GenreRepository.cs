﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models.DB
{
    public class GenreRepository : SQLConnection, IGenreRepository
    {
        public Genre GetGenre(int genreID)
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }
            try
            {
                MySqlCommand cmdSelectGenre = this._connection.CreateCommand();
                cmdSelectGenre.CommandText = "SELECT * FROM genres WHERE genreid = @genreid";
                cmdSelectGenre.Parameters.AddWithValue("genreid", genreID);
                using (MySqlDataReader reader = cmdSelectGenre.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        return new Genre()
                        {
                            GenreID = reader["genreid"] != DBNull.Value ? Convert.ToInt32(reader["genreid"]) : (int?)null,
                            De = Convert.ToString(reader["De"]),
                            En=Convert.ToString(reader["En"])
                        };
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return null;
        }

        public List<Genre> GetAllGenres()
        {
            if ((this._connection == null) || (this._connection.State != ConnectionState.Open))
            {
                return null;
            }

            List<Genre> allGenres = new List<Genre>();
            try
            {
                MySqlCommand cmdSelectAllGenres = this._connection.CreateCommand();
                cmdSelectAllGenres.CommandText = "SELECT * FROM genres";

                using (MySqlDataReader reader = cmdSelectAllGenres.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        allGenres.Add(
                            new Genre
                            {
                                GenreID = reader["genreid"] != DBNull.Value ? Convert.ToInt32(reader["genreid"]) : (int?)null,
                                De = Convert.ToString(reader["De"]),
                                En = Convert.ToString(reader["En"])
                            });
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            return allGenres.Count >= 1 ? allGenres : null;
        }
    }
}
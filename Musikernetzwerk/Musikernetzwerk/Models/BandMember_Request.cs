﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models
{
    public class BandMember_Request
    {
        public int? MemberRequestID { get; set; }
        public int? BandID { get; set; }
        public int? MusicianID { get; set; }
        
        public BandMember_Request(): this(null, null, null) { }
        public BandMember_Request(int? memberRequestID, int? bandID, int? musicianID)
        {
            this.MemberRequestID = memberRequestID;    
            this.BandID = bandID;
            this.MusicianID = musicianID;
        }
    }
}
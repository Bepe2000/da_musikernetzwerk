﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models
{
    public class City
    {
        public int? CityID { get; set; }
        public int? CountryID { get; set; }
        public int? PostalCode { get; set; }
        public string CityName { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        public City() : this(null, null, null, "", null, null) { }
        public City(int? cityID, int? countryID, int? postalCode, string cityName, float? latitude, double? longitude)
        {
            this.CityID = cityID;
            this.CountryID = countryID;
            this.PostalCode = postalCode;
            this.CityName = cityName;
            this.Latitude = latitude;
            this.Longitude = longitude;
        }
    }
}
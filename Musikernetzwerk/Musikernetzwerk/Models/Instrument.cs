﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models
{
    public class Instrument
    {
        public int? InstrumentID { get; set;}
        public int? Skill { get; set; }
        public string De { get; set; }
        public string En { get; set; }

        public Instrument (): this(null, null, "", "") { }
        public Instrument(int? instrumentID, int? skill, string de, string en)
        {
            this.InstrumentID = instrumentID;
            this.Skill = skill;
            this.De = de;
            this.En = en;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models
{
    public class Musician
    {
        public int MusicianID { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? Birthdate { get; set; }
        public int? Skill { get; set; }
        public string Userlanguage { get; set; }
        public int? CityID { get; set; }     
        public string ArtistName { get; set; }
        
        public Musician(): this(-1, "", "", "", "", null, null, "", null, "") { }
        public Musician(int musicianID, string email, string passwordhash, string firstname, string lastname, DateTime? birthdate, int? skill, string userlanguage, int? cityID, string artistname)
        {
            this.MusicianID = musicianID;
            this.Email = email;
            this.PasswordHash = passwordhash;
            this.FirstName = firstname;
            this.LastName = lastname;
            this.Birthdate = birthdate;   
            this.Skill=skill;
            this.Userlanguage = userlanguage;
            this.CityID = cityID;           
            this.ArtistName = artistname;
        }
    }
}
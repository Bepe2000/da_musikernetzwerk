﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models
{
    public class Genre
    {
        public int? GenreID { get; set; }
        public string De { get; set; }
        public string En { get; set; }

        public Genre() : this(null, "", "") { }
        public Genre(int? genreID, string de, string en)
        {
            this.GenreID = genreID;
            this.De = de;
            this.En = en;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models
{
    public class Country
    {
        public int? CountryID { get; set; }
        public string CountryName { get; set; }

        public Country(): this(null, "") { }
        public Country(int? countryID, string countryName)
        {
            this.CountryID = countryID;
            this.CountryName = countryName;
        }
    }
}
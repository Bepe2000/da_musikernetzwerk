﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models
{
    public class MusicianBand_SearchGenre
    {
        public int? MusicianBand_SearchID{ get; set; }
        public int? GenreId { get; set; }

        public MusicianBand_SearchGenre() : this(null, null) { }
        public MusicianBand_SearchGenre(int? MusicianBand_SearchID, int? genreId)
        {
            this.MusicianBand_SearchID= MusicianBand_SearchID;
            this.GenreId = genreId;
        }
    }
}
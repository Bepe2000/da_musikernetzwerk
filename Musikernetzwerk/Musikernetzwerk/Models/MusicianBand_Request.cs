﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models
{
    public class MusicianBand_Request
    {
        public int? BandRequestID { get; set; }
        public int? MusicianID { get; set; }
        public int? BandID { get; set; }

        public MusicianBand_Request() : this(null, null, null) { }
        public MusicianBand_Request(int? bandRequestID, int? musicianID, int? bandID)
        {
            this.BandRequestID = bandRequestID;
            this.MusicianID = musicianID;
            this.BandID = bandID;
        }
    }
}
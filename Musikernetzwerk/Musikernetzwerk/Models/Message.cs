﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Musikernetzwerk.Models
{
    public class Message
    {
        public string Header { get; set; }
        public string MessageText { get; set; }

        public Message() : this("", "") { }
        public Message(string header, string messageText)
        {
            Header = header;
            MessageText = messageText;
        }
    }
}
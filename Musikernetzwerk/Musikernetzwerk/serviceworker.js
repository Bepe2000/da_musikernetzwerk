importScripts(
  "https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js"
);

// WORKBOX

// Plugins
const bgSyncPlugin = new workbox.backgroundSync.Plugin("WeBand-Queue", {
  maxRetentionTime: 15 // min
});

const expirationPlugin = new workbox.expiration.Plugin({
  maxAgeSeconds: 60 * 60 * 24 * 7 // 7 Tage
});

// Navigation PreLoad
workbox.navigationPreload.enable();

// Section: PreCache
workbox.precaching.precacheAndRoute([
  "/favicon.ico",
  "/manifest.json",
  "/Content/Images/hero.jpg"
]);

// Section: FALLBACK
// Request-FALLBACK
var FALLBACK_HTML_URL = "/offline.html";
var FALLBACK_IMAGE_URL = "/Content/Images/icons/icon-512x512.png";
workbox.precaching.precacheAndRoute([FALLBACK_HTML_URL]);
workbox.precaching.precacheAndRoute([FALLBACK_IMAGE_URL]);

workbox.routing.setCatchHandler(({ event }) => {
  switch (event.request.destination) {
    case "document":
      return caches.match(
        workbox.precaching.getCacheKeyForURL(FALLBACK_HTML_URL)
      );

    case "image":
      return caches.match(
        workbox.precaching.getCacheKeyForURL(FALLBACK_IMAGE_URL)
      );

    default:
      return Response.error("Test");
  }
});

// FALLBACK-Handler
workbox.routing.setDefaultHandler(new workbox.strategies.NetworkOnly());

// Section: RegisterRoute
// Cached Ressources
workbox.routing.registerRoute(
  "/",
  new workbox.strategies.StaleWhileRevalidate({ cacheName: "STW-cache" })
);
workbox.routing.registerRoute(
  /(\/Home\/Index)|(\/Home\/Imprint)|(\/SearchAfterMusician\/SearchDecision)/,
  new workbox.strategies.StaleWhileRevalidate({ cacheName: "STW-cache" })
);

// Bilder
workbox.routing.registerRoute(
  /\.(?:png|gif|jpg|jpeg|webp|svg)$/,
  new workbox.strategies.CacheFirst({
    cacheName: "image-cache",
    plugins: [bgSyncPlugin, expirationPlugin]
  })
);

// Stylesheets und Javascriptfiles
workbox.routing.registerRoute(
  /\.(?:js|css)$/,
  new workbox.strategies.CacheFirst({
    cacheName: "static_resources-cache",
    plugins: [bgSyncPlugin, expirationPlugin]
  })
);

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Musikernetzwerk.Models;
using Musikernetzwerk.Models.DB;
using MySql.Data.MySqlClient;
using Musikernetzwerk.Models.ViewModels;

namespace Musikernetzwerk.Controllers
{
    public class LoginController : Controller
    {
        IMusicianRepository loginMusicianRepository, registrationMusicianRepository, getCityMusicianRepository;
        IBandRepository loginBandRepository, registrationBandRepository;
        IInstrumentRepository instrumentRepository;
        IGenreRepository genreRepository;
        IMusicianInstrumentsRepository musicianInstrumentsRepository;
        IBandGenreRepository bandGenreRepository;

        [HttpGet]
        public ActionResult LoginMusician()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoginMusician(Musician login)
        {
            if ((login == null) || (login.Email == null) || (login.PasswordHash == null) || (login.Email.Trim().Length <= 5))
            {
                return View("Message", new Message(Resources.Resources.Login_Fehler, Resources.Resources.Ihre_Login_Daten_sind_zu_kurz));
            }
            try
            {
                loginMusicianRepository = new MusicianRepository();
                loginMusicianRepository.Open();

                Musician SessionMusician = loginMusicianRepository.Authenticate(login.Email, login.PasswordHash);
                if (SessionMusician != null)
                {
                    Session["Musician"] = SessionMusician;
                    Resources.Resources.Culture = CultureInfo.GetCultureInfo(SessionMusician.Userlanguage);
                    return View("PrepareCache");
                }
                else
                {
                    return View("Message", new Message(Resources.Resources.Login_Fehler, Resources.Resources.Ihre_Login_Daten_sind_falsch));
                }
            }
            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Versuchen_Sie_es_später_erneut));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Datenverarbeitungsproblem, Resources.Resources.Versuchen_Sie_es_später_erneut));
            }
            finally
            {
                if (loginMusicianRepository != null)
                    loginMusicianRepository.Close();
            }
        }

        public ActionResult LogoutMusician()
        {
            Session["Musician"] = null;
            return RedirectToAction("index", "home");
        }

        [HttpGet]
        public ActionResult RegistrationMusician()
        {
            instrumentRepository = new InstrumentRepository();
            try
            {
                instrumentRepository.Open();
                MusicianModel m = new MusicianModel();
                if (Resources.Resources.Culture != null)
                {
                    switch (Resources.Resources.Culture.Name)
                    {
                        case "en":
                            m.InstrumentSelectList = new MultiSelectList(instrumentRepository.GetAllInstruments(), "InstrumentID", "En");
                            break;
                        default:
                            m.InstrumentSelectList = new MultiSelectList(instrumentRepository.GetAllInstruments(), "InstrumentID", "De");
                            break;
                    }
                }
                else
                {
                    m.InstrumentSelectList = new MultiSelectList(instrumentRepository.GetAllInstruments(), "InstrumentID", "De");
                }
                m.NewChosenInstrumentIDs = new int[m.InstrumentSelectList.Count()];
                return View(m);
            }
            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Versuchen_Sie_es_später_erneut));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Datenverarbeitungsproblem, Resources.Resources.Versuchen_Sie_es_später_erneut));
            }
            finally
            {
                if (instrumentRepository != null)
                {
                    instrumentRepository.Close();
                }
            }

        }

        [HttpPost]
        public ActionResult RegistrationMusician(MusicianModel musicianmodel)
        {
            if (musicianmodel != null)
            {
                ValidationForRegistrationMusician(musicianmodel);

                if (ModelState.IsValid)
                {
                    string lang = (string)Session["lang"];
                    if ((lang != null) && (lang.Length == 2))
                    {
                        musicianmodel.Musician.Userlanguage = lang;
                    }
                    else
                    {
                        musicianmodel.Musician.Userlanguage = "de";
                    }
                    registrationMusicianRepository = new MusicianRepository();
                    instrumentRepository = new InstrumentRepository();
                    if ((musicianmodel != null) || (musicianmodel.NewChosenInstrumentIDs.Length > 0))
                    {
                        int zerocount = 0;
                        foreach (int id in musicianmodel.NewChosenInstrumentIDs)
                        {
                            if (id == 0)
                            {
                                zerocount++;
                            }
                        }
                        if (musicianmodel.NewChosenInstrumentIDs.Length == zerocount) { return RedirectToAction("RegistrationMusician"); }
                        try
                        {
                            instrumentRepository.Open();
                            Instrument i;
                            foreach (int id in musicianmodel.NewChosenInstrumentIDs)
                            {
                                if (id > 0)
                                {
                                    i = instrumentRepository.GetInstrument(id);
                                    musicianmodel.NewChosenInstruments.Add(i);
                                }
                            }
                            Session["musicianmodel"] = musicianmodel;
                            return View("RegistrationMusicianWithInstruments", musicianmodel);
                        }

                        catch (MySqlException)
                        {
                            return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Versuchen_Sie_es_später_erneut));
                        }
                        catch (Exception)
                        {
                            return View("Message", new Message(Resources.Resources.Datenverarbeitungsproblem, Resources.Resources.Versuchen_Sie_es_später_erneut));
                        }
                        finally
                        {
                            if (registrationMusicianRepository != null)
                                registrationMusicianRepository.Close();
                            if (instrumentRepository != null)
                            {
                                instrumentRepository.Close();
                            }
                        }
                    }
                    else
                    {
                        return View(musicianmodel);
                    }
                }
                else
                {
                    return RedirectToAction("RegistrationMusician");
                }
            }
            return RedirectToAction("RegistrationMusician");
        }

        [HttpPost]
        public ActionResult RegistrationMusicianWithInstruments(MusicianModel musicianmodel)
        {
            MusicianModel m = (MusicianModel)Session["musicianmodel"];
            m.Musician.CityID = m.City.CityID;
            registrationMusicianRepository = new MusicianRepository();
            musicianInstrumentsRepository = new MusicianInstrumentsRepository();
            for (int index = 0; index < musicianmodel.NewChosenInstruments.Count; index++)
            {
                musicianmodel.NewChosenInstruments[index].InstrumentID = m.NewChosenInstruments[index].InstrumentID;
            }
            bool noFail = false;
            if ((musicianmodel != null) && (m != null))
            {
                m.NewChosenInstruments = musicianmodel.NewChosenInstruments;
                try
                {
                    if ((m.Musician != null) && (m.NewChosenInstruments != null))
                    {
                        registrationMusicianRepository.Open();
                        noFail = registrationMusicianRepository.InsertMusician(m.Musician);
                        if (noFail)
                        {
                            Musician DBMusician = registrationMusicianRepository.GetMusicianIdByArtistname(m.Musician.ArtistName);
                            if (DBMusician != null)
                            {
                                musicianInstrumentsRepository.Open();
                                foreach (Instrument inst in m.NewChosenInstruments)
                                {
                                    if ((inst.InstrumentID != null) && (inst.Skill != null))
                                        musicianInstrumentsRepository.InsertMusicianInstrument(DBMusician.MusicianID, (int)inst.InstrumentID, (int)inst.Skill);
                                }
                                return RedirectToAction("LoginMusician");
                            }
                        }
                        return RedirectToAction("RegistrationMusician");
                    }
                }
                catch (MySqlException)
                {
                    return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Es_gibt_zurzeit_ein_Datenbankproblem));
                }
                catch (Exception)
                {
                    return View("Message", new Message(Resources.Resources.Allgemeiner_Fehler, Resources.Resources.Es_ist_ein_allgemeiner_Fehler_aufgetreten));
                }
                finally
                {
                    registrationMusicianRepository.Close();
                    if (musicianInstrumentsRepository != null)
                        musicianInstrumentsRepository.Close();
                }

            }
            return RedirectToAction("LoginMusician");
        }

        [HttpGet]
        public ActionResult LoginBand()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoginBand(Band login)
        {
            if ((login == null) || (login.Email == null) || (login.PasswordHash == null) || (login.Email.Trim().Length <= 5))
            {
                return View("Message", new Message(Resources.Resources.Login_Fehler, Resources.Resources.Ihre_Login_Daten_sind_zu_kurz));
            }
            try
            {
                loginBandRepository = new BandRepository();
                loginBandRepository.Open();

                Band SessionBand = loginBandRepository.Authenticate(login.Email, login.PasswordHash);
                if (SessionBand != null)
                {
                    Session["Band"] = SessionBand;
                    Resources.Resources.Culture = CultureInfo.GetCultureInfo(SessionBand.Userlanguage);
                    return View("PrepareCache");
                }
                else
                {
                    return View("Message", new Message(Resources.Resources.Login_Fehler, Resources.Resources.Ihre_Login_Daten_sind_falsch));
                }
            }
            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Versuchen_Sie_es_später_erneut));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Allgemeiner_Fehler, Resources.Resources.Es_ist_ein_allgemeiner_Fehler_aufgetreten));
            }
            finally
            {
                if (loginBandRepository != null)
                    loginBandRepository.Close();
            }
        }

        public ActionResult LogoutBand()
        {
            Session["Band"] = null;
            return RedirectToAction("index", "home");
        }

        [HttpGet]
        public ActionResult RegistrationBand()
        {
            genreRepository = new GenreRepository();
            try
            {
                genreRepository.Open();
                BandModel b = new BandModel();
                if (Resources.Resources.Culture != null)
                {
                    switch (Resources.Resources.Culture.Name)
                    {
                        case "en":
                            b.GenreSelectList = new MultiSelectList(genreRepository.GetAllGenres(), "GenreID", "En");
                            break;
                        default:
                            b.GenreSelectList = new MultiSelectList(genreRepository.GetAllGenres(), "GenreID", "De");
                            break;
                    }
                }
                else
                {
                    b.GenreSelectList = new MultiSelectList(genreRepository.GetAllGenres(), "GenreID", "De");
                }
                b.NewChosenGenreIDs = new int[b.GenreSelectList.Count()];
                return View(b);
            }
            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Versuchen_Sie_es_später_erneut));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Datenverarbeitungsproblem, Resources.Resources.Versuchen_Sie_es_später_erneut));
            }
            finally
            {
                if (genreRepository != null)
                {
                    genreRepository.Close();
                }
            }
        }

        [HttpPost]
        public ActionResult RegistrationBand(BandModel bandmodel)
        {
            if (bandmodel != null)
            {
                ValidationForRegistrationBand(bandmodel);

                if (ModelState.IsValid)
                {
                    string lang = (string)Session["lang"];
                    if ((lang != null) && (lang.Length == 2))
                    {
                        bandmodel.Band.Userlanguage = lang;
                    }
                    else
                    {
                        bandmodel.Band.Userlanguage = "de";
                    }

                    try
                    {
                        registrationBandRepository = new BandRepository();
                        bandmodel.Band.CityID = bandmodel.City.CityID;
                        registrationBandRepository.Open();
                        bool noFail = registrationBandRepository.InsertBand(bandmodel.Band);
                        if (noFail)
                        {
                            Band b = registrationBandRepository.GetBandUsingBandname(bandmodel.Band.Bandname);
                            if (b != null)
                            {
                                bandGenreRepository = new BandGenreRepository();
                                bandGenreRepository.Open();
                                foreach (int id in bandmodel.NewChosenGenreIDs)
                                {
                                    bandGenreRepository.InsertBandGenre(b.BandID, id);
                                }
                                return RedirectToAction("LoginBand");
                            }
                        }
                        return View(bandmodel);
                    }
                    catch (MySqlException)
                    {
                        return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Versuchen_Sie_es_später_erneut));
                    }
                    catch (Exception)
                    {
                        return View("Message", new Message(Resources.Resources.Datenverarbeitungsproblem, Resources.Resources.Versuchen_Sie_es_später_erneut));
                    }
                    finally
                    {
                        if (registrationBandRepository != null)
                            registrationBandRepository.Close();
                        if (genreRepository != null)
                        {
                            genreRepository.Close();
                        }
                    }
                }
                else
                {
                    return View(bandmodel);
                }
            }
            else
            {
                return RedirectToAction("RegistrationBand");
            }
        }

        [HttpGet]
        public JsonResult GetCitiesForRegistration(string cityname, int postalcode)
        {
            try
            {
                getCityMusicianRepository = new MusicianRepository();
                getCityMusicianRepository.Open();

                List<City> c = getCityMusicianRepository.GetCities(cityname, postalcode);

                if (c != null)
                {
                    return Json(c, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Response.StatusCode = 500;
                    return Json(new { success = false, responseText = "Error" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (MySqlException)
            {
                Response.StatusCode = 500;
                return Json(new { success = false, responseText = "Error" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                Response.StatusCode = 500;
                return Json(new { success = false, responseText = "Error" }, JsonRequestBehavior.AllowGet);
            }

            finally
            {
                getCityMusicianRepository.Close();
            }
        }

        public void ValidationForRegistrationMusician(MusicianModel musicianmodel)
        {
            if (musicianmodel.Musician.Email == null)
            {
                ModelState.AddModelError("Email", Resources.Resources.Die_Email_Adresse_ist_ein_Pflichtfeld);
            }
            if ((musicianmodel.Musician.PasswordHash == null) || (musicianmodel.Musician.PasswordHash.Length < 8) || (musicianmodel.Musician.PasswordHash.IndexOfAny(new[] { '!', '$', '%', '&', '/', '§' }) == -1))
            {
                ModelState.AddModelError("PasswordHash", Resources.Resources.Das_Passwort_muss_mindestens_8_Zeichen_lang_sein_und_eines_der_angeführten_Sonderzeichen_beinhalten);
            }
            if ((musicianmodel.Musician.FirstName == null) || (musicianmodel.Musician.FirstName != null) && (musicianmodel.Musician.FirstName.Trim().Length < 3))
            {
                ModelState.AddModelError("FirstName", Resources.Resources.Der_Vorname_muss_mindestens_3_Zeichen_lang_sein);
            }
            if ((musicianmodel.Musician.LastName == null) || (musicianmodel.Musician.LastName != null) && (musicianmodel.Musician.LastName.Trim().Length < 3))
            {
                ModelState.AddModelError("LastName", Resources.Resources.Der_Nachname_muss_mindestens_3_Zeichen_lang_sein);
            }
            if ((musicianmodel.Musician.Birthdate == null) || (musicianmodel.Musician.Birthdate >= DateTime.Today))
            {
                ModelState.AddModelError("Birthdate", Resources.Resources.Das_Geburtsdatum_muss_in_der_Vergangenheit_liegen);
            }
            if (musicianmodel.Musician.ArtistName == null)
            {
                ModelState.AddModelError("ArtistName", Resources.Resources.Der_Künstlername_ist_ein_Pflichtfeld);
            }
            if (musicianmodel.City == null)
            {
                ModelState.AddModelError("City", Resources.Resources.Die_Angabe_einer_Stadt_ist_ein_Pflichtfeld);
            }
            if (musicianmodel.NewChosenInstrumentIDs == null)
            {
                ModelState.AddModelError("OldChosenInstruments", Resources.Resources.Die_Angabe_von_mindestens_einem_Instrument_ist_verpflichtend);
            }
        }

        public void ValidationForRegistrationBand(BandModel bandmodel)
        {
            if (bandmodel.Band.Email == null)
            {
                ModelState.AddModelError("Email", Resources.Resources.Die_Email_Adresse_ist_ein_Pflichtfeld);
            }
            if ((bandmodel.Band.PasswordHash == null) || (bandmodel.Band.PasswordHash.Length < 8) || (bandmodel.Band.PasswordHash.IndexOfAny(new[] { '!', '$', '%', '&', '/', '§' }) == -1))
            {
                ModelState.AddModelError("PasswordHash", Resources.Resources.Das_Passwort_muss_mindestens_8_Zeichen_lang_sein_und_eines_der_angeführten_Sonderzeichen_beinhalten);
            }
            if (bandmodel.Band.Bandname == null)
            {
                ModelState.AddModelError("Bandname", Resources.Resources.Der_Bandname_ist_ein_Pflichtfeld);
            }
            if ((bandmodel.Band.Description == null) || (bandmodel.Band.Description.Length < 5))
            {
                ModelState.AddModelError("Description", Resources.Resources.Die_Beschreibung_ist_ein_Pflichtfeld_und_sollte_mindestens_20_Zeichen_lang_sein);
            }
            if (bandmodel.City == null)
            {
                ModelState.AddModelError("City", Resources.Resources.Die_Angabe_einer_Stadt_ist_ein_Pflichtfeld);
            }
            if (bandmodel.NewChosenGenreIDs == null)
            {
                ModelState.AddModelError("OldChosenGenres", Resources.Resources.Die_Angabe_von_mindestens_einem_Genre_ist_verpflichtend);
            }
        }
    }
}

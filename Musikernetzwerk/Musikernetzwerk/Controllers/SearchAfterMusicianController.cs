﻿using Musikernetzwerk.Models;
using Musikernetzwerk.Models.DB;
using Musikernetzwerk.Models.ViewModels;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Musikernetzwerk.Controllers
{
    public class SearchAfterMusicianController : Controller
    {
        IMusicianSearchRepository musicianSearchRepository;
        ICityRepository cityRepository;
        IBandMemberSearchPropertiesRepository bandMemberSearchPropertiesRepository;
        IBandMemberSearchInstrumentsRepository bandMemberSearchInstrumentsRepository;
        IInstrumentRepository instrumentRepository;

        [HttpGet]
        public ActionResult SearchAfterMusician()
        {
            SearchAfterMusicianModel m = new SearchAfterMusicianModel();
            try
            {
                instrumentRepository = new InstrumentRepository();
                instrumentRepository.Open();

                List<Instrument> i = instrumentRepository.GetAllInstruments();

                if (i != null)
                {
                    m.Instruments = new MultiSelectList(i, "InstrumentID", "De");
                    if (Resources.Resources.Culture != null)
                    {
                        string lang = (string)Resources.Resources.Culture.ToString();
                        if (lang != null)
                        {
                            switch (lang)
                            {
                                case "en":
                                    m.Instruments = new MultiSelectList(i, "InstrumentID", "En");
                                    break;
                                default:
                                    m.Instruments = new MultiSelectList(i, "InstrumentID", "De");
                                    break;
                            }
                        }
                    }
                    Musician user = (Musician)Session["Musician"];
                    if (user != null)
                    {
                        switch (user.Userlanguage)
                        {
                            case "en":
                                m.Instruments = new MultiSelectList(i, "InstrumentID", "En");
                                break;
                            default:
                                m.Instruments = new MultiSelectList(i, "InstrumentID", "De");
                                break;
                        }
                    }
                    if (user == null)
                    {
                        Band user2 = (Band)Session["Band"];
                        if (user2 != null)
                        {
                            switch (user2.Userlanguage)
                            {
                                case "en":
                                    m.Instruments = new MultiSelectList(i, "InstrumentID", "En");
                                    break;
                                default:
                                    m.Instruments = new MultiSelectList(i, "InstrumentID", "De");
                                    break;
                            }
                        }
                    }

                    return View(m);
                }
                else
                {
                    return View("Message", new Message(Resources.Resources.Instrumenten_Fehler, Resources.Resources.Keine_Instrumente_gefunden));
                }

            }
            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Es_gibt_zurzeit_ein_Datenbankproblem));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Allgemeiner_Fehler, Resources.Resources.Es_ist_ein_allgemeiner_Fehler_aufgetreten));
            }

            finally
            {
                instrumentRepository.Close();
            }
        }

        [HttpPost]
        public ActionResult SearchAfterMusician(SearchAfterMusicianModel searchAfterMusicianModel)
        {
            List<SearchAfterMusicianResultModel> SearchResult = new List<SearchAfterMusicianResultModel>();

            if (searchAfterMusicianModel != null)
            {
                try
                {
                    musicianSearchRepository = new MusicianSearchRepository();
                    musicianSearchRepository.Open();

                    if (Session["Musician"] != null)
                    {
                        Musician m = (Musician)Session["Musician"];
                        cityRepository = new CityRepository();
                        cityRepository.Open();
                        City c = cityRepository.GetCity((int)m.CityID);
                        SearchResult = musicianSearchRepository.MusicianSearch(searchAfterMusicianModel, (double)c.Latitude, (double)c.Longitude);
                    }
                    else if (Session["Band"] != null)
                    {
                        Band m = (Band)Session["Band"];
                        Session["SkillMin"] = searchAfterMusicianModel.SkillMin;
                        Session["SkillMax"] = searchAfterMusicianModel.SkillMax;
                        Session["CityDistance"] = searchAfterMusicianModel.CityDistance;
                        Session["ChosenInstrumentIDs"] = searchAfterMusicianModel.ChosenInstrumentIDs;
                        Session["BandID"] = m.BandID;

                        cityRepository = new CityRepository();
                        cityRepository.Open();
                        City c = cityRepository.GetCity((int)m.CityID);
                        SearchResult = musicianSearchRepository.MusicianSearch(searchAfterMusicianModel, (float)c.Latitude, (float)c.Longitude);
                    }
                    else
                    {
                        searchAfterMusicianModel.CityDistance = 0;
                        SearchResult = musicianSearchRepository.MusicianSearch(searchAfterMusicianModel, 0, 0);
                    }
                }

                catch (MySqlException)
                {
                    return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Es_gibt_zurzeit_ein_Datenbankproblem));
                }
                catch (Exception)
                {
                    return View("Message", new Message(Resources.Resources.Datenverarbeitungsproblem, Resources.Resources.Versuchen_Sie_es_später_erneut));
                }
                finally
                {
                    if (musicianSearchRepository != null)
                    {
                        musicianSearchRepository.Close();
                    }
                }
            }
            return View("SearchAfterMusicianResult", SearchResult);
        }

        [HttpPost]
        public ActionResult SaveSearch()
        {
            try
            {
                bandMemberSearchPropertiesRepository = new BandMemberSearchPropertiesRepository();
                bandMemberSearchPropertiesRepository.Open();
                bool error = false;
                if (Session["ChosenInstrumentIDs"] == null || Session["SkillMin"] == null || Session["SkillMax"] == null || Session["CityDistance"] == null || Session["BandID"] == null)
                {
                    error = true;
                }
                int? BandID = (int?)Session["BandID"];
                int? SkillMin = (int?)Session["SkillMin"];
                int? SkillMax = (int?)Session["SkillMax"];
                int? CityDistance = (int?)Session["CityDistance"];
                int[] ChosenInstrumentIDs = (int[])Session["ChosenInstrumentIDs"];
                bandMemberSearchPropertiesRepository.InsertBandMemberSearchProperty(new BandMember_SearchProperty(null, BandID, SkillMin, SkillMax, CityDistance));
                List<BandMember_SearchProperty> result = bandMemberSearchPropertiesRepository.GetAllBandMemberSearchPropertiesOfBand((int)BandID);
                int index = 0;
                foreach (BandMember_SearchProperty bs in result)
                {
                    if (bs.CityDistance_max == CityDistance && bs.SkillMin == SkillMin && bs.SkillMax == SkillMax)
                    {
                        break;
                    }
                    index++;
                }
                bandMemberSearchInstrumentsRepository = new BandMemberSearchInstrumentsRepository();
                bandMemberSearchInstrumentsRepository.Open();
                foreach (int id in ChosenInstrumentIDs)
                {
                    bandMemberSearchInstrumentsRepository.InsertBandMemberSearchInstrument(new BandMember_SearchInstrument(result[index].BandMember_SearchId, id));
                }
                if (error)
                    return Json(new { status = "error", message = "error saving Search" });

                return Json(new { status = "success", message = "Search saved" });
            }
            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Es_gibt_zurzeit_ein_Datenbankproblem));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Datenverarbeitungsproblem, Resources.Resources.Versuchen_Sie_es_später_erneut));
            }
            finally
            {
                if (bandMemberSearchInstrumentsRepository != null)
                    bandMemberSearchInstrumentsRepository.Close();
            }
        }
        public ActionResult CurrentSearchesAfterMusician()
        {
            if (Session["Band"] != null)
            {
                bandMemberSearchPropertiesRepository = new BandMemberSearchPropertiesRepository();
                bandMemberSearchPropertiesRepository.Open();
                List<BandMember_SearchProperty> model = bandMemberSearchPropertiesRepository.GetAllBandMemberSearchPropertiesOfBand(((Band)Session["Band"]).BandID);

                return View(model);
            }
            else
            {
                RedirectToAction("Home");
            }
            return View();
        }

        public ActionResult SearchDecision()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetSearchAfterModelWithSearchId(int searchid)
        {
            if (Session["Band"] != null)
            {
                musicianSearchRepository = new MusicianSearchRepository();
                musicianSearchRepository.Open();
                SearchAfterMusicianModel searchAfterMusicianModel = musicianSearchRepository.GetSearchAfterModelWithSearchId(searchid);

                return SearchAfterMusician(searchAfterMusicianModel);
            }
            else
            {
                RedirectToAction("SeachAfterMusician", "SearchAfterMusician");
            }
            return View();
        }
    }
}
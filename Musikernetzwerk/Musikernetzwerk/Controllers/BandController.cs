﻿using Musikernetzwerk.Models.DB;
using Musikernetzwerk.Models;
using Musikernetzwerk.Models.ViewModels;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Musikernetzwerk.Controllers;

namespace Musikernetzwerk.Controllers
{
    public class BandController : Controller
    {
        IBandRepository bandRepository;
        IMusicianBandRequestRepository musicianbandRequestRepository;
        IBandMembersRepository bandmemberrepository;
        IBandMemberRequestRepository bandMemberRequestRepository;
        IGenreRepository genreRepository;

        [HttpGet]
        public ActionResult GetBandPrivate(int bandId)
        {
            try
            {
                bandRepository = new BandRepository();
                bandRepository.Open();

                Band b = bandRepository.GetBand(bandId);
                BandModel bandModel = bandRepository.GetBandModel(b);

                if (b != null)
                {
                    return View(bandModel);
                }
                else
                {
                    return View("Message", new Message(Resources.Resources.Band_Fehler, Resources.Resources.Keine_Band_gefunden));
                }
            }

            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Es_gibt_zurzeit_ein_Datenbankproblem));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Allgemeiner_Fehler, Resources.Resources.Es_ist_ein_allgemeiner_Fehler_aufgetreten));
            }

            finally
            {
                bandRepository.Close();
            }
        }

        public ActionResult GetBandPublic(int bandId)
        {
            try
            {
                bandRepository = new BandRepository();
                bandRepository.Open();

                Band b = bandRepository.GetBand(bandId);
                BandModel bandModel = bandRepository.GetBandModel(b);

                if (b != null)
                {
                    return View(bandModel);
                }
                else
                {
                    return View();
                }
            }

            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Es_gibt_zurzeit_ein_Datenbankproblem));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Allgemeiner_Fehler, Resources.Resources.Es_ist_ein_allgemeiner_Fehler_aufgetreten));
            }

            finally
            {
                bandRepository.Close();
            }
        }

        [HttpGet]
        public ActionResult ChangeBandDataWithGenres()
        {
            BandModel bandModel = new BandModel();
            if (Session["Band"] != null)
            {
                bandRepository = new BandRepository();
                bandRepository.Open();
                Band b = (Band)Session["Band"];
                bandModel = bandRepository.GetBandModel(b);
                Session["bandmodel"] = bandModel;
            }
            else
            {
                return RedirectToAction("Login", "LoginBand");
            }
            return View(bandModel);
        }


        [HttpPost]
        public ActionResult ChangeBandDataWithGenres(BandModel bandmodel)
        {
            bandRepository = new BandRepository();
            Band oldband = (Band)Session["Band"];
            bandmodel.Band.BandID = oldband.BandID;
            bandmodel.Band.Userlanguage = oldband.Userlanguage;
            genreRepository = new GenreRepository();
            bool noFail = false;
            if ((bandmodel != null) || (bandmodel.NewChosenGenreIDs.Length > 0))
            {
                int zerocount = 0;
                foreach (int id in bandmodel.NewChosenGenreIDs)
                {
                    if (id == 0)
                    {
                        zerocount++;
                    }
                }
                if (bandmodel.NewChosenGenreIDs.Length == zerocount) { return RedirectToAction("ChangeBandDataWithGenres"); }
                try
                {
                    genreRepository.Open();
                    Genre g;
                    foreach (int id in bandmodel.NewChosenGenreIDs)
                    {
                        if (id > 0)
                        {
                            g = genreRepository.GetGenre(id);
                            bandmodel.NewChosenGenres.Add(g);
                        }
                    }
                    Session["bandmodel"] = bandmodel;
                    if ((bandmodel.Band != null) && (bandmodel.NewChosenGenres != null) && (bandmodel.City != null))
                    {
                        bandRepository.Open();
                        noFail = bandRepository.ChangeBandDataWithGenres(bandmodel);
                    }
                    if (noFail == true)
                    {
                        Session["Band"] = bandmodel.Band;
                        return View("Message", new Message(Resources.Resources.Geändert, Resources.Resources.Musikeränderungen_wurden_durchgeführt));
                    }
                    else
                    {
                        return View(bandmodel);
                    }

                }
                catch (MySqlException)
                {
                    return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Es_gibt_zurzeit_ein_Datenbankproblem));
                }
                catch (Exception)
                {
                    return View("Message", new Message(Resources.Resources.Allgemeiner_Fehler, Resources.Resources.Es_ist_ein_allgemeiner_Fehler_aufgetreten));
                }
                finally
                {
                    if (genreRepository != null)
                        genreRepository.Close();
                }
            }
            else
            {
                return RedirectToAction("ChangeBandDataWithGenres");
            }
        }


        [HttpGet]
        public ActionResult GetMusicianRequests()
        {
            try
            {
                Musician m = (Musician)Session["Musician"];
                if (m != null)
                {
                    bandMemberRequestRepository = new BandMemberRequestRepository();
                    bandMemberRequestRepository.Open();

                    List<BandMemberRequestModel> bandMember_Requests = bandMemberRequestRepository.GetAllMusicianRequests(m.MusicianID);


                    if (bandMember_Requests != null)
                    {
                        return View(bandMember_Requests);
                    }
                    else
                    {
                        return View("Message", new Message(Resources.Resources.Anfragen_Fehler, Resources.Resources.Keine_Anfragen_gefunden));
                    }
                }
                else
                {
                    return RedirectToAction("LoginMusician", "Login");
                }
            }
            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Es_gibt_zurzeit_ein_Datenbankproblem));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Allgemeiner_Fehler, Resources.Resources.Es_ist_ein_allgemeiner_Fehler_aufgetreten));
            }

            finally
            {
                bandMemberRequestRepository.Close();
            }
        }



        [HttpGet]
        public ActionResult GetBandRequests()
        {
            try
            {
                Band b = (Band)Session["Band"];
                if (b != null)
                {
                    musicianbandRequestRepository = new MusicianBandRequestRepository();
                    musicianbandRequestRepository.Open();

                    List<MusicianBandRequestModel> musicianBand_Requests = musicianbandRequestRepository.GetAllBandRequests(b.BandID);


                    if (musicianBand_Requests != null)
                    {
                        return View(musicianBand_Requests);
                    }
                    else
                    {
                        return View("Message", new Message(Resources.Resources.Anfragen_Fehler, Resources.Resources.Keine_Anfragen_gefunden));
                    }
                }
                else
                {
                    return RedirectToAction("LoginBand", "Login");
                }
            }
            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Es_gibt_zurzeit_ein_Datenbankproblem));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Allgemeiner_Fehler, Resources.Resources.Es_ist_ein_allgemeiner_Fehler_aufgetreten));
            }

            finally
            {
                musicianbandRequestRepository.Close();
            }
        }

        [HttpGet]
        public ActionResult AcceptRequest(int musicianbandRequestId)
        {
            try
            {
                musicianbandRequestRepository = new MusicianBandRequestRepository();
                musicianbandRequestRepository.Open();

                MusicianBandRequestModel r = musicianbandRequestRepository.GetMusicianBandRequest(musicianbandRequestId);


                if ((r != null) && (r.Request.BandID != null) && (r.Request.MusicianID != null) && (r.Musician.MusicianID != -1))
                {
                    bandmemberrepository = new BandMembersRepository();
                    bandmemberrepository.Open();
                    bandmemberrepository.InsertBandMember((int)r.Request.BandID, (int)r.Request.MusicianID);
                    bandRepository = new BandRepository();
                    bandRepository.Open();
                    bandRepository.IncreaseMemberCount((int)r.Request.BandID);
                    musicianbandRequestRepository.Open();
                    musicianbandRequestRepository.DeleteMusicianBandRequest(musicianbandRequestId);
                    string src = "<script language='javascript' type='text/javascript'> window.history.back(); alert('" + Resources.Resources.Anfrage_akzeptiert + "'); </script>";
                    return Content(src);
                }
                else
                {
                    return View("Message", new Message(Resources.Resources.Anfragen_Fehler, Resources.Resources.Akzeptieren_fehlgeschlagen));
                }
            }
            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Akzeptieren_aufgrund_eines_Datenbankfehlers_fehlgeschlagen));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Allgemeiner_Fehler, Resources.Resources.Akzeptieren_aufgrund_eines_allgemeinen_Fehlers_fehlgeschlagen));
            }

            finally
            {
                if (bandRepository != null)
                {
                    bandRepository.Close();
                }
                musicianbandRequestRepository.Close();
            }
        }

        [HttpGet]
        public ActionResult RejectRequest(int musicianbandRequestId)
        {
            try
            {
                musicianbandRequestRepository = new MusicianBandRequestRepository();
                musicianbandRequestRepository.Open();

                bool r = musicianbandRequestRepository.DeleteMusicianBandRequest(musicianbandRequestId);

                if (r == true)
                {
                    string src = "<script language='javascript' type='text/javascript'> window.history.back(); alert('" + Resources.Resources.Anfrage_abgelehnt + "'); </script>";
                    return Content(src);
                }
                else
                {
                    return View("Message", new Message(Resources.Resources.Anfragen_Fehler, Resources.Resources.Ablehnen_fehlgeschlagen));
                }
            }
            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Ablehnen_aufgrund_eines_Datenbankfehlers_fehlgeschlagen));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Allgemeiner_Fehler, Resources.Resources.Ablehnen_aufgrund_eines_allgemeinen_Fehlers_fehlgeschlagen));
            }

            finally
            {

                musicianbandRequestRepository.Close();
            }
        }

        public ActionResult SendRequestToMusician(int musicianid)
        {
            try
            {
                bandMemberRequestRepository = new BandMemberRequestRepository();
                bandMemberRequestRepository.Open();

                Band b = (Band)Session["Band"];
                if (b != null)
                {
                    bool sr = bandMemberRequestRepository.InsertBandMemberRequest((int)musicianid, b.BandID);

                    if (sr == true)
                    {
                        string src = "<script language='javascript' type='text/javascript'> window.history.back(); alert('" + Resources.Resources.Anfrage_gesendet + "'); </script>";
                        return Content(src);
                    }
                    else
                    {
                        return View("Message", new Message(Resources.Resources.Anfragen_Fehler, Resources.Resources.Anfrage_konnte_nicht_gesendet_werden));
                    }
                }
                else
                {
                    return View("Message", new Message(Resources.Resources.Anfragen_Fehler, Resources.Resources.Anfrage_konnte_nicht_gesendet_werden));
                }
            }
            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Es_gibt_zurzeit_ein_Datenbankproblem));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Allgemeiner_Fehler, Resources.Resources.Es_ist_ein_allgemeiner_Fehler_aufgetreten));
            }

            finally
            {

                bandMemberRequestRepository.Close();
            }
        }

        [HttpGet]
        public ActionResult GetBandSavedSearches()
        {
            try
            {
                Band b = (Band)Session["Band"];
                if (b != null)
                {
                    bandRepository = new BandRepository();
                    bandRepository.Open();

                    List<BandSavedSearchModel> band_SavedSearches = bandRepository.GetAllBandSavedSearches(b.BandID);

                    if (band_SavedSearches != null)
                    {
                        return View(band_SavedSearches);
                    }
                    else
                    {
                        return View("Message", new Message(Resources.Resources.Gespeicherte_Suchen_Fehler, Resources.Resources.Keine_gespeicherten_Suchen_bei_dieser_Band_vorhanden));
                    }
                }
                else
                {
                    return RedirectToAction("LoginBand", "Login");
                }
            }
            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Es_gibt_zurzeit_ein_Datenbankproblem));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Allgemeiner_Fehler, Resources.Resources.Es_ist_ein_allgemeiner_Fehler_aufgetreten));
            }

            finally
            {
                bandRepository.Close();
            }
        }

        public ActionResult GetSavedSearchOfBand(int searchid)
        {
            try
            {
                Band b = (Band)Session["Band"];
                if (b != null)
                {
                    bandRepository = new BandRepository();
                    bandRepository.Open();


                    BandSavedSearchModel savedSearchOfBand = bandRepository.GetBandSavedSearch(searchid);
                    if (savedSearchOfBand != null)
                    {
                        return View(savedSearchOfBand);
                    }
                    else
                    {
                        return View("Message", new Message(Resources.Resources.Gespeicherte_Suchen_Fehler, Resources.Resources.Keine_gespeicherten_Suchen_bei_dieser_Band_vorhanden));
                    }
                }

                else
                {
                    return View("Message", new Message(Resources.Resources.Band_Fehler, Resources.Resources.Keine_Band_gefunden));
                }
            }
            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Es_gibt_zurzeit_ein_Datenbankproblem));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Allgemeiner_Fehler, Resources.Resources.Es_ist_ein_allgemeiner_Fehler_aufgetreten));
            }

            finally
            {
                bandRepository.Close();
            }
        }

        public ActionResult DeleteSavedSearchOfBand(int searchid)
        {
            try
            {
                Band b = (Band)Session["Band"];
                if (b != null)
                {
                    bandRepository = new BandRepository();
                    bandRepository.Open();


                    bool deleteSavedSearchOfBand = bandRepository.DeleteBandSavedSearch(searchid);
                    if (deleteSavedSearchOfBand != false)
                    {
                        return RedirectToAction("GetBandSavedSearches");
                    }
                    else
                    {
                        return View("Message", new Message(Resources.Resources.Gespeicherte_Suchen_Fehler, Resources.Resources.Gespeicherte_Suche_konnte_nicht_gelöscht_werden));
                    }
                }

                else
                {
                    return View("Message", new Message(Resources.Resources.Band_Fehler, Resources.Resources.Keine_Band_gefunden));
                }
            }
            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Es_gibt_zurzeit_ein_Datenbankproblem));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Allgemeiner_Fehler, Resources.Resources.Es_ist_ein_allgemeiner_Fehler_aufgetreten));
            }

            finally
            {
                bandRepository.Close();
            }
        }
    }
}

﻿using Musikernetzwerk.Models.DB;
using Musikernetzwerk.Models;
using Musikernetzwerk.Models.ViewModels;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Musikernetzwerk.Controllers;

namespace Musikernetzwerk.Controllers
{
    public class MusicianController : Controller
    {
        IMusicianRepository musicianRepository;
        IBandMemberRequestRepository bandmemberRequestRepository;
        IBandMembersRepository bandmemberrepository;
        IBandRepository bandrepository;
        IMusicianBandRequestRepository musicianBandRequestRepository;
        IInstrumentRepository instrumentRepository;

        [HttpGet]
        public ActionResult GetMusicianPrivate(int musicianId)
        {
            try
            {
                musicianRepository = new MusicianRepository();
                musicianRepository.Open();

                Musician m = musicianRepository.GetMusician(musicianId);
                MusicianModel musicianModel = musicianRepository.GetMusicianModel(m);

                if (m != null)
                {
                    return View(musicianModel);
                }
                else
                {
                    return View("Message", new Message(Resources.Resources.Musiker_Fehler, Resources.Resources.Kein_Musiker_gefunden));
                }
            }
            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Es_gibt_zurzeit_ein_Datenbankproblem));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Allgemeiner_Fehler, Resources.Resources.Es_ist_ein_allgemeiner_Fehler_aufgetreten));
            }

            finally
            {
                musicianRepository.Close();
            }
        }

        [HttpGet]
        public ActionResult GetMusicianPublic(int musicianId)
        {
            try
            {
                musicianRepository = new MusicianRepository();
                musicianRepository.Open();

                Musician m = musicianRepository.GetMusician(musicianId);
                MusicianModel musicianModel = musicianRepository.GetMusicianModel(m);

                if (m != null)
                {
                    return View(musicianModel);
                }
                else
                {
                    return View("Message", new Message(Resources.Resources.Musiker_Fehler, Resources.Resources.Kein_Musiker_gefunden));
                }
            }
            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Es_gibt_zurzeit_ein_Datenbankproblem));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Allgemeiner_Fehler, Resources.Resources.Es_ist_ein_allgemeiner_Fehler_aufgetreten));
            }

            finally
            {
                musicianRepository.Close();
            }
        }

        [HttpGet]
        public ActionResult ChangeMusicianDataWithInstruments()
        {
            MusicianModel musicianModel = new MusicianModel();
            if (Session["Musician"] != null)
            {
                musicianRepository = new MusicianRepository();
                musicianRepository.Open();
                Musician m = (Musician)Session["Musician"];
                musicianModel = musicianRepository.GetMusicianModel(m);
                Session["musicianmodel"] = musicianModel;
            }
            else
            {
                return RedirectToAction("Login", "LoginMusician");
            }
            return View(musicianModel);
        }

        [HttpPost]
        public ActionResult ChangeMusicianDataWithInstruments(MusicianModel musicianmodel)
        {
            Musician oldmusician = (Musician)Session["Musician"];
            musicianmodel.Musician.MusicianID = oldmusician.MusicianID;
            musicianmodel.Musician.Userlanguage = oldmusician.Userlanguage;
            instrumentRepository = new InstrumentRepository();
            if ((musicianmodel != null) || (musicianmodel.NewChosenInstrumentIDs.Length > 0))
            {
                int zerocount = 0;
                foreach (int id in musicianmodel.NewChosenInstrumentIDs)
                {
                    if (id == 0)
                    {
                        zerocount++;
                    }
                }
                if (musicianmodel.NewChosenInstrumentIDs.Length == zerocount) { return RedirectToAction("ChangeMusicianDataWithInstruments"); }
                try
                {
                    instrumentRepository.Open();
                    Instrument i;
                    foreach (int id in musicianmodel.NewChosenInstrumentIDs)
                    {
                        if (id > 0)
                        {
                            i = instrumentRepository.GetInstrument(id);
                            musicianmodel.NewChosenInstruments.Add(i);
                        }
                    }
                    Session["musicianmodel"] = musicianmodel;
                    return View("ChangeMusicianDataWithInstrumentsFinal", musicianmodel);
                }
                catch (MySqlException)
                {
                    return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Es_gibt_zurzeit_ein_Datenbankproblem));
                }
                catch (Exception)
                {
                    return View("Message", new Message(Resources.Resources.Allgemeiner_Fehler, Resources.Resources.Es_ist_ein_allgemeiner_Fehler_aufgetreten));
                }
                finally
                {
                    if (instrumentRepository != null)
                        instrumentRepository.Close();
                }
            }
            return RedirectToAction("ChangeMusicianDataWithInstruments");
        }

        [HttpPost]
        public ActionResult ChangeMusicianDataWithInstrumentsFinal(MusicianModel musicianmodel)
        {
            MusicianModel m = (MusicianModel)Session["musicianmodel"];
            musicianRepository = new MusicianRepository();
            for (int index = 0; index < musicianmodel.NewChosenInstruments.Count; index++)
            {
                musicianmodel.NewChosenInstruments[index].InstrumentID = m.NewChosenInstruments[index].InstrumentID;
            }
            bool noFail = false;
            if ((musicianmodel != null) && (m != null))
            {
                m.NewChosenInstruments = musicianmodel.NewChosenInstruments;
                try
                {
                    if ((m.Musician != null) && (m.NewChosenInstruments != null) && (m.City != null))
                    {
                        musicianRepository.Open();
                        noFail = musicianRepository.ChangeMusicianDataWithInstruments(m);
                    }
                }
                catch (MySqlException)
                {
                    return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Es_gibt_zurzeit_ein_Datenbankproblem));
                }
                catch (Exception)
                {
                    return View("Message", new Message(Resources.Resources.Allgemeiner_Fehler, Resources.Resources.Es_ist_ein_allgemeiner_Fehler_aufgetreten));
                }
                finally
                {
                    musicianRepository.Close();
                }
                if (noFail == true)
                {
                    Session["Musician"] = m.Musician;
                    return View("Message", new Message(Resources.Resources.Geändert, Resources.Resources.Musikeränderungen_wurden_durchgeführt));
                }
                else
                {
                    return View(m);
                }
            }
            else
            {
                return RedirectToAction("ChangeMusicianData");
            }

        }

        [HttpGet]
        public ActionResult GetMusicianRequests()
        {
            try
            {
                Musician m = (Musician)Session["Musician"];
                if (m != null)
                {
                    bandmemberRequestRepository = new BandMemberRequestRepository();
                    bandmemberRequestRepository.Open();

                    List<BandMemberRequestModel> bandMember_Requests = bandmemberRequestRepository.GetAllMusicianRequests(m.MusicianID);


                    if (bandMember_Requests != null)
                    {
                        return View(bandMember_Requests);
                    }
                    else
                    {
                        return View("Message", new Message(Resources.Resources.Anfragen_Fehler, Resources.Resources.Keine_Anfragen_gefunden));
                    }
                }
                else
                {
                    return RedirectToAction("LoginMusician", "Login");
                }
            }
            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Es_gibt_zurzeit_ein_Datenbankproblem));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Allgemeiner_Fehler, Resources.Resources.Es_ist_ein_allgemeiner_Fehler_aufgetreten));
            }

            finally
            {
                bandmemberRequestRepository.Close();
            }
        }

        [HttpGet]
        public ActionResult AcceptRequest(int bandMemberRequestId)
        {
            try
            {
                bandmemberRequestRepository = new BandMemberRequestRepository();
                bandmemberRequestRepository.Open();

                BandMemberRequestModel r = bandmemberRequestRepository.GetBandMemberRequest(bandMemberRequestId);

                if ((r != null) && (r.Request.BandID != null) && (r.Request.MusicianID != null) && (r.Band.BandID != -1))
                {
                    bandmemberrepository = new BandMembersRepository();
                    bandmemberrepository.Open();
                    bandmemberrepository.InsertBandMember((int)r.Request.BandID, (int)r.Request.MusicianID);
                    bandrepository = new BandRepository();
                    bandrepository.Open();
                    bandrepository.IncreaseMemberCount((int)r.Band.BandID);
                    bandmemberRequestRepository.Open();
                    bandmemberRequestRepository.DeleteBandMemberRequest(bandMemberRequestId);
                    string src = "<script language='javascript' type='text/javascript'> window.history.back(); alert('" + Resources.Resources.Anfrage_akzeptiert +
                            "'); </script>";
                    return Content(src);
                }
                else
                {
                    return View("Message", new Message(Resources.Resources.Anfragen_Fehler, Resources.Resources.Akzeptieren_fehlgeschlagen));
                }
            }
            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Akzeptieren_aufgrund_eines_Datenbankfehlers_fehlgeschlagen));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Allgemeiner_Fehler, Resources.Resources.Akzeptieren_aufgrund_eines_allgemeinen_Fehlers_fehlgeschlagen));
            }

            finally
            {
                if (bandrepository != null)
                {
                    bandrepository.Close();
                }
                bandmemberRequestRepository.Close();
            }
        }

        [HttpGet]
        public ActionResult RejectRequest(int bandMemberRequestId)
        {
            try
            {
                bandmemberRequestRepository = new BandMemberRequestRepository();
                bandmemberRequestRepository.Open();

                bool r = bandmemberRequestRepository.DeleteBandMemberRequest(bandMemberRequestId);

                if (r == true)
                {
                    string src = "<script language='javascript' type='text/javascript'> window.history.back(); alert('" + Resources.Resources.Anfrage_abgelehnt +
                            "'); </script>";
                    return Content(src);
                }
                else
                {
                    return View("Message", new Message(Resources.Resources.Anfragen_Fehler, Resources.Resources.Ablehnen_fehlgeschlagen));

                }
            }
            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Ablehnen_aufgrund_eines_Datenbankfehlers_fehlgeschlagen));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Allgemeiner_Fehler, Resources.Resources.Ablehnen_aufgrund_eines_allgemeinen_Fehlers_fehlgeschlagen));
            }

            finally
            {
                bandmemberRequestRepository.Close();
            }
        }

        public ActionResult SendRequestToBand(int bandid)
        {
            try
            {
                musicianBandRequestRepository = new MusicianBandRequestRepository();
                musicianBandRequestRepository.Open();

                Musician m = (Musician)Session["Musician"];
                if (m != null)
                {
                    bool sr = musicianBandRequestRepository.InsertMusicianBandRequest((int)bandid, m.MusicianID);

                    if (sr == true)
                    {
                        string src = "<script language='javascript' type='text/javascript'> window.history.back(); alert('" + Resources.Resources.Anfrage_gesendet +
                            "'); </script>";
                        return Content(src);
                    }
                    else
                    {
                        return View("Message", new Message(Resources.Resources.Anfragen_Fehler, Resources.Resources.Anfrage_konnte_nicht_gesendet_werden));
                    }
                }
                else
                {
                    return View("Message", new Message(Resources.Resources.Anfragen_Fehler, Resources.Resources.Anfrage_konnte_nicht_gesendet_werden));
                }

            }
            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Es_gibt_zurzeit_ein_Datenbankproblem));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Allgemeiner_Fehler, Resources.Resources.Es_ist_ein_allgemeiner_Fehler_aufgetreten));
            }

            finally
            {
                musicianBandRequestRepository.Close();
            }
        }

        [HttpGet]
        public ActionResult GetMusicianSavedSearches()
        {
            try
            {
                Musician m = (Musician)Session["Musician"];
                if (m != null)
                {
                    musicianRepository = new MusicianRepository();
                    musicianRepository.Open();

                    List<MusicianSavedSearchModel> musician_SavedSearches = musicianRepository.GetAllMusicianSavedSearches(m.MusicianID);

                    if (musician_SavedSearches != null)
                    {
                        return View(musician_SavedSearches);
                    }
                    else
                    {
                        return View("Message", new Message(Resources.Resources.Gespeicherte_Suchen_Fehler, Resources.Resources.Keine_gespeicherten_Suchen_bei_diesem_Musiker_vorhanden));
                    }
                }
                else
                {
                    return RedirectToAction("LoginMusician", "Login");
                }
            }
            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Es_gibt_zurzeit_ein_Datenbankproblem));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Allgemeiner_Fehler, Resources.Resources.Es_ist_ein_allgemeiner_Fehler_aufgetreten));
            }

            finally
            {
                musicianRepository.Close();
            }
        }

        public ActionResult DeleteSavedSearchOfMusician(int searchid)
        {
            try
            {
                Musician m = (Musician)Session["Musician"];
                if (m != null)
                {
                    musicianRepository = new MusicianRepository();
                    musicianRepository.Open();

                    bool deleteSavedSearchOfMusician = musicianRepository.DeleteMusicianSavedSearch(searchid);
                    if (deleteSavedSearchOfMusician == true)
                    {
                        return RedirectToAction("GetMusicianSavedSearches");
                    }
                    else
                    {
                        return View("Message", new Message(Resources.Resources.Gespeicherte_Suchen_Fehler, Resources.Resources.Gespeicherte_Suche_konnte_nicht_gelöscht_werden));
                    }
                }

                else
                {
                    return View("Message", new Message(Resources.Resources.Musiker_Fehler, Resources.Resources.Kein_Musiker_gefunden));
                }
            }
            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Es_gibt_zurzeit_ein_Datenbankproblem));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Allgemeiner_Fehler, Resources.Resources.Es_ist_ein_allgemeiner_Fehler_aufgetreten));
            }

            finally
            {
                musicianRepository.Close();
            }
        }
    }
}












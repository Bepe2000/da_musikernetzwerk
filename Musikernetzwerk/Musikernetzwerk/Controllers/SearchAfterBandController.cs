﻿using Musikernetzwerk.Models;
using Musikernetzwerk.Models.DB;
using Musikernetzwerk.Models.ViewModels;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Musikernetzwerk.Controllers
{
    public class SearchAfterBandController : Controller
    {
        IBandSearchRepository bandSearchRepository;
        ICityRepository cityRepository;
        IMusicianBandSearchPropertiesRepository bandSearchPropertiesRepository;
        IMusicianBandSearchGenresRepository bandSearchGenresRepository;
        IGenreRepository genreRepository;

        [HttpGet]
        public ActionResult SearchAfterBand()
        {
            SearchAfterBandModel b = new SearchAfterBandModel();
            try
            {
                genreRepository = new GenreRepository();
                genreRepository.Open();

                List<Genre> g = genreRepository.GetAllGenres();

                if (g != null)
                {
                    b.Genres = new MultiSelectList(g, "GenreID", "De");
                    if (Resources.Resources.Culture != null)
                    {
                        string lang = (string)Resources.Resources.Culture.ToString();
                        if (lang != null)
                        {
                            switch (lang)
                            {
                                case "en":
                                    b.Genres = new MultiSelectList(g, "GenreID", "En");
                                    break;
                                default:
                                    b.Genres = new MultiSelectList(g, "GenreID", "De");
                                    break;
                            }
                        }
                    }
                    Band user = (Band)Session["Band"];
                    if (user != null)
                    {
                        switch (user.Userlanguage)
                        {
                            case "en":
                                b.Genres = new MultiSelectList(g, "GenreID", "En");
                                break;
                            default:
                                b.Genres = new MultiSelectList(g, "GenreID", "De");
                                break;
                        }
                    }
                    if (user == null)
                    {
                        Musician user2 = (Musician)Session["Musician"];
                        if (user2 != null)
                        {
                            switch (user2.Userlanguage)
                            {
                                case "en":
                                    b.Genres = new MultiSelectList(g, "GenreID", "En");
                                    break;
                                default:
                                    b.Genres = new MultiSelectList(g, "GenreID", "De");
                                    break;
                            }
                        }
                    }
                    return View(b);
                }
                else
                {
                    return View("Message", new Message(Resources.Resources.Genre_Fehler, Resources.Resources.Keine_Genres_gefunden));
                }

            }
            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Es_gibt_zurzeit_ein_Datenbankproblem));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Allgemeiner_Fehler, Resources.Resources.Es_ist_ein_allgemeiner_Fehler_aufgetreten));
            }

            finally
            {
                genreRepository.Close();
            }
        }

        [HttpPost]
        public ActionResult SearchAfterBand(SearchAfterBandModel searchAfterBandModel)
        {
            List<SearchAfterBandResultModel> SearchResult = new List<SearchAfterBandResultModel>();

            if (searchAfterBandModel != null)
            {
                try
                {
                    bandSearchRepository = new BandSearchRepository();
                    bandSearchRepository.Open();

                    if (Session["Band"] != null)
                    {
                        Band b = (Band)Session["Band"];
                        cityRepository = new CityRepository();
                        cityRepository.Open();
                        City c = cityRepository.GetCity((int)b.CityID);
                        SearchResult = bandSearchRepository.BandSearch(searchAfterBandModel, (double)c.Latitude, (double)c.Longitude);
                    }
                    else if (Session["Musician"] != null)
                    {
                        Musician m = (Musician)Session["Musician"];
                        Session["SkillMin"] = searchAfterBandModel.SkillMin;
                        Session["SkillMax"] = searchAfterBandModel.SkillMax;
                        Session["CityDistance"] = searchAfterBandModel.CityDistance;
                        Session["ChosenGenreIDs"] = searchAfterBandModel.ChosenGenreIDs;
                        Session["MusicianID"] = m.MusicianID;

                        cityRepository = new CityRepository();
                        cityRepository.Open();
                        City c = cityRepository.GetCity((int)m.CityID);
                        SearchResult = bandSearchRepository.BandSearch(searchAfterBandModel, (float)c.Latitude, (float)c.Longitude);
                    }
                    else
                    {
                        searchAfterBandModel.CityDistance = 0;
                        SearchResult = bandSearchRepository.BandSearch(searchAfterBandModel, 0, 0);
                    }
                }

                catch (MySqlException)
                {
                    return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Es_gibt_zurzeit_ein_Datenbankproblem));
                }
                catch (Exception)
                {
                    return View("Message", new Message(Resources.Resources.Datenverarbeitungsproblem, Resources.Resources.Versuchen_Sie_es_später_erneut));
                }
                finally
                {
                    if (bandSearchRepository != null)
                    {
                        bandSearchRepository.Close();
                    }
                }
            }
            return View("SearchAfterBandResult", SearchResult);
        }

        [HttpPost]
        public ActionResult saveSearch()
        {
            try
            {
                bandSearchPropertiesRepository = new MusicianBandSearchPropertiesRepository();
                bandSearchPropertiesRepository.Open();
                bool error = false;
                if ((Session["ChosenGenreIDs"] == null) || Session["SkillMin"] == null || Session["SkillMax"] == null || Session["CityDistance"] == null || Session["BandID"] == null)
                {
                    error = true;
                }
                int? MusicianID = (int?)Session["MusicianID"];
                int? SkillMin = (int?)Session["SkillMin"];
                int? SkillMax = (int?)Session["SkillMax"];
                int? MemberCountMin = (int?)Session["MemberCount_Min"];
                int? MemberCountMax = (int?)Session["MemberCount_Max"];
                int? CityDistance = (int?)Session["CityDistance"];
                int[] ChosenGenreIDs = (int[])Session["ChosenGenreIDs"];
                bandSearchPropertiesRepository.InsertMusicianBandSearchProperty(new MusicianBand_SearchProperty(null, MusicianID, SkillMin, SkillMax, MemberCountMin, MemberCountMax, CityDistance));
                List<MusicianBand_SearchProperty> result = bandSearchPropertiesRepository.GetAllMusicianBandSearchPropertiesOfMusician((int)MusicianID);
                int index = 0;
                foreach (MusicianBand_SearchProperty bs in result)
                {
                    if (bs.CityDistance_max == CityDistance && bs.SkillMin == SkillMin && bs.SkillMax == SkillMax)
                    {
                        break;
                    }
                    index++;
                }

                bandSearchGenresRepository = new MusicianBandSearchGenresRepository();
                bandSearchGenresRepository.Open();
                foreach (int id in ChosenGenreIDs)
                {
                    bandSearchGenresRepository.InsertBandSearchGenre(new MusicianBand_SearchGenre(result[index].MusicianBand_SearchID, id));
                }

                if (error)
                    return Json(new { status = "error", message = "error saving Search" });

                return Json(new { status = "success", message = "Search saved" });
            }
            catch (MySqlException)
            {
                return View("Message", new Message(Resources.Resources.Datenbank_Fehler, Resources.Resources.Es_gibt_zurzeit_ein_Datenbankproblem));
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.Datenverarbeitungsproblem, Resources.Resources.Versuchen_Sie_es_später_erneut));
            }
        }
        public ActionResult CurrentSearchesAfterBand()
        {
            if (Session["Musician"] != null)
            {
                bandSearchPropertiesRepository = new MusicianBandSearchPropertiesRepository();
                bandSearchPropertiesRepository.Open();
                List<MusicianBand_SearchProperty> model = bandSearchPropertiesRepository.GetAllMusicianBandSearchPropertiesOfMusician(((Musician)Session["Musician"]).MusicianID);

                return View(model);
            }
            else
            {
                RedirectToAction("Home");
            }
            return View();
        }

        public ActionResult SearchDecision()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetSearchAfterModelWithSearchId(int searchid)
        {
            if (Session["Musician"] != null)
            {
                bandSearchRepository = new BandSearchRepository();
                bandSearchRepository.Open();
                SearchAfterBandModel searchAfterBandModel = bandSearchRepository.GetSearchAfterModelWithSearchId(searchid);

                return SearchAfterBand(searchAfterBandModel); ;
            }
            else
            {
                RedirectToAction("SeachAfterBand", "SearchAfterBand");
            }
            return View();
        }

    }
}
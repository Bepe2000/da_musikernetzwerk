﻿using Musikernetzwerk.Models;
using Musikernetzwerk.Models.DB;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Musikernetzwerk.Controllers
{
    public class HomeController : Controller
    {
        IMusicianRepository musicianRepository;
        IBandRepository bandRepository;
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult Imprint()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ChangeLang(string lang)
        {
            if (lang != null)
            {
                Musician mUser = (Musician)Session["Musician"];
                if (mUser != null)
                {
                    mUser.Userlanguage = lang;
                    musicianRepository = new MusicianRepository();
                    try
                    {
                        musicianRepository.Open();
                        musicianRepository.ChangeMusicianData(mUser);
                        Session["Musician"] = mUser;
                    }
                    catch (Exception)
                    {
                        return Json(new { success = false, responseText = "Error!" }, JsonRequestBehavior.AllowGet);
                    }
                    finally
                    {
                        musicianRepository.Close();
                    }
                }

                Band bUser = (Band)Session["Band"];
                if (bUser != null)
                {
                    bUser.Userlanguage = lang;
                    bandRepository = new BandRepository();
                    try
                    {
                        bandRepository.Open();
                        bandRepository.ChangeBandData(bUser);
                        Session["Band"] = bUser;
                    }
                    catch (Exception)
                    {
                        return Json(new { success = false, responseText = "Error!" }, JsonRequestBehavior.AllowGet);
                    }
                    finally
                    {
                        bandRepository.Close();
                    }
                }
            }


            if ((lang == null) || (lang == "de"))
            {
                Resources.Resources.Culture = CultureInfo.GetCultureInfo("");
            }
            else
            {
                Resources.Resources.Culture = CultureInfo.GetCultureInfo(lang);
            }
            return Redirect(Request.UrlReferrer.ToString());
        }

        [HttpGet]
        public ActionResult SendMail()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SendMail(EMail email)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    email.Subject = "Contact Message WeBand";
                    var senderEmail = new MailAddress(email.Address);
                    var receiverEmail = new MailAddress("message.weband@gmail.com");
                    var password = "musiker2019";
                    var sub = email.Subject;
                    var body = email.Message;
                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(receiverEmail.Address, password)
                    };
                    using (var mess = new MailMessage(senderEmail, receiverEmail)
                    {
                        Subject = email.Subject,
                        Body = body
                    })
                    {
                        smtp.Send(mess);
                    }
                    return View();
                }
            }
            catch (Exception)
            {
                return View("Message", new Message(Resources.Resources.E_Mail_Problem, Resources.Resources.Versuchen_Sie_es_später_erneut));
            }
            return View();
        }
    }
}
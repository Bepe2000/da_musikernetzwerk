﻿function transformNavBurger(x) {
  x.classList.toggle("change");

  var visible = document.getElementsByClassName("nav-items-responsive")[0];
  var notVisible = document.getElementsByClassName("nav-items-right")[0];
  if (visible) {
    visible.className = "nav-items-right";
  }
  if (notVisible) {
    notVisible.className = "nav-items-responsive";
  }
}

function loginLinkMusician() {
  window.location.href = "../Login/LoginMusician";
}

function loginLinkBand() {
  window.location.href = "../Login/LoginBand";
}



$(document).ready(function () {

    $("#hero-box-left").fadeIn("slow");
    $("#hero-box-right").fadeIn("slow");

    $("#musician-request-button").click(function() {
    $("#musician-request-button").text("\u2713");
    });
    $("#band-request-button").click(function() {
    $("#band-request-button").text("\u2713");
    });

    $("#search-city").click(function() {
    event.preventDefault();
    var cityname = $("#cityname")
        .val()
        .toString();
    var postalcode = $("#postalcode").val();
    if (cityname != "" && postalcode != "") {
        $.get("/Login/GetCitiesForRegistration", {
        cityname: cityname,
        postalcode: postalcode
        })
        .done(function(cities) {
            console.log(cities);
            $("#City_CityID")
            .find("option")
            .remove()
            .end();
            for (var i = 0; i < cities.length; i++) {
            $("#City_CityID").append(
                new Option(cities[i].CityName, cities[i].CityID)
            );
            }
        })
        .fail(function() {
            alert("Failed!");
        });
    } else {
        alert("Failed!");
    }
    });
   
});

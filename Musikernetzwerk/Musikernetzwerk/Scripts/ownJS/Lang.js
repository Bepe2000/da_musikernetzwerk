function checkNetConnection() {
  jQuery.ajaxSetup({ async: false });
  result = false;
  r = Math.round(Math.random() * 10000);
  $.get("/Content/Images/icons/icon-72x72.png", { randomval: r }, function(d) {
    result = true;
  }).fail(function(d) {
    result = false;
  });
  return result;
}

function set_lang(lang_var) {
  if (!checkNetConnection()) {
    fetch("/offline.html", { method: "GET", redirect: "follow" }).then(
      response => {
        if (!response.redirected) {
          window.location.href = response.url;
        }
      }
    );
  } else {
    localStorage.setItem("lang", lang_var);
    $.post({
      type: "POST",
      url: "/Home/ChangeLang/?lang=" + lang_var
    });
    localStorage.removeItem("cache_age");

    caches.delete("STW-Cache-" + window.location.origin).then(function() {
      location.reload();
    });
  }
}

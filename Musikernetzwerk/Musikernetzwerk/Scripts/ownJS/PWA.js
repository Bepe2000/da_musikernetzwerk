// DOCUMENT READY

$(document).ready(function() {
  if ("serviceWorker" in navigator) {
    window.addEventListener("load", function() {
      navigator.serviceWorker.register("/serviceworker.js").then(
        function() {
          localStorage.setItem("serviceworker", true);
        },
        function(err) {
          console.log("ServiceWorker Registrierung fehlgeschlagen: ", err);
        }
      );
    });
  }
  check_cache_seen();
});

// UPDATE CACHE
var urls = [
  "/",
  "/Home/Index",
  "/Home/Imprint",
  "/SearchAfterMusician/SearchDecision"
];
function update_cache_age() {
  caches.delete("STW-Cache-" + window.location.origin + "/");

  $.each(urls, function(i, u) {
    $.ajax({
      type: "GET",
      url: u
    });
  });

  var sec = Math.round(Date.now() / 1000);
  localStorage.setItem("cache_age", sec);
  return sec;
}

function check_cache_seen() {
  if (
    localStorage.getItem("serviceworker") != null &&
    localStorage.getItem("serviceworker") == "true"
  ) {
    var seen = localStorage.getItem("cache_age") || update_cache_age();
    var now = Math.round(Date.now() / 1000);
    var week = 604800;
    if (now - seen >= week) {
      update_cache_age();
    }
  }
}

// ADD TO HOMESCREEN

window.addEventListener("beforeinstallprompt", function(event) {
  // not show the default browser install app prompt
  event.preventDefault();
  var seen =
    localStorage.getItem("lastInstallTry_age") || update_lastInstallTry_age();
  var now = Math.round(Date.now() / 1000);
  var day = 604800 / 7;
  if (now - seen >= day) {
    update_lastInstallTry_age();

    $(".a2hs").css("visibility", "visible");
    $(".graywall").css("visibility", "visible");

    // save the event to use it later
    // (it has the important prompt method and userChoice property)
    window.promptEvent = event;
  }
});

function update_lastInstallTry_age() {
  var sec = Math.round(Date.now() / 1000);
  localStorage.setItem("lastInstallTry_age", sec);
  return sec;
}

$(".a2hs-cancel").click(function() {
  $(".a2hs").css("visibility", "hidden");
  $(".graywall").css("visibility", "hidden");
});

$(".a2hs-install").click(function() {
  addToHomeScreen();
});

function addToHomeScreen() {
  // show the install app prompt
  window.promptEvent.prompt();

  // handle the Decline/Accept choice of the user
  window.promptEvent.userChoice.then(function(choiceResult) {
    // hide the prompt banner here
    // …
    $(".a2hs").css("visibility", "hidden");
    $(".graywall").css("visibility", "hidden");

    window.promptEvent = null;
  });
}

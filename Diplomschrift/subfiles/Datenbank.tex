% !TEX root = ../Diplomschrift.tex
\documentclass[../Diplomschrift.tex]{subfiles}
\begin{document}
\subsection{Datenbank}\cfoot{\thesection$\;$-$\;$\rightmark}
\lfoot{Ladner}
\subsubsection{Theoretische Grundlagen}
Da die Datenbank dieses Diplomprojektes das Fundament der Webanwendung bildet, werden hier die theoretischen Grundlagen, nach denen die Datenbank entworfen wurde, kurz erläutert. \\\\
Grundsätzlich gilt es bei relationalen Datenbanken zu beachten, dass sowohl eine klare Struktur gebildet wird, als auch die Daten unter Vermeidung von Anomalien abgespeichert werden. Da eine Verletzung dieser Regelungen zu gravierenden Fehlern führt, müssen jegliche Fehler vor Inbetriebnahme der Datenbank ausgemerzt sein.
\paragraph[Anomalien]{Anomalien:}
% https://de.wikipedia.org/wiki/Anomalie_(Informatik)
Im relationalen Datenbankenkontext wird der Begriff \emph{Anomalien} für Fehler verwendet, welche aufgrund des Datenbank-Designs oder von Programmen, die im Hintergrund mit der Datenbank interagieren, verursacht werden. Daher entstehen diese Fehler nicht absichtlich durch den Anwender.
\subparagraph{Einfüge-Anomalie:}  ~\\
Von der Einfüge-Anomalie sind all jene Tabellen betroffen, welche den Primärschlüssel aus zu vielen Teilattributen bilden, sodass logisch-valide Datenreihen nicht eingetragen werden können. Dies lässt sich damit begründen, dass für vorerst unwichtige Attribute, welche jedoch fälschlicherweise Teil des Primärschlüssel sind, keine Werte vorhanden sind.
\subparagraph{Änderungs-Anomalie:}  ~\\
Eine Änderungs-Anomalie entsteht, falls Werte, die einem Objekt eindeutig zugeordnet werden können, redundant sind. Daher geschieht bei einer Änderung dieses Objektes jedoch eine Änderungs-Anomalie und die Werte werden nicht bei allen Datenzellen entsprechend dem Wunsch des Benutzers geändert.
\subparagraph{Lösch-Anomalie:}  ~\\
Damit die Lösch-Anomalie verhindert wird, muss jede Tabelle der Datenbank so aufgebaut sein, dass wirklich nur zusammenhängende Informationen voneinander abhängig sind. Ist dies nicht der Fall, kommt es bei einer Löschung von bestimmten Daten automatisch zu einem Verlust von relevanten Daten.
\paragraph[Normalformen]{Normalformen:} \cfoot{\thesubsection$\;$-$\;$\rightmark}
Das Ziel der Normalisierung ist die Vermeidung von Redundanzen oder Anomalien. Im Kontext von relationalen Datenbanken versteht man unter dem Begriff \emph{Redundanzen} mehrfaches Abspeichern von identen Informationen.

Damit diese Normalisierung strukturiert an der jeweiligen Datenbank durchgeführt werden kann, wurden einzelne Schritte gebildet. Diese einzelnen Stufen können dann ähnlich wie ein Rezept abgearbeitet werden. Wie bei einem Rezept bauen die einzelnen Schritte, im Fachjargon \emph{Normalformen} (NF) genannt, alle aufeinander auf. 
\subparagraph{Erste Normalform:} %~\\
\begin{quote}
	''Jedes Attribut der Relation muss einen atomaren Wertebereich haben, und die Relation muss frei von Wiederholungsgruppen sein.'' \wfootcite{wiki-nf}
\end{quote}

Die erste Normalform definiert den Inhalt der einzelnen Wertezellen. Die Datenzellen sollten so aufgebaut werden, dass jeder Zelleninhalt nicht noch weiter aufteilbar ist. 

Des Weiteren sollten Datenspalten mit sehr ähnlichem oder gleichem Inhalt aus der Tabelle entfernt werden und mittels einer weiteren Tabelle an das vorherige Attribut verknüpft werden.


\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\linewidth]{pics/ladner/nf1_1}
	\caption{Negativbeispiel der ersten Normalform}
	\label{fig:nf11}
\end{figure}

Abbildung \ref{fig:nf11}\wfootcite{wiki-nf} zeigt eine Datenbank-Tabelle, bevor die erste Normalform auf die Tabelle angewandt wird. Die rot markierten Felder verstoßen gegen die erste Normalform und daher sind Veränderungen vorzunehmen. 

Da die Spalte \emph{Album} sowohl die Band, als auch den Albumtitel beinhaltet, verstoßt diese Spalte gegen die erste Normalform. Zusätzlich besitzt die Zeile mit der \emph{CD\_ID} 4711 im Feld \emph{Titelliste} mehrere Titel. Dieses Feld \emph{Titelliste} ist daher ebenfalls aufzuspalten.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\linewidth]{pics/ladner/nf1_2}
	\caption{Lösung des Beispiels in der ersten Normalform}
	\label{fig:nf12}
\end{figure}

Wie in Abbildung \ref{fig:nf12}\wfootcite{wiki-nf} zu sehen ist, wurden alle Fehler beseitigt und daher entspricht diese Tabelle den Vorgaben der ersten Normalform.

Das Feld \emph{Album} wurde aufgespalten in \emph{Albumtitel} und \emph{Interpret}. Des Weiteren wurde das Feld \emph{Titelliste} in \emph{Track} und \emph{Titel} umgewandelt. \emph{Track} bildet nun mit \emph{CD\_ID} einen Primärschlüssel, welcher aus zwei Teilen besteht.

\subparagraph{Zweite Normalform:} %~\\
\begin{quote}
	''Eine Relation ist genau dann in der zweiten Normalform, wenn die erste Normalform vorliegt und kein Nichtprimärattribut (Attribut, das nicht Teil eines Schlüsselkandidaten ist) funktional von einer echten Teilmenge eines Schlüsselkandidaten abhängt.'' \wfootcite{wiki-nf}
\end{quote}

Grundsätzlich erfüllt jede Tabelle mit nur einem einzelnen Primärschlüsselattribut automatisch die zweite Normalform, falls sie ebenfalls die erste Normalform erfüllt.

Besitzt jedoch die Relation mehrere Teil-Schlüssel, die dann einen gemeinsamen Primärschlüssel bilden, so muss beachtet werden, dass jedes Nicht-Schlüsselattribut von allen Teil-Schlüsseln des Primärschlüssels abhängt.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\linewidth]{pics/ladner/nf2_1}
	\caption{Negativbeispiel der zweiten Normalform}
	\label{fig:nf21}
\end{figure}

Bei genauerer Betrachtung von Abbildung \ref{fig:nf21}\wfootcite{wiki-nf} wird ersichtlich, dass hierbei die Spalten \emph{Gründungsjahr}, \emph{Erscheinungsjahr} sowie \emph{Albumtitel} zwar von dem Teil-Schlüssel \emph{CD\_ID} abhängen, jedoch nicht vom zweiten Teil-Schlüssel \emph{Track}.
\newpage
\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\linewidth]{pics/ladner/nf2_2}
	\caption{Lösung des Beispiels in der zweiten Normalform}
	\label{fig:nf22}
\end{figure}
Zur Lösung der vorher genannten Probleme wird in Abbildung \ref{fig:nf22}\wfootcite{wiki-nf} die vorherige Tabelle \emph{CD\_Lied} in zwei Tabellen aufgeteilt. Damit die redundanten Dateneinträge verhindert werden, bilden nun \emph{Track} und \emph{Titel} die neue Tabelle \emph{Lied}. Damit die CD mit den einzelnen Liedern der CD verknüpft werden, kommt der Fremdschlüssel \emph{CD\_ID} zum Einsatz. Dieser Fremdschlüssel bildet mit \emph{Track} einen gemeinsamen Primärschlüssel. Da \emph{CD} nun nur noch ein Primärschlüssel-Attribut besitzt, erfüllt die Tabelle automatische die zweite Normalform. Des Weiteren entspricht auch die Tabelle \emph{Lied} der zweiten Normalform, da \emph{Titel} von beiden Teilschlüsseln des Primärschlüssels abhängt.
\subparagraph{Dritte Normalform:}
\begin{quote}
	''Die dritte Normalform ist erreicht, wenn sich das Relationenschema in der zweiten Normalform befindet, und kein Nichtschlüsselattribut  von einem anderen Nichtschlüsselattribut funktional abhängig ist.''\wfootcite{wiki-nf}
\end{quote}
Wie auch schon bei der vorherigen Normalform müssten die vorangegangen Normalformen entsprechend der jeweiligen Vorgaben abgearbeitet sein, damit die dritte Normalform erfüllt ist. Des Weiteren muss die Datenbank so angepasst werden, dass alle Nichtschlüsselattribute ohne Zwischenschritt direkt vom Primärschlüssel abhängen. 
\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\linewidth]{pics/ladner/nf3_1}
	\caption{Negativbeispiel der dritten Normalform}
	\label{fig:nf31}
\end{figure}

In Abbildung \ref{fig:nf31}\wfootcite{wiki-nf} befinden sich in der Tabelle \emph{CD} einige Fehler. Grundsätzlich hängt das Gründungsjahr nicht von der Spalte \emph{CD\_ID} ab, sondern vom Interpreten. 

Weiters würde es bei einer Veränderung des Interpreten-Namens, welcher mehrere \emph{CD}-Einträge besitzt, leicht zu einer Änderungs-Anomalie kommen. Das Ergebnis eines fehlerhaften Datenbankveränderungsbefehls wären dann \emph{CD}-Einträge, bei denen jedoch nur teilweise der Name des \emph{Interpreten} verändert ist.
\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\linewidth]{pics/ladner/nf3_2}
	\caption{Lösung des Beispiels in der dritten Normalform}
	\label{fig:nf32}
\end{figure}

Damit die Änderungs-Anomalie verhindert werden kann, wurde Abbildung \ref{fig:nf32}\wfootcite{wiki-nf} dementsprechend angepasst. Bei Betrachtung der Grafik wird ersichtlich, dass nun eine neue Tabelle \emph{Künstler} vorhanden ist. Diese Tabelle beinhaltet die zwei zuvor fehlerbehafteten Spalten \emph{Interpret} und \emph{Gründungsjahr} und zusätzlich noch das Feld \emph{Interpret\_ID}, welches zur eindeutigen Identifizierung des Interpreten dient.

Damit nun die \emph{CD}-Einträge mit dem Künstler verknüpft werden können, wird in der Tabelle \emph{CD} ein neues Fremdschlüssel-Feld mit dem Namen \emph{Interpret\_ID} hinzugefügt. Dadurch ist eine 1-N Verknüpfung zu der Tabelle Künstler möglich. Eine 1-N Verknüpfung ermöglicht dem Künstler ($\rightarrow$ 1 Künstler) mehrere CDs ($\rightarrow$ N CDs) zu veröffentlichen, jedoch kann jede CD nur einen Interpreten besitzen.
\\\\
Da die vorher genannten, ersten drei Normalformen den Großteil der Fehler im Bereich der Datenbankmodelierung verhindern, wird auf eine weitere Ausführung der Normalformen verzichtet.
\subsubsection{Datenbank-Schema} %\label{Datenbank-Schema}
Aufgrund unserer Unterrichtserfahrungen mit MariaDB wurde dieses Datenbankmanagementsystem für das Diplomprojekt verwendet. Da es sich hierbei um eine Abspaltung des populären Datenbanksystems MySQL handelt, ist die Syntax für etwaige Befehle ähnlich. Aufgrund der erleichterten Installation und Konfiguration fiel die Wahl auf XAMPP. Dadurch war die Datenbank schnell einsatzbereit. 

Das Datenbank-Schema befindet sich im Unterkapitel \ref{ssec:datenbankschema} \emph{Datenbankschema} (Abbildung \ref{fig:datenbankschema} \emph{Datenbankschema}) auf Seite \pageref{fig:datenbankschema}.
\subsubsection{Erläuterung der Tabellen}
Wie im Datenbank-Schema zu sehen ist, besteht unsere Datenbank aus 15 Tabellen. Die Anzahl der Tabellen wuchs im Laufe der Diplomarbeit an, da immer wieder neue Anforderungen an die Datenbank gestellt wurden.
\subparagraph{Bands:}  ~\\
Hierbei werden die Eigenschaften der Bands abgespeichert. Das Passwort der Band ist mittels des \emph{Secure Hash Algorithm Version 2 (SHA2)-Standards} verschlüsselt, welcher noch immer als sicher betrachtet wird.
\subparagraph{Genres:}  ~\\
Hierbei werden die verschiedenen Genres eingetragen, damit dann in weiterer Folge Bands verschiedene Genres angeben können. Damit Benutzer in ihrer Sprache Genres auswählen können, werden die Genrenamen in verschiedenen Sprachen abgespeichert.
\subparagraph{BandGenres:}  ~\\
Diese Tabelle ermöglicht es den Bands, einzelne Genres abzuspeichern, welche von der Band gespielt werden. Dadurch gelingt es Anwendern leichter, Bands entsprechend des Musikstils zu finden.  
\subparagraph{Musicians:}  ~\\
Wie auch schon bei der \emph{Bands}-Tabelle werden hier alle Daten des Benutzers gesichert. Jedoch unterscheiden sich die Eigenschaften, welche in der Tabelle enthalten sind.
\subparagraph{BandMembers:}  ~\\
Diese Zwischentabelle verknüpft die Tabellen \emph{Bands} und \emph{Musicians} miteinander. Dadurch wird es ermöglicht, dass eine Band mehrere Musiker als Mitglieder hat und der einzelne Musiker Teil mehrerer Bands sein kann.
\subparagraph{MusicanBand\_Requests:}  ~\\
Aufgrund dieser Relation wird es einem Musiker ermöglicht, eine Mitgliedsanfrage an beliebige Bands zu senden. Diese werden dann auf der privaten Profilansicht der Band angezeigt und können von der Band angenommen oder abgelehnt werden.
\subparagraph{MusicianBand\_SearchRequest:}  ~\\
Diese Tabelle sollte Suchanfragen nach Bands abspeichern können. Damit es zu keiner Verletzung der vorher genannten Normalformen kommt, wird eine weitere Tabelle benötigt, welche die auswählten Genres pro Suche abspeichert.
\subparagraph{MusicianBand\_SearchGenres:} ~\\%TODO
Falls eine Suche nach Bands abgespeichert werden sollte, welche eine Anzahl von Genres als Suchparameter beinhaltet, werden diese mithilfe der Tabelle \emph{MusicianBand\_SearchGenres} abgespeichert.
\subparagraph{BandMember\_Requests:}  ~\\
Dies ist das Gegenstück zur Tabelle \emph{MusicianBand\_Requests}. Dadurch können Bands zur Mitgliederakquirierung einzelnen Musikern Mitgliedsanfragen senden.
\subparagraph{BandMember\_SearchProperties:}  ~\\
Damit eine Suche nach einzelnen Musikern für Bands erleichtert wird, können auch hier Suchabfragen gesichert werden.
\subparagraph{Instruments:}  ~\\
Damit der Benutzer einfacher Suchabfragen ausführen kann, wurden alle Instrumente in eine eigene Tabelle abgespeichert. Dadurch werden Doppelbezeichnungen für Instrumente verhindert und der Benutzer kann seine persönlichen Instrumente aus einer Liste von Instrumenten auswählen.
\subparagraph{BandMember\_SearchInstruments:}  ~\\
Diese Zwischentabelle (Verknüpfung zwischen \emph{BandMember\_SearchProperties} und \emph{Instruments}) ermöglicht eine präzisere Suche nach neuen Mitgliedern, da hier mehrere Instrumente angegeben werden können. Spielt ein Musiker eines der angegebenen Instrumente, so scheint dieser in den Suchergebnissen auf.
\subparagraph{MusicianInstruments:}  ~\\
Diese Relation ermöglicht dem Musiker-Benutzer individuell seine Instrumente abzuspeichern.
\subparagraph{Cities:}  ~\\
Damit die Suchen auch geografisch eingeschränkt werden können, wurde diese Relation eingebaut. Dadurch kann der Benutzer einen Heimatort angeben und in beliebigem Umkreis nach Bands bzw. Musikern suchen. 
\subparagraph{Countries:}  ~\\
Wie auch schon die \emph{Cities}-Tabelle, ist die \emph{Country}-Tabelle auch bei den Suchabfragen hilfreich. Im Gegensatz dazu ermöglicht sie jedoch eine effizientere Suche nach dem Heimatort des jeweiligen Benutzers.
\subsubsection{Geografische Daten}
Damit unsere Webseite Benutzern einen Mehrwert bieten kann, müssen etwaige Suchabfragen, Vorschläge oder Benachrichtigungen unter Verwendung von Standortdaten relevant gemacht werden. Daher benötigt unsere Datenbank die entsprechenden Einträge.
\\\\
Offensichtlich können diese Einträge nicht manuell in die Datenbank eingetragen werden, daher suchten wir nach einer Lösung, welche sowohl möglichst unkompliziert, als auch bevorzugt kostenfrei verwendet werden darf. 
\\\\
Nach einer Recherche nach möglichen Problemlösungen fanden wir ein Github-Repository (\url{www.github.com/zauberware/postal-codes-json-xml-csv}), welches unseren Ansprüchen gerecht wurde. Dieser Datensatz bezieht seine Werte von GeoNames (\url{www.geonames.org}) und veröffentlicht diese Daten unter der \emph{\Gls{a-CCGeo}}. Diese Lizenz erlaubt die Verwendung, falls der Name der Quelle und deren Lizenz genannt werden.
\\\\
Das Github-Repository beinhaltet die Daten in den gängigsten Dateiformaten. Damit wir den für uns relevanten Teilbereich in die Datenbank einfügen können, wählten wir das \gls{a-Json}-Format. Danach wandelten wir mittels eines JSON-To-SQL-Converters (\url{www.sqlify.io/convert/json/to/sql}) die JSON-Datei in eine \Gls{a-SQL}-Datei. Während der Konvertierung konnten noch die einzelnen Spalten des Endproduktes editiert werden.
\\\\
Zur leichteren Implementierung wandelten wir zuerst die JSON-Datei in eine temporäre Tabelle um, welche dann mittels diversen SQL-Abfragen unsere permanenten Tabellen befüllt.
\\\\
Da sich die Webseite auf den mitteleuropäischen Sprachraum beziehen sollte, implementierten wir Datensätze für folgende Länder:
\begin{itemize}
	\item Österreich (18735 Einträge)
	\item Deutschland (17327 Einträge)
	\item Schweiz (3456 Einträge)
\end{itemize}
\end{document}
